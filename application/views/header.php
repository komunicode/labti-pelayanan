<!DOCTYPE html>
<html>
<head>
	<title>Labti-Pelayanan</title>
	<link href='<?php echo base_url(); ?>assets/img/labti.png' rel='shortcut icon'>

	<!--Import Google Icon Font-->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/material-icons/material-icons.css" />

	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/materialize.min.css" />

	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<!-- header -->	
	<div class="navbar-fixed">
		<nav class="lighten-1 z-depth-3" style="background-color: #0f3057;">
		    <div class="container nav-wrapper">
		    	<a href="<?php echo base_url(); ?>welcome" class="brand-logo" style="padding: 10px"><img style="width: 60px" src="<?php echo base_url(); ?>assets/img/labti.png"></a>
	      		<a href="#!" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
	      		<ul class="right hide-on-med-and-down">
		        	<li><a href="<?php echo base_url(); ?>welcome" style="font-size: 13px"><i class="material-icons left">home</i>Home</a></li>
	        		<li><a href="<?php echo base_url(); ?>welcome/about" style="font-size: 13px"><i class="material-icons left">info</i>About</a></li>
	        		<li><a href="<?php echo base_url(); ?>welcome/contact" style="font-size: 13px"><i class="material-icons left">phone</i>Contact Us</a></li>
	      		</ul>	      	
	    	</div>
		</nav>
	</div>
	<ul class="side-nav" id="mobile-demo">
		<div class="center lighten-1 z-depth-3" style="background-color: #0f3057; padding: 10px">
			<img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
	    </div>
		<li><a href="<?php echo base_url(); ?>welcome" class="waves-effect waves-teal"><i class="material-icons left">home</i>Home</a></li>
	    <li><a href="<?php echo base_url(); ?>welcome/about" class="waves-effect waves-teal"><i class="material-icons left">info</i>About</a></li>
	    <li><a href="<?php echo base_url(); ?>welcome/contact" class="waves-effect waves-teal"><i class="material-icons left">phone</i>Contact Us</a></li>
	</ul>