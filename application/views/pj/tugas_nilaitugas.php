<main>
	<div class="row">
		<?php foreach($record as $a): ?>
		<div class="center">
			<img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">			
			<h5>Penilaian Tugas Praktikan</h5><br><br>
		</div>
		<form method="post" action="<?php echo base_url(); ?>pj/NilaiTugasPraktikan">
			<div class="input-field col s4">
	            <input type="text" readonly value="<?php echo $a['nama']; ?>">
	            <label >Nama Praktikan</label>
	        </div>
	    	<div class="input-field col s4">
	            <input readonly type="text" value="<?php echo $a['kelas']; ?>">
	            <label >Kelas</label>
			</div>
			<div class="input-field col s4">
	            <input readonly type="text" value="<?php echo $a['no_hp']; ?>">
	            <label >No. HP</label>
			</div>
			<div class="input-field col s6">
	            <input readonly type="text" value="<?php echo $a['matprak']; ?>">
	            <label >Mata Praktikum</label>
			</div>
			<div class="input-field col s6">
	            <input readonly type="text" value="<?php echo $a['pertemuan']; ?>">
	            <label >Pertemuan</label>
			</div>
			<div class="input-field col s12">
	            <input readonly type="text" value="<?php echo $a['alasan']; ?>">
	            <label >Alasan</label>
			</div>
			<div class="input-field col s6">
	            Tugas : <br>
	            <p><?php echo $a['soal']; ?></p><br>
	            <a href="<?php echo base_url(); ?>pj/tugas/<?php echo $a['tugas']; ?>" target="_blank"><?php echo $a['tugas']; ?></a>
			</div>
			<div class="input-field col s6">
	            <input type="text" name="nilai" required>
	            <label >Nilai</label>
			</div>
			<input type="text" value="<?php echo $a['id_laporan']; ?>" name="id_laporan" hidden>
	    	<div class="center col s12">
              	<input class="blue waves-effect waves-light btn" type="submit" name="submit" value="NILAI TUGAS PRAKTIKAN">
            </div>
    	</form>
    	<?php endforeach; ?><!-- akhir perulangan -->
    </div>
</main>