<main>
  <!-- tabel user -->
    <div class="section z-depth-5" style="margin: 50px">
      <div style="padding: 10px">
      <div class="center">
        <img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
        <h4 class="header center text-lighten-2">Data Penilaian Tugas Praktikan</h4>
      </div>
      <table id="example" class="highlight centered">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama Praktikan</th>
            <th>Kelas</th>
            <th>Mata Praktikum</th>
            <th>Pertemuan</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $no=1; foreach($record as $a): ?><!-- perulangan disini -->
            <tr class="z-depth-1">
              <td><?php echo $no; ?></td>
              <td><?php echo $a['nama']; ?></td>
              <td><?php echo $a['kelas']; ?></td>
              <td><?php echo $a['matprak']; ?></td>
              <td><?php echo $a['pertemuan']; ?></td>
              <?php if ($a['status_laporan']==4): ?>
                <td><b class="red-text"><?php echo $a['catatan']; ?></b></td>  
              <?php elseif ($a['status_laporan']==5): ?>
                <td><b class="blue-text"><?php echo $a['catatan']; ?></b></td>  
              <?php elseif ($a['status_laporan']==6): ?>
                <td><b class="green-text"><?php echo $a['catatan']; ?></b></td>  
              <?php endif ?>
              <td>
                <a href="#<?php echo $a['id_laporan']; ?>" class="btn-floating tooltipped waves-effect waves-light green darken-4 z-depth-4" data-position="buttom" data-delay="10" data-tooltip="Lihat Laporan"><i class="material-icons">launch</i></a>                  
              </td>
            </tr>
          <?php $no++; endforeach; ?><!-- akhir perulangan -->
        </tbody>
      </table>
      </div>
    </div>

    <!-- modal launch -->
    <?php foreach($record as $b): ?><!-- perulangan disini -->
      <div id="<?php echo $b['id_laporan']; ?>" class="modal modal-fixed-footer">
        <div class="modal-content">
          <!-- judul -->
          <div class="center">
            <img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
            <h4>Data Penilaian Tugas Praktikan</h4>
          </div>
          <!-- bagian penampil data -->
          <div class="row">
            <div class="input-field col s4">
              <input readonly type="text" value="<?php echo $b['nama']; ?>">
              <label >Nama Praktikan</label>
            </div>
            <div class="input-field col s4">
              <input readonly type="text" value="<?php echo $b['kelas']; ?>">
              <label >Kelas</label>
            </div>
            <div class="input-field col s4">
              <input readonly type="text" value="<?php echo $b['no_hp']; ?>">
              <label >No. HP</label>
            </div>
            <div class="input-field col s6">
              <input readonly type="text" value="<?php echo $b['matprak']; ?>">
              <label >Mata Praktikum</label>
            </div>
            <div class="input-field col s6">
              <input readonly type="text" value="<?php echo $b['pertemuan']; ?>">
              <label >Pertemuan</label>
            </div>
            <div class="input-field col s12">
              <input readonly type="text" value="<?php echo $b['alasan']; ?>">
              <label >Alasan</label>
            </div>
            <div class="input-field col s12">
              Tugas : <br>
              <p><?php echo $b['soal']; ?></p><br>
              <a href="<?php echo base_url(); ?>pj/tugas/<?php echo $b['tugas']; ?>" target="_blank"><?php echo $b['tugas']; ?></a>
            </div>
          </div>
        </div>
        <?php if($b['status_laporan']==5): ?>
        <!-- action modal launch -->
        <div class="modal-footer">
          <a href="<?php echo base_url(); ?>pj/tugas_nilaitugas/<?php echo $b['id_laporan']; ?>" class="modal-action modal-close waves-effect waves-green btn-flat">Nilai Tugas</a>
        </div>
        <?php else: ?>
        <?php endif; ?>
      </div>
    <?php endforeach; ?><!-- akhir perulangan --> 
</main>