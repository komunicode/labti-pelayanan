	
	<!-- footer -->
	<footer class="lighten-1" style="background-color: #0f3057;">
    <div class="center" style="padding: 20px">
      <img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
      <h5 class="white-text"><b>Pelayanan Laboratorium Teknik Informatika</b></h5>
      <p class="grey-text text-lighten-4">Pelayanan LabTi kini hadir bersama anda, memberikan kemudahan dalam pelayanan LabTi.</p>
      <h5 class="white-text">Kunjungi Website Kami</h5>
      <a class="grey-text text-lighten-3" href="http://ti-dasar.lab.gunadarma.ac.id/">LabTi - Dasar</a>
      <a>  |  </a>
      <a class="grey-text text-lighten-3" href="http://ti-menengah.lab.gunadarma.ac.id/">LabTi - Menengah</a>
      <a>  |  </a>
      <a class="grey-text text-lighten-3" href="http://ti-lanjut.lab.gunadarma.ac.id/">LabTi - Lanjut</a><br>
      <a href="http://gunadarma.ac.id/"><img style="width: 300px" src="<?php echo base_url(); ?>assets/img/bartelme.png"></a>
    </div>

    <div class="footer-copyright center">
      <div class="container">
        © 2017 Copyright LabTi-Pelayanan
      </div>
		</div>
	</footer>

	<!--Import jQuery before materialize.js-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/materialize.min.js"></script>
    <script type="text/javascript">
    	$(document).ready(function(){
    		$(".button-collapse").sideNav();
    		$('.slider').slider();
    		$('.modal').modal();
        $('.parallax').parallax();
  		});
  	</script>
</body>
</html>