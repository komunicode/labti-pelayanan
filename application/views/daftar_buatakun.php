<main class="container">
    <div class="row" style="padding: 20px">
		<div class="center">
			<img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
        	<h5>Buat Akun Pelayanan</h5>
      	</div>
      	<form method="post" action="<?php echo base_url(); ?>welcome/daftar_buatakunbaru">
          <?php foreach ($record as $a):?>
            <div class="input-field col s6 m12">
                <input type="text" value="<?php echo $a['npm'] ?>" required readonly name="npm">
                <label>NPM Anda</label>
            </div>
            <div class="input-field col s6 m4">
                <input type="text" value="<?php echo $a['nama'] ?>" required readonly name="nama">
                <label>Nama Anda</label>
            </div>
            <div class="input-field col s6 m4">
                <input type="text" value="<?php echo $a['kelas'] ?>" required readonly name="kelas">
                <label>Kelas Anda</label>
            </div>
            <div class="input-field col s6 m4">
                <input type="text" value="<?php echo $a['angkatan'] ?>" required readonly name="angkatan">
                <label>Angkatan Anda</label>
            </div>
          <?php endforeach;?>
          	<div class="input-field col s12 m12">
            		<input type="text" name="username" required name="username">
            		<label>Masukkan Username</label>
          	</div>
            <div class="input-field col s12 m12">
                <input type="password" name="password" required name="password">
                <label>Masukkan Password</label>
            </div>
        	<div class="center col s12">
          		<input class="blue waves-effect waves-light btn" type="submit" name="submit" value="BUAT AKUN">
        	</div>
      	</form>
    </div>
</main>