<!-- parallax -->
<div class="section z-depth-5" style="background: url(<?php echo base_url(); ?>assets/img/5.jpg); background-repeat: no-repeat; background-position: center; background-size: cover;">
  <!-- Text -->
  <div class="row">
    <div class="col s12 m6">
    </div>
    <div class="col s12 m6" style="margin-top: 2%">
      <div class="container row">
        <div class="center">
          <img style="width: 150px" src="<?php echo base_url(); ?>assets/img/labti.png">
          <h5><b>Pelayanan Laboratorium Teknik Informatika</b></h5>
          <hr>
        </div>
        <div class="col s12" style="background-color: rgba(15,48,87,0.2); padding: 15px">
          <h5><b>Ruangan</b></h5>
          <p>Universitas Gunadarma Kampus E, Kelapa Dua, Depok, Gedung 5, Lantai 3, Ruang E535</p>
          <h5><b>E-Mail</b></h5>
          <b><a href="mailto:labtipelayanan@gmail.com?Subject=Pelayanan%20Praktikan">labtipelayanan@gmail.com</a></b>
        </div>
      </div>
    </div>
    <div class="col s12">
      <br><br><br><br><br><br><br><br><br><br>
    </div>
  </div>
</div>

<div class="parallax-container z-depth-3">
  <div class="parallax"><img src="<?php echo base_url(); ?>assets/img/6.jpg"></div>
  <div class="center" style="margin-top: 10% ">
    <img style="width: 100px; height: auto;" src="<?php echo base_url(); ?>assets/img/labti.png">
    <h5><b>Pelayanan Laboratorium Teknik Informatika</b></h5>
  </div>
</div>