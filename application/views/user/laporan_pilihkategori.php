<main>
  <div class="row z-depth-5" style="padding: 10px; margin: 20px">
    <div class="center">
      <img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
      <h4><b>Pilih Kategori Izin</b></h4>
    </div>
    <ul class="collapsible popout" data-collapsible="accordion">
      <li>
        <div class="collapsible-header">
          <h5><img style="width: 50px" src="<?php echo base_url(); ?>assets/img/labti.png"><b>Sakit</b></h5>
        </div>
        <div class="collapsible-body">
          <div class="row">
            <div class="col s12 m6 center" style="padding: 10px 0px 10px 0px">
              <a href="<?php echo base_url(); ?>user/laporan_pilihpraktikum/1"><h5><b>Rawat Inap</b></h5></a>
            </div>
            <div class="col s12 m6 center" style="padding: 10px 0px 10px 0px">
              <a href="<?php echo base_url(); ?>user/laporan_pilihpraktikum/2"><h5><b>Non-Rawat Inap</b></h5></a>
            </div>
          </div>
        </div>
      </li>

      <li>
        <div class="collapsible-header">
          <h5><img style="width: 50px" src="<?php echo base_url(); ?>assets/img/labti.png"><b>Terlambat</b></h5>
        </div>
        <div class="collapsible-body">
          <div class="row">
            <div class="col s12 m4 center" style="padding: 10px 0px 10px 0px">
              <a href="<?php echo base_url(); ?>user/laporan_pilihpraktikum/3"><h5><b>Keterlambatan Kereta</b></h5></a>
            </div>
            <div class="col s12 m4 center" style="padding: 10px 0px 10px 0px">
              <a href="<?php echo base_url(); ?>user/laporan_pilihpraktikum/4"><h5><b>Ban Bocor</b></h5></a>
            </div>
            <div class="col s12 m4 center" style="padding: 10px 0px 10px 0px">
              <a href="<?php echo base_url(); ?>user/laporan_pilihpraktikum/5"><h5><b>Lainnya</b></h5></a>
            </div>
          </div>
        </div>
      </li>

      <li>
        <div class="collapsible-header">
          <h5><img style="width: 50px" src="<?php echo base_url(); ?>assets/img/labti.png"><b>Izin</b></h5>
        </div>
        <div class="collapsible-body">
          <div class="row">
            <div class="col s12 m4 center" style="padding: 10px 0px 10px 0px">
              <a href="<?php echo base_url(); ?>user/laporan_pilihpraktikum/6"><h5><b>Acara Keluarga</b></h5></a>
            </div>
            <div class="col s12 m4 center" style="padding: 10px 0px 10px 0px">
              <a href="<?php echo base_url(); ?>user/laporan_pilihpraktikum/7"><h5><b>Acara Kampus</b></h5></a>
            </div>
            <div class="col s12 m4 center" style="padding: 10px 0px 10px 0px">
              <a href="<?php echo base_url(); ?>user/laporan_pilihpraktikum/8"><h5><b>Acara Lainnya</b></h5></a>
            </div>
          </div>
        </div>
      </li>

    </ul>
  </div>
</main>