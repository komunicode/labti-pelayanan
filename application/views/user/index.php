<main>
  	<!-- bagian pelayanan -->
  	<div class="row section" style="padding: 20px 5px 0px 5px">
  		<div class="col s12 m12 center" style="margin-bottom: 20px">
  			<img class="responsive-img" style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
  			<h5><b>Laboratorium Teknik Informatika</b></h5>
  		</div>
  		<div class="col s12">

  			<!-- LAPORAN AKTIF -->
  			<h6><b>Laporan Aktif :</b></h6>
  			<ul class="collapsible popout" data-collapsible="accordion">
  				<?php if($lap_aktif!=null): ?>
	  				<?php foreach ($lap_aktif as $a): ?>
	  					<?php if(($a['tipe_laporan']==1) AND ($a['status_laporan']==1)) : ?>
	  						<li>
			  					<div class="collapsible-header grey lighten-2 active" id="judul">
			  						<b><?php echo $a['matprak']; ?></b>
						      		<span class="new badge" data-badge-caption="">Upload Tugas !!!</span>
						  		</div>
						    	<div class="collapsible-body row" style="padding: 5px">
						    		<div class="col s10" id="tugas">
						    			<span><b>Tanggal Lapor : </b><?php echo $a['tgl_lapor']; ?></span><br>
							    		<span><b>Pertemuan : </b><?php echo $a['pertemuan']; ?></span><br>
							    		<span><b>Alasan : </b><?php echo $a['alasan']; ?></span><br>
							    		<span><b>No. Handphone : </b><?php echo $a['no_hp']; ?></span><br>
							    		<span><b>Tugas : </b>(Laporan dalam format .pdf , Max. Size 10 MB)</span><br>	
							    		<span><h5><b><?php echo $a['soal']; ?></b></h5></span><br>	
						    		</div>
						    		<div class="col s2" style="text-align: right;">
						    			<a class="btn-floating red click-to-toggle z-depth-4 tooltipped" href="<?php echo base_url(); ?>user/delete_laporan/<?php echo $a['id_laporan']; ?>" data-position="left" data-delay="10" data-tooltip="Hapus Laporan" onclick="return  confirm('Hapus Laporan Ketidakhadiran ?')">
			                        		<i class="material-icons">delete</i>
			                      		</a><br><br>
						    			<a class="btn-floating green click-to-toggle z-depth-4 tooltipped" href="<?php echo base_url(); ?>user/laporan_inputtugas/<?php echo $a['id_laporan']; ?>" data-position="left" data-delay="10" data-tooltip="Upload Tugas" onclick="return  confirm('Upload Tugas ?')">
			                        		<i class="material-icons">file_upload</i>
			                      		</a>
						    		</div>
						    	</div>
						  	</li>
	  					<?php elseif(($a['tipe_laporan']==1) AND ($a['status_laporan']==2)) : ?>
	  						<li>
			  					<div class="collapsible-header yellow lighten-2" id="judul">
			  						<b><?php echo $a['matprak']; ?></b>
						      		<span class="new badge" data-badge-caption="">Laporan Diproses ...</span>
						  		</div>
						    	<div class="collapsible-body" style="padding: 5px">
									<span><b>Tanggal Lapor : </b><?php echo $a['tgl_lapor']; ?></span><br>
							    	<span><b>Pertemuan : </b><?php echo $a['pertemuan']; ?></span><br>
							    	<span><b>Alasan : </b><?php echo $a['alasan']; ?></span><br>
							    	<span><b>No. Handphone : </b><?php echo $a['no_hp']; ?></span><br>
						    	</div>
						  	</li>
	  					<?php elseif(($a['tipe_laporan']==1) AND ($a['status_laporan']==3)) : ?>
	  						<li>
			  					<div class="collapsible-header green lighten-2" id="judul">
			  						<b><?php echo $a['matprak']; ?></b>
						      		<span class="new badge" data-badge-caption="">QRCode Aktif !!!</span>
						  		</div>
						    	<div class="collapsible-body row" style="padding: 5px">
						    		<div class="col s12 center">
						    			<img src="https://chart.googleapis.com/chart?cht=qr&amp;chs=140x140&amp;chl=<?php echo $a['id_laporan']; ?>">
						    		</div>
						    		<div class="col s10">
						    			<span><b>Tanggal Lapor : </b><?php echo $a['tgl_lapor']; ?></span><br>
								    	<span><b>Pertemuan : </b><?php echo $a['pertemuan']; ?></span><br>
								    	<span><b>Alasan : </b><?php echo $a['alasan']; ?></span><br>
								    	<span><b>No. Handphone : </b><?php echo $a['no_hp']; ?></span><br>
						    		</div>
						    		<div class="col s2" style="text-align: right;">
						    			<a class="btn-floating blue click-to-toggle z-depth-4 tooltipped" href="<?php echo base_url(); ?>user/laporan_cetaklaporan/<?php echo $a['id_laporan']; ?>" data-position="left" data-delay="10" data-tooltip="Cetak Laporan" onclick="return  confirm('Cetak Laporan ?')">
			                        		<i class="material-icons">print</i>
			                      		</a>
						    		</div>
						    	</div>
						  	</li>
	  					<?php elseif(($a['tipe_laporan']==1) AND ($a['status_laporan']==4)) : ?>
	  						<li>
			  					<div class="collapsible-header blue lighten-2" id="judul">
			  						<b><?php echo $a['matprak']; ?></b>
						      		<span class="new badge" data-badge-caption="">Bayar Blanko !!!</span>
						  		</div>
						    	<div class="collapsible-body" style="padding: 5px">
									<span><b>Tanggal Lapor : </b><?php echo $a['tgl_lapor']; ?></span><br>
								    <span><b>Pertemuan : </b><?php echo $a['pertemuan']; ?></span><br>
								    <span><b>Alasan : </b><?php echo $a['alasan']; ?></span><br>
								    <span><b>No. Handphone : </b><?php echo $a['no_hp']; ?></span><br>
						    	</div>
						  	</li>
	  					<?php elseif(($a['tipe_laporan']==1) AND ($a['status_laporan']==5)) : ?>
	  						<li>
			  					<div class="collapsible-header lime lighten-2" id="judul">
			  						<b><?php echo $a['matprak']; ?></b>
						      		<span class="new badge" data-badge-caption="">Blanko Diterima !!!</span>
						  		</div>
						    	<div class="collapsible-body" style="padding: 5px">
									<span><b>Tanggal Lapor : </b><?php echo $a['tgl_lapor']; ?></span><br>
								    <span><b>Pertemuan : </b><?php echo $a['pertemuan']; ?></span><br>
								    <span><b>Alasan : </b><?php echo $a['alasan']; ?></span><br>
								    <span><b>No. Handphone : </b><?php echo $a['no_hp']; ?></span><br>
						    	</div>
						  	</li>
	  					<?php elseif(($a['tipe_laporan']==1) AND ($a['status_laporan']==6)) : ?>
	  						<li>
			  					<div class="collapsible-header orange lighten-2" id="judul">
			  						<b><?php echo $a['matprak']; ?></b>
						      		<span class="new badge" data-badge-caption="">Tugas Ternilai !!!</span>
						  		</div>
						    	<div class="collapsible-body" style="padding: 5px">
									<span><b>Tanggal Lapor : </b><?php echo $a['tgl_lapor']; ?></span><br>
								    <span><b>Pertemuan : </b><?php echo $a['pertemuan']; ?></span><br>
								    <span><b>Alasan : </b><?php echo $a['alasan']; ?></span><br>
								    <span><b>No. Handphone : </b><?php echo $a['no_hp']; ?></span><br>
						    	</div>
						  	</li>
	  					<?php elseif(($a['tipe_laporan']==2) AND ($a['status_laporan']==1)) : ?>
	  						<li>
			  					<div class="collapsible-header grey lighten-2" id="judul">
			  						<b><?php echo $a['matprak']; ?></b>
						      		<span class="new badge" data-badge-caption="">Belum Terkonfirmasi !!!</span>
						  		</div>
						    	<div class="collapsible-body row" style="padding: 5px">
						    		<div class="col s10">
						    			<span><b>Tanggal Lapor : </b><?php echo $a['tgl_lapor']; ?></span><br>
							    		<span><b>No. Handphone : </b><?php echo $a['no_hp']; ?></span><br>
						    		</div>
						    		<div class="col s2" style="text-align: right;">
						    			<a class="btn-floating red click-to-toggle z-depth-4 tooltipped" href="<?php echo base_url(); ?>user/delete_laporan/<?php echo $a['id_laporan']; ?>" data-position="left" data-delay="10" data-tooltip="Hapus Laporan" onclick="return  confirm('Hapus Laporan Kartu Hilang ?')">
			                        		<i class="material-icons">delete</i>
			                      		</a><br><br>
						    		</div>
						    	</div>
						  	</li>
						<?php elseif(($a['tipe_laporan']==2) AND ($a['status_laporan']==6)) : ?>
	  						<li>
			  					<div class="collapsible-header orange lighten-2" id="judul">
			  						<b>Kartu Hilang</b>
						      		<span class="new badge" data-badge-caption="">Laporan Terkonfirmasi !!!</span>
						  		</div>
						    	<div class="collapsible-body" style="padding: 5px">
						    		<span><b>Tanggal Lapor : </b><?php echo $a['tgl_lapor']; ?></span><br>
							    	<span><b>No. Handphone : </b><?php echo $a['no_hp']; ?></span><br>
						    	</div>
						  	</li>
	  					<?php endif; ?>
	  				<?php endforeach ?>
  				<?php else: ?>
				  	<div class="col s12 m12 center" style="padding: 10px">
	  					<b class="grey-text">Anda Belum Memiliki Laporan Aktif !!!</b>
	  				</div>
	  			<?php endif; ?>

			</ul>  			
  		</div>
  		<div class="col s12">
  			<hr>
  		</div>

  		<div class="col s12">	

  			<!-- LAPORAN DITOLAK -->
  			<h6><b>Laporan Ditolak :</b></h6>
  			<ul class="collapsible popout" data-collapsible="accordion">
  				<?php if($lap_ditolak!=null): ?>
  					<?php foreach ($lap_ditolak as $b): ?>
  						<?php if ($b['status_laporan']==8): ?>
  							<li>
			  					<div class="collapsible-header red">			  						
			  						<b><?php echo $b['matprak']; ?></b>
						      		<span class="new badge" data-badge-caption="">Laporan Ditolak !!!</span>
						  		</div>
						    	<div class="collapsible-body" style="padding: 5px">
									<span><b>Tanggal Lapor : </b><?php echo $b['tgl_lapor']; ?></span><br>
							    	<span><b>Pertemuan : </b><?php echo $b['pertemuan']; ?></span><br>
							    	<span><b>Alasan : </b><?php echo $b['alasan']; ?></span><br>
							    	<span><b>No. Handphone : </b><?php echo $b['no_hp']; ?></span><br>
							    	<span><b>Catatan : </b><?php echo $b['catatan']; ?></span><br>
						    	</div>
						  	</li>
  						<?php elseif ($b['status_laporan']==7): ?>
  							<li>
			  					<div class="collapsible-header red lighten-2">
			  						<b><?php echo $b['matprak']; ?></b>
						      		<span class="new badge" data-badge-caption="">Laporan Ditolak !!!</span>
						  		</div>
						    	<div class="collapsible-body" style="padding: 5px">
									<span><b>Tanggal Lapor : </b><?php echo $b['tgl_lapor']; ?></span><br>
							    	<span><b>Pertemuan : </b><?php echo $b['pertemuan']; ?></span><br>
							    	<span><b>Alasan : </b><?php echo $b['alasan']; ?></span><br>
							    	<span><b>No. Handphone : </b><?php echo $b['no_hp']; ?></span><br>
							    	<span><b>Catatan : </b><?php echo $b['catatan']; ?></span><br>
						    	</div>
						  	</li>
  						<?php endif ?>
  					<?php endforeach ?>
  				<?php else: ?>  							  	
				  	<div class="col s12 m12 center" style="padding: 10px">
	  					<b class="grey-text">Anda Tidak Memiliki Laporan Ditolak !!!</b>
	  				</div>
  				<?php endif; ?>
			</ul>  
  			
  		</div>

  		<div class="col s12">
  			<hr>
  		</div>

  		<div class="col s12">
  			<b>Keterangan Label : </b><br>
  			<div class="row">
  				<div class="col s6 m3 grey lighten-2 z-depth-3" style="padding: 10px">
  					<span class="new badge z-depth-2" data-badge-caption="Upload Tugas !!!"></span>
  				</div>
  				<div class="col s6 m9">
  					<p>Laporan Terkirim, Silahkan Upload Tugas</p>
  				</div>

  				<div class="col s6 m3 yellow lighten-2 z-depth-3" style="padding: 10px">
  					<span class="new badge z-depth-2" data-badge-caption="Laporan Diproses ..."></span>
  				</div>
  				<div class="col s6 m9">
  					<p>Tugas Terkirim, Laporan Sedang Diproses</p>
  				</div>

  				<div class="col s6 m3 green lighten-2 z-depth-3" style="padding: 10px">
  					<span class="new badge z-depth-2" data-badge-caption="QRCode Aktif !!!"></span>
  				</div>
  				<div class="col s6 m9">
  					<p>QRCode Aktif</p>
  				</div>

  				<div class="col s6 m3 blue lighten-2 z-depth-3" style="padding: 10px">
  					<span class="new badge z-depth-2" data-badge-caption="Bayar Blanko !!!"></span>
  				</div>
  				<div class="col s6 m9">
  					<p>QRCode Ter-Scanner, Bayar Blanko</p>
  				</div>

  				<div class="col s6 m3 lime lighten-2 z-depth-3" style="padding: 10px">
  					<span class="new badge z-depth-2" data-badge-caption="Blanko Diterima !!!"></span>
  				</div>
  				<div class="col s6 m9">
  					<p>Bukti Pembayaran Blanko Diterima, Menunggu Penilian Tugas</p>
  				</div>

  				<div class="col s6 m3 orange lighten-2 z-depth-3" style="padding: 10px">
  					<span class="new badge z-depth-2" data-badge-caption="Tugas Ternilai !!!"></span>
  				</div>
  				<div class="col s6 m9">
  					<p>Tugas Sudah Dinilai oleh PJ</p>
  				</div>

  				<div class="col s6 m3 red lighten-2 z-depth-3" style="padding: 10px">
  					<span class="new badge z-depth-2" data-badge-caption="Laporan Ditolak !!!"></span>
  				</div>
  				<div class="col s6 m9">
  					<p>Laporan Ditolak oleh PJ</p>
  				</div>

  				<div class="col s6 m3 red z-depth-3" style="padding: 10px">
  					<span class="new badge z-depth-2" data-badge-caption="Laporan Ditolak !!!"></span>
  				</div>
  				<div class="col s6 m9">
  					<p>Laporan Ditolak oleh Pelayanan</p>
  				</div>
  				
  			</div>
  			<hr>
  		</div>
  	</div>

  	<div class="parallax-container z-depth-3">
  		<div class="parallax"><img src="<?php echo base_url(); ?>assets/img/6.jpg"></div>
  		<div class="center" style="margin-top: 10% ">
  			<img style="width: 100px; height: auto;" src="<?php echo base_url(); ?>assets/img/labti.png">
  			<h5><b>Pelayanan Laboratorium Teknik Informatika</b></h5>
  		</div>
	</div>

  	<div class="fixed-action-btn click-to-toggle horizontal">
  		<a class="btn-floating btn-large green z-depth-4">
  			<i class="large material-icons">playlist_add</i>
	    </a>
	    <ul>
	    	<li>
	    		<a href="<?php echo base_url(); ?>user/laporan_pilihkategori" class="btn-floating blue z-depth-4" onclick="return  confirm('Buat Laporan Ketidakhadiran ?')"><i class="material-icons">assignment</i></a>
	    	</li>
	    	<li>
	    		<a href="<?php echo base_url(); ?>user/input_kartuhilang" class="btn-floating red z-depth-4" onclick="return  confirm('Buat Laporan Kartu Hilang ?')"><i class="material-icons">phonelink_erase</i></a>
	    	</li>
	    </ul>
	</div>
</main>