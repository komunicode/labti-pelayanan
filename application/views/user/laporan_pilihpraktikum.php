	<main>
    <div class="row" style="padding: 20px">
      <div class="center">
        <img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
        <h5>Pilih Praktikum dan Pertemuan</h5>
      </div>
      <form method="post" action="<?php echo base_url(); ?>user/laporan_inputlaporan" enctype="multipart/form-data">
        <div class="input-field col s12 m12">
          <input type="text" readonly value="<?php echo $alasan ?>" name="alasan">
          <label >Kategori Izin</label>
        </div>
        <div class="input-field col s12 m6">
          <select class='form-control' name="id_matprak" required>
            <option value="" disabled selected>--Pilih Praktikum--</option>
              <?php foreach($mp as $a): ?>
                <option value="<?php echo $a['id_matprak']; ?>"><?php echo $a['matprak']; ?></option>
              <?php endforeach; ?><!-- akhir perulangan -->
          </select>
          <label >Mata Praktikum</label>
        </div>
        <div class="input-field col s12 m6">
          <select class='form-control' name="pertemuan" required>
            <option value="" disabled selected>--Pilih Pertemuan--</option>
              <?php for ($i=1; $i<9 ; $i++):?>
                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
              <?php endfor; ?><!-- akhir perulangan -->
          </select>
          <label >Pertemuan</label>
        </div>
        <?php if(($alasan=='Terlambat, Lainnya') OR ($alasan=='Izin, Lainnya')): ?>
        <div class="input-field col s12">
            <textarea id="textarea1" class="materialize-textarea" name="alasan1" required></textarea>
            <label for="textarea1">Masukkan Keterangan Izin</label>
        </div> 
        <?php else: ?>
            <input type="text" readonly name="" hidden>       
        <?php endif; ?>
        <div class="center col s12">
          <input class="btn blue waves-effect waves-light" type="submit" name="submit" value="OK" onclick="return  confirm('Apakah Data Mata Praktikum dan Pertemuan Sudah Benar ?')">
        </div>
      </form>
    </div>
  </main>