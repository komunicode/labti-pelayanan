  <main>
    <div class="row" style="padding: 20px">
      <div class="center">
        <img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
        <h5>Input Tugas</h5>
      </div>
      <form method="post" action="<?php echo base_url(); ?>user/InputTugasPraktikan" enctype="multipart/form-data">
        <?php foreach($record as $a): ?>
          <div class="input-field col s12 m12">
            <input type="text" value="<?php echo $a['npm']; ?>" readonly>
            <label>NPM Anda</label>
          </div>
          <div class="input-field col s6 m4">
            <input type="text" value="<?php echo $a['nama']; ?>" readonly>
            <label>Nama Anda</label>
          </div>
          <div class="input-field col s6 m4">
            <input type="text" value="<?php echo $a['kelas']; ?>" readonly>
            <label>Kelas Anda</label>
          </div>
          <div class="input-field col s12 m4">
            <input type="text" value="<?php echo $a['no_hp']; ?>" readonly>
            <label>No. Handphone</label>
          </div>
          <div class="input-field col s6 m6">
            <input type="text" required value="<?php echo $a['matprak']; ?>" readonly>
            <label>Praktikum</label>
          </div>
        <div class="input-field col s6 m6">
          <input type="text" required value="<?php echo $a['pertemuan']; ?>" readonly>
          <label>Pertemuan</label>
        </div>
        <div class="input-field col s12">
          <textarea id="textarea1" class="materialize-textarea" required readonly><?php echo $a['alasan']; ?></textarea>
          <label for="textarea1">Alasan</label>
        </div>        
        <div class="file-field input-field col s12 m12">
          <p>
            Tugas :<br>
            <h5><b><?php echo $a['soal']; ?></b></h5>
          </p>
          <div class="btn blue">
            <span>File</span>
            <input type="file" name="tugas1"><!-- tugas -->
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text" placeholder="Upload disini (.pdf | max 10mb)" required name="tugas">
          </div>
        </div>
        <input type="text" readonly name="id_laporan" value="<?php echo $a['id_laporan']; ?>" hidden><!-- id laporan -->
        <?php endforeach; ?>
        <div class="center col s12">
          <input class="blue waves-effect waves-light btn" type="submit" name="submit" value="INPUT TUGAS">
        </div>
      </form>
    </div>    
  </main>