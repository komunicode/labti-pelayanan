<!DOCTYPE html>
<html>
    <head>
        <title>Labti-Pelayanan</title>
        <link href='<?php echo base_url(); ?>assets/img/labti.png' rel='shortcut icon'>

        <!--Import Google Icon Font-->
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/material-icons/material-icons.css" />

        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/materialize.min.css" />

        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <main>
        <div class="container">
            <!-- <div class="row" style="border: 1px solid black">
                <div class="col s4 center ">
                    <div style="margin: 10px">
                        <img style="width: auto; height: 100px;" src="<?php echo base_url(); ?>assets/img/labti.png">    
                    </div>                    
                </div>
                <div class="col s5 center">
                    <div style="padding-top: 20px">
                        <h5><b>Surat Keterangan</b></h5>
                        <h5><b>Laboratorium Teknik Informatika</b></h5> 
                    </div>                    
                </div>
                <div class="col s3 center">
                </div>
                <div class="col s12">
                    <hr>
                </div>
                <?php foreach($record as $a): ?>
                    <div class="col s4 center">
                        <b>Scan Disini</b>        <br>
                        <img src="https://chart.googleapis.com/chart?cht=qr&amp;chs=199x199&amp;chl=<?php echo $a['id_laporan']; ?>">
                    </div>
                    <div class="col s3">
                        <b>Tanggal Lapor</b>        <br>
                        <b>Tanggal Terkonfirmasi</b><br>
                        <b>Nama</b>                 <br>
                        <b>NPM</b>                  <br>
                        <b>Kelas</b>                <br>
                        <b>No. Telp</b>             <br>
                        <b>Praktikum</b>            <br>
                        <b>Pertemuan Ke-</b>        <br>
                        <b>Alasan</b>               <br>
                    </div>
                    <div class="col s3">
                        : <?php echo $a['tgl_lapor']; ?>        <br>
                        : <?php echo $a['tgl_terkonfirmasi']; ?><br>
                        : <?php echo $a['nama']; ?>             <br>
                        : <?php echo $a['npm']; ?>              <br>
                        : <?php echo $a['kelas']; ?>            <br>
                        : <?php echo $a['no_hp']; ?>            <br>
                        : <?php echo $a['matprak']; ?>          <br>
                        : <?php echo $a['pertemuan']; ?>        <br>
                        : <?php echo $a['alasan']; ?>           <br>
                    </div>
                <?php endforeach; ?>
            </div> -->  

            <div class="row" style="border: 1px solid black">
                <div class="col s4 center ">
                    <div style="margin: 10px">
                        <img style="width: auto; height: 100px;" src="<?php echo base_url(); ?>assets/img/labti.png">    
                    </div>                    
                </div>
                <div class="col s5 center">
                    <div style="padding-top: 20px">
                        <h5><b>Surat Keterangan</b></h5>
                        <h5><b>Laboratorium Teknik Informatika</b></h5> 
                    </div>                    
                </div>
                <div class="col s3 center">
                </div>
                <div class="col s12">
                    <hr>
                </div>
                <?php foreach($record as $a): ?><!-- perulangan disini -->
                    <div class="col s3 center">
                        <b>Scan Disini</b>        <br>
                        <img src="https://chart.googleapis.com/chart?cht=qr&amp;chs=199x199&amp;chl=<?php echo $a['id_laporan']; ?>">
                    </div>
                    <div class="col s3">
                        <b>Nama : </b>              <br>
                        <?php echo $a['nama']; ?>   <br>
                        <b>NPM : </b>               <br>
                        <?php echo $a['npm']; ?>    <br>
                        <b>Kelas : </b>             <br>
                        <?php echo $a['kelas']; ?>  <br>
                        <b>No. Handphone : </b>     <br>
                        <?php echo $a['no_hp']; ?>  <br>
                    </div>
                    <div class="col s3">
                        <b>Praktikum : </b>                     <br>
                        <?php echo $a['matprak']; ?>            <br>
                        <b>Pertemuan : </b>                     <br>
                        <?php echo $a['pertemuan']; ?>          <br>
                        <b>Alasan : </b>                        <br>
                        <?php echo $a['alasan']; ?>             <br>
                    </div>
                    <div class="col s3">
                        <b>Tanggal Lapor : </b>                 <br>
                        <?php echo $a['tgl_lapor']; ?>          <br>
                        <b>Tanggal Terkonfirmasi : </b>         <br>
                        <?php echo $a['tgl_terkonfirmasi']; ?>  <br>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <script>
            window.print();
        </script>    

        <!--Import jQuery before materialize.js-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/materialize.min.js"></script>    
    </main>
</html>