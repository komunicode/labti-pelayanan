	<main>
    <div class="row" style="padding: 20px">
      <div class="center">
        <img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
        <h5>Input Laporan</h5>
      </div>
      <form method="post" action="<?php echo base_url(); ?>user/KonfirmasiInputLaporan" enctype="multipart/form-data">
        <?php foreach($user as $a): ?>
          <div class="input-field col s12 m12">
            <input type="text" name="id_user" value="<?php echo $a['id_user']; ?>" readonly hidden><!-- id_user -->
            <input type="text" name="npm" value="<?php echo $a['npm']; ?>" readonly><!-- npm -->
            <label>NPM Anda</label>
          </div>
          <div class="input-field col s6 m6">
            <input type="text" value="<?php echo $a['nama']; ?>" readonly>
            <label>Nama Anda</label>
          </div>
          <div class="input-field col s6 m6">
            <input type="text" value="<?php echo $a['kelas']; ?>" readonly>
            <label>Kelas Anda</label>
          </div>
        <?php endforeach; ?>
        <div class="input-field col s6 m6">
          <input type="text" name="id_matprak" required value="<?php echo $id_matprak ?>" readonly><!-- id_matprak -->
          <label>Praktikum</label>
        </div>
        <div class="input-field col s6 m6">
          <input type="text" name="pertemuan" required value="<?php echo $pertemuan ?>" readonly><!-- pertemuan -->
          <label>Pertemuan</label>
        </div>
        <div class="input-field col s12">
          <textarea id="textarea1" class="materialize-textarea" name="alasan" required readonly><?php echo $alasan ?>, <?php echo $alasan1 ?></textarea>
          <label for="textarea1">Alasan</label><!-- alasan -->
        </div>
        <div class="input-field col s12 m12">
          <input type="text" name="no_hp" required><!-- no_hp -->
          <label>No. Handphone</label>
        </div>

        <?php if($alasan=='Terlambat, Lainnya') : ?>
          <input type="text" readonly name="ktp1" value="" hidden><!-- id tugas -->
          <input type="text" readonly name="ktp" value="" hidden><!-- id tugas -->
          <input type="text" readonly name="surket1" value="" hidden><!-- id tugas -->
          <input type="text" readonly name="surket" value="" hidden><!-- id tugas -->
        <?php elseif($alasan=='Sakit, Rawat Inap') : ?>
          <div class="file-field input-field col s12 m12">
            <p>
              <b>Masukkan Surat Keterangan :</b> <br>
              <b>- Sakit</b><br> 
              Surat Dokter (Rawat Inap) / Surat dari Orang Tua disertai Tanda Tangan (Non-Rawat Inap)<br> 
              <b>- Terlambat</b><br>
              Surat Keterlambatan Kereta / Surat dari Orang Tua disertai Tanda Tangan<br>
              <b>- Izin</b> <br>
                Keluarga (Surat dari Orang Tua disertai Tanda Tangan) <br> 
                Kampus dan Lainnya (Surat Resmi dari Instansi yang Bersangkutan)
            </p>
            <div class="btn blue">
              <span>File</span>
              <input type="file" name="surket1"><!-- surket -->
            </div>
            <div class="file-path-wrapper">
              <input class="file-path validate" type="text" placeholder="Upload disini (.jpg | max 5mb)" required name="surket">
            </div>
          </div>
          <input type="text" readonly name="ktp1" value="" hidden><!-- id tugas -->
          <input type="text" readonly name="ktp" value="" hidden><!-- id tugas -->
        <?php else : ?>
          <div class="file-field input-field col s12 m6">
            <p>
              <b>Masukkan Surat Keterangan :</b> <br>
              <b>- Sakit</b><br> 
              Surat Dokter (Rawat Inap) / Surat dari Orang Tua disertai Tanda Tangan (Non-Rawat Inap)<br> 
              <b>- Terlambat</b><br>
              Surat Keterlambatan Kereta / Surat dari Orang Tua disertai Tanda Tangan<br>
              <b>- Izin</b> <br>
                Keluarga (Surat dari Orang Tua disertai Tanda Tangan) <br> 
                Kampus dan Lainnya (Surat Resmi dari Instansi yang Bersangkutan)
            </p>
            <div class="btn blue">
              <span>File</span>
              <input type="file" name="surket1"><!-- surket -->
            </div>
            <div class="file-path-wrapper">
              <input class="file-path validate" type="text" placeholder="Upload disini (.jpg | max 5mb)" required name="surket">
            </div>
          </div>
          <div class="file-field input-field col s12 m6">
            <p><b>Masukkan KTP :</b><br>
            Orang Tua / Wali (Sakit/Terlambat/Izin) </p>
            <div class="btn blue">
              <span>File</span>
              <input type="file" name="ktp1"><!-- ktp -->
            </div>
            <div class="file-path-wrapper">
              <input class="file-path validate" type="text" placeholder="Upload disini (.jpg | max 5mb)" required name="ktp">
            </div>
          </div>
        <?php endif; ?>


        <input type="text" readonly name="id_tugas" value="<?php echo $id_tugas; ?>" hidden><!-- id tugas -->
        <div class="center col s12">
          <input class="blue waves-effect waves-light btn" type="submit" name="submit" value="INPUT LAPORAN">
        </div>
      </form>
    </div>
  </main>