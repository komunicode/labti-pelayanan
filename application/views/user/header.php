<!DOCTYPE html>
<html>
<head>
	<title>Labti-Pelayanan</title>
	<link href='<?php echo base_url(); ?>assets/img/labti.png' rel='shortcut icon'>

	<!--Import Google Icon Font-->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/material-icons/material-icons.css" />

	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/materialize.min.css" />
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/u_custom.css" />

	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<!-- header -->	
	<div class="navbar-fixed">
		<nav class="z-depth-3" style="background-color: #0f3057;">
		    <div class="nav-wrapper">
		    	<a href="<?php echo base_url(); ?>user" class="brand-logo" style="padding: 10px"><img style="width: 60px" src="<?php echo base_url(); ?>assets/img/labti.png"></a>
	      		<a href="#!" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
	      		<ul class="right hide-on-med-and-down">
	        		<li><a href="<?php echo base_url(); ?>user/logout"><i class="material-icons left tooltipped" data-position="left" data-delay="10" data-tooltip="Logout">power_settings_new</i></a></li>
	      		</ul>	      	
	    	</div>
		</nav>
	</div>
	<ul class="side-nav fixed" id="mobile-demo" style="width: 250px">
		<div class="center z-depth-3 white-text" style="background-color: #0f3057;">
			<img style="width: 100px;margin: 10px" src="<?php echo base_url(); ?>assets/img/labti.png">
			<?php foreach($user as $a): ?>
			<h6><b><?php echo $a['nama']; ?></b></h6>
			<h6><?php echo $a['npm']; ?></h6>
			<h6><?php echo $a['kelas']; ?></h6>
			<?php endforeach; ?>
			<br>
	    </div>
		<li><a href="<?php echo base_url(); ?>user" class="collapsible-header waves-effect waves-teal"><i class="material-icons left">home</i>Halaman Utama</a></li>
		<li><a href="<?php echo base_url(); ?>user/laporan_pilihkategori" class="collapsible-header waves-effect waves-teal truncate"><i class="material-icons left">assignment</i>Lapor Ketidakhadiran</a></li>
		<li><a href="<?php echo base_url(); ?>user/input_kartuhilang" class="collapsible-header waves-effect waves-teal truncate"><i class="material-icons left">phonelink_erase</i>Lapor Kartu Hilang</a></li>
	    <li><a href="<?php echo base_url(); ?>user/about" class="collapsible-header waves-effect waves-teal"><i class="material-icons left">info</i>Tentang Kami</a></li>
	    <li><a href="<?php echo base_url(); ?>user/contact" class="collapsible-header waves-effect waves-teal"><i class="material-icons left">phone</i>Hubungi Kami</a></li>
	    <li><a href="<?php echo base_url(); ?>user/logout" class="collapsible-header waves-effect waves-teal"><i class="material-icons left">power_settings_new</i>Logout</a></li>
	</ul>
