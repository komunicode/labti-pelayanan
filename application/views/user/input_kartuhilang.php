<main>
  <div class="row" style="padding: 20px;">

    <div class="center">
    	<img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
	    <h4>Prosedur Kartu Hilang</h4>
    </div>

    <div class="container">
    	<p>1. Kelengkapan dokumen yang harus dilengkapi : </p>
	    	<p>- <b>Surat Pernyataan Kartu Hilang</b></p>
    	<p>2. <b>Surat Pernyataan Kartu Hilang</b> dengan syarat sebagai berikut :</p>
	    	<p>- Surat Pernyataan Kartu Hilang yang harus dilengkapi dengan <b>data diri, tanda tangan praktikan yang bersangkutan, ketua kelas praktikan tersebut dan Kepala Laboratorium Teknik Informatika.</b></p>
	    	<p>- Surat Pernyataan Kartu Hilang bisa di download <b><a href="https://drive.google.com/uc?id=0B9F2KANc7GjyaUdkbXhUMmhEdGs&export=download" target="_blank">di sini</a>.</b></p>
      <p>3. Mengunjungi ruang pelayanan LabTI di gedung E535 dengan membawa <b>Surat Pernyataan Kartu Hilang</b> untuk memkonfirmasi laporan</p>
    	<p>4. Mengisi data <b>laporan kehilangan kartu</b> di bawah ini : </p>
    	<form method="post" action="<?php echo base_url(); ?>user/inputlaporan_kartuhilang" enctype="multipart/form-data">
        <?php foreach($user as $a): ?>
          <div class="input-field col s12">
            <input type="text" name="iduser" value="<?php echo $a['id_user']; ?>" readonly hidden>
            <input type="text" name="npm" value="<?php echo $a['npm']; ?>" readonly>
            <label>NPM Anda</label>
          </div>
          <div class="input-field col s6">
            <input type="text" value="<?php echo $a['nama']; ?>" readonly>
            <label>Nama Anda</label>
          </div>
          <div class="input-field col s6">
            <input type="text" value="<?php echo $a['kelas']; ?>" readonly>
            <label>Kelas Anda</label>
          </div>
        <?php endforeach; ?>
        <div class="input-field col s12">
          <input type="text" name="no_hp" required>
          <label>No. Handphone</label>
        </div>    
        <div class="center col s12">
          <input class="blue waves-effect waves-light btn" type="submit" name="submit" value="KIRIM LAPORAN">
        </div>
      </form>
    </div>

    

  </div>
</main>