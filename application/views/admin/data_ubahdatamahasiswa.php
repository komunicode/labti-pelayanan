<main>
	<div class="row" style="padding: 59px">
		<div class="center">
			<img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
			<h5>Ubah Data Mahasiswa</h5>
		</div>
		<form method="post" action="<?php echo base_url(); ?>admin/UbahDataMahasiswa" enctype="multipart/form-data">
			<?php foreach ($record as $a): ?>
				<div class="input-field col s12">
		    		<input type="text" name="npm" required readonly value="<?php echo $a['npm'] ?>">
		    		<label >NPM</label>
		    	</div>
		    	<div class="input-field col s12">
		    		<input type="text" name="nama" required value="<?php echo $a['nama'] ?>">
		    		<label >Nama</label>
		    	</div>
		    	<div class="input-field col s6">
		    		<input type="text" name="angkatan" required value="<?php echo $a['angkatan'] ?>">
		    		<label >Angkatan</label>
		    	</div>
		    	<div class="input-field col s6">
		    		<input type="text" name="kelas" required value="<?php echo $a['kelas'] ?>">
		    		<label >Kelas</label>
		    	</div>
			<?php endforeach ?>	    	
	    	<div class="center col s12">
              <input class="waves-effect waves-light btn" type="submit" name="submit" value="UBAH DATA MAHASISWA" onclick="return  confirm('Data Mahasiswa Sudah Benar ?')">
            </div>
    	</form>
    </div>
</main>