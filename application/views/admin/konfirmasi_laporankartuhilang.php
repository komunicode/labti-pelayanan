<main>
	<div class="row" style="padding: 59px">
		<div class="center">
			<img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
			<h5>Konfirmasi Laporan Kartu Hilang</h5>
		</div>
		<form method="post" action="<?php echo base_url(); ?>admin/KonfirmasiLaporanKartuHilang" enctype="multipart/form-data">
			<?php foreach($record as $a): ?><!-- perulangan disini -->
	    	<div class="input-field col s6">
	    		<input readonly type="text" value="<?php echo $a['id_laporan']; ?>" name="id_laporan">
                <label >ID Laporan Kartu Hilang</label>
			</div>
			<div class="input-field col s6">
            	<input readonly type="text" value="<?php echo $a['tgl_lapor']; ?>">
				<label >Tanggal Lapor</label>
			</div>
			<div class="input-field col s3">
            	<input readonly type="text" value="<?php echo $a['nama']; ?>">
				<label >Nama</label>
			</div>
            <div class="input-field col s3">
            	<input readonly type="text" value="<?php echo $a['npm']; ?>">
                <label >NPM</label>
			</div>
            <div class="input-field col s3">
            	<input readonly type="text" value="<?php echo $a['kelas']; ?>">
                <label >Kelas</label>
			</div>
			<div class="input-field col s3">
            	<input readonly type="text" value="<?php echo $a['no_hp']; ?>">
                <label >No. Handphone</label>
			</div>
	    	<?php endforeach; ?><!-- akhir perulangan -->	
	    	<div class="center col s12">
              <input class="waves-effect waves-light btn" type="submit" name="submit" value="KONFIRMASI LAPORAN" onclick="return  confirm('Terima Laporan ?')">
            </div>
    	</form>
    </div>
</main>