  <main>
	
	<!-- bagian pelayanan -->
  	<div class="section z-depth-5" style="padding: 10px">
  		<div class="center">
  			<img style="width: 100px; height: auto" src="<?php echo base_url(); ?>assets/img/labti.png">
  			<h5><b>Pelayanan Laboratorium Teknik Informatika</b></h5>
    	</div>
    	<div class="col s12">
    		<div class="row">

    			<div class="col s12">
    				<ul class="collapsible popout" data-collapsible="accordion">
    					<li>
    						<div class="collapsible-header active" style="padding: 10px">
    							<i class="material-icons">filter_drama</i>
    							<h5><b>PELAYANAN</b></h5>
    						</div>
			    			<div class="collapsible-body row blue-text" style="padding: 10px">
			    				<div class="col s12 m3" style="padding: 5px">
			    					<i class="small material-icons">move_to_inbox</i>	
			    					<?php $a = count($lap_masuk) ?>
			    					<a href="<?php echo base_url(); ?>admin/laporan_masuk"><b><?php echo "$a"; ?> - Laporan Masuk</b></a>
			    				</div>
			    				<div class="col s12 m3" style="padding: 5px">
									<i class="small material-icons">assignment_turned_in</i>	
									<?php $a = count($lap_terkonfirmasi) ?>
			    					<a href="<?php echo base_url(); ?>admin/laporan_terkonfirmasi"><b><?php echo "$a"; ?> - Laporan Terkonfirmasi</b></a>
			    				</div>
			    				<div class="col s12 m3" style="padding: 5px">
			    					<i class="small material-icons">assignment_late</i>	
			    					<?php $a = count($lap_ditolak) ?>
			    					<a href="<?php echo base_url(); ?>admin/laporan_tidakterkonfirmasi"><b><?php echo "$a"; ?> - Laporan Ditolak</b></a>
			    				</div>
			    				<div class="col s12 m3" style="padding: 5px">
			    					<i class="small material-icons">phonelink_erase</i>	
			    					<?php $a = count($lap_kartuhilang) ?>
			    					<a href="<?php echo base_url(); ?>admin/laporan_kartuhilang"><b><?php echo "$a"; ?> - Laporan Kartu Hilang</b></a>
			    				</div>
			    				<div class="col s12 m3" style="padding: 5px">
			    					<i class="small material-icons">swap_horiz</i>	
			    					<?php $a = count($lap_praktrans) ?>
			    					<a href="<?php echo base_url(); ?>admin/transfer_praktikantransfer"><b><?php echo "$a"; ?> - Praktikan Transfer</b></a>
			    				</div>
			    				<div class="col s12 m3" style="padding: 5px">
			    					<i class="small material-icons">star</i>	
			    					<a href="<?php echo base_url(); ?>admin/case_laporankhusus"><b>Laporan Khusus Praktikan</b></a>
			    				</div>
			    				<div class="col s12 m3" style="padding: 5px">
			    					<i class="small material-icons">print</i>	
			    					<a href="<?php echo base_url(); ?>admin/cetak_laporanpelayanan"><b>Cetak Laporan Pelayanan</b></a>
			    				</div>
			    			</div>
			    		</li>
			    	</ul>
			    </div>
			    <div class="col s12">
		    		<ul class="collapsible popout" data-collapsible="accordion">
			    		<li>
			    			<div class="collapsible-header active" style="padding: 10px">
			    				<i class="material-icons">filter_drama</i>
			    				<h5><b>USER</b></h5>
			    			</div>
			    			<div class="collapsible-body row blue-text" style="padding: 10px">
			    				<div class="col s12 m3" style="padding: 5px">
			    					<i class="small material-icons">person</i>	
			    					<?php $a = count($praktikan) ?>
			    					<a href="<?php echo base_url(); ?>admin/user/1"><b><?php echo "$a"; ?> - Praktikan</b></a>
			    				</div>
			    				<div class="col s12 m3" style="padding: 5px">
			    					<i class="small material-icons">person_pin</i>	
			    					<?php $a = count($at) ?>
			    					<a href="<?php echo base_url(); ?>admin/user/2"><b><?php echo "$a"; ?> - Asisten Tetap</b></a>
			    				</div>
			    				<div class="col s12 m3" style="padding: 5px">
			    					<i class="small material-icons">supervisor_account</i>	
			    					<?php $a = count($pj) ?>
			    					<a href="<?php echo base_url(); ?>admin/user/3"><b><?php echo "$a"; ?> - PJ/Tutor</b></a>
			    				</div>
			    				<div class="col s12 m3" style="padding: 5px">
			    					<i class="small material-icons">people</i>	
			    					<?php $a = count($mhs) ?>
			    					<a href="<?php echo base_url(); ?>admin/data_datamahasiswa"><b><?php echo "$a"; ?> - Data Mahasiswa</a>
			    				</div>
			    			</div>
			    		</li>
			    	</ul>
			    </div>
			    <div class="col s12">
		    		<ul class="collapsible popout" data-collapsible="accordion">
			    		<li>
			    			<div class="collapsible-header active" style="padding: 10px">
			    				<i class="material-icons">filter_drama</i>
			    				<h5><b>PRAKTIKUM</b></h5>
			    			</div>
			    			<div class="collapsible-body row blue-text" style="padding: 10px">
			    				<div class="col s12 m3" style="padding: 5px">
			    					<i class="small material-icons">import_contacts</i>	
			    					<?php $a = count($mp) ?>
			    					<a href="<?php echo base_url(); ?>admin/matprak_pilihtingkat"><b><?php echo "$a"; ?> - Mata Praktikum</b></a>
			    				</div>
			    				<div class="col s12 m4" style="padding: 5px">
			    					<i class="small material-icons">library_books</i>	
			    					<?php $a = count($tugas) ?>
			    					<a href="<?php echo base_url(); ?>admin/tugas_praktikan"><b><?php echo "$a"; ?> - Tugas Praktikan</b></a>
			    				</div>
			    			</div>
			    		</li>
			    	</ul>
			    </div>
			</div>
		</div>
  	</div>

  	<!-- parallax -->
	<div class="parallax-container z-depth-5">
    	<div class="parallax">
    		<img src="<?php echo base_url(); ?>assets/img/6.jpg">
    	</div>
	    <!-- Text -->
	    <div class="container">
	     	<br><br>
	     	<div class="center">
				<img style="width: 200px" src="<?php echo base_url(); ?>assets/img/labti.png">
		    </div>
	     	<h1 class="header center white-text text-lighten-2">Staff / Asisten Tetap</h1>
	     	<div class="header center white-text text-lighten-2">
	     	<?php foreach($user as $a): ?><!-- perulangan disini -->
				<h5><?php echo $a['nama']; ?></h5>
	     	<?php endforeach; ?><!-- akhir perulangan -->
	     	</div>
	     	<br><br>
	   	</div>
	</div>

  </main>