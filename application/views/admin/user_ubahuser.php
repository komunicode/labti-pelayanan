<main>
	<div class="row" style="padding: 59px">
		<div class="center">
			<img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
			<h5>Edit Data User</h5>
		</div>
		<form method="post" action="<?php echo base_url(); ?>admin/UserUpdateUser">
			<?php foreach($record as $a): ?><!-- perulangan disini -->
	    	<div class="input-field col s6">
	    		<input class="red" type="text" name="id_user" required value="<?php echo $a['id_user']; ?>" readonly>
	    		<label >ID User</label>
	    	</div>
	    	<div class="input-field col s6">
	    		<?php if($a['level']==1): ?>
	    			<input class="red" type="text" required value="Praktikan" readonly>	
	    		<?php elseif($a['level']==2): ?>
	    			<input class="red" type="text" required value="Asisten Tetap" readonly>	
	    		<?php elseif($a['level']==3): ?>
	    			<input class="red" type="text" required value="PJ / Tutor" readonly>	
	    		<?php endif; ?>	    		
	    		<label >Tipe User</label>
	    	</div>
	    	<div class="input-field col s6">
	    		<input class="red" type="text" required value="<?php echo $a['username']; ?>" readonly>
	    		<label >Username</label>
	    	</div>
	    	<div class="input-field col s6">
	    		<input type="text" required name="password" value="<?php echo $a['password']; ?>">
	    		<label >Password</label>
	    	</div>
	    	<div class="input-field col s3">
	    		<input type="text" name="nama" required value="<?php echo $a['nama']; ?>" >
	    		<label >Nama</label>
	    	</div>
	    	<div class="input-field col s3">
	    		<input class="red" type="text" name="npm" required value="<?php echo $a['npm']; ?>" readonly>
	    		<label >NPM</label>
	    	</div>
	    	<div class="input-field col s3">
	    		<input type="text" name="kelas" required value="<?php echo $a['kelas']; ?>">
	    		<label >Kelas</label>
	    	</div>
	    	<div class="input-field col s3">
	    		<input type="text" name="angkatan" required value="<?php echo $a['angkatan']; ?>">
	    		<label >Angkatan</label>
	    	</div>	    	
	    	<?php endforeach; ?><!-- akhir perulangan -->	
	    	<div class="center col s12">
              <input class="waves-effect waves-light btn" type="submit" name="submit" value="UPDATE USER">
            </div>
    	</form>
    </div>
</main>