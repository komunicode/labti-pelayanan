<main>
  <div style="margin: 10px">
    <div class="center">
      <img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
      <h4 class="header center text-lighten-2">Mata Praktikum LabTI</h4>
    </div>
    <div class="row">
      <?php if($record!=null): ?>
        <?php foreach($record as $a): ?><!-- perulangan disini -->
          <a href="<?php echo base_url(); ?>admin/matprak_lihatpraktikum/<?php echo $a['id_matprak']; ?>">
            <div class="col s12 m3 center hoverable z-depth-2" style="padding: 10px;">
              <img style="width: 200px" src="<?php echo base_url(); ?>assets/img/labti.png">  
              <div class="row black-text" style="text-align: left;">
                <div class="col s12" style="margin: 5px">
                  <b class="truncate"><i class="material-icons">import_contacts</i><?php echo $a['matprak']; ?></b>
                </div>
                <div class="col s12" style="margin: 5px">
                  <i class="material-icons">assignment</i>
                  <b><?php echo $a['jum_pertemuan']; ?> Pertemuan</b>
                </div>
                <div class="col s12" style="margin: 5px">
                  <i class="material-icons">class</i>
                  <b>Tingkat - <?php echo $a['tingkat']; ?></b>
                </div>
              </div>
            </div>
          </a>
        <?php endforeach; ?><!-- akhir perulangan -->
      <?php else: ?>
        <div class="center">
          <div class="z-depth-2" style="margin: 50px; padding: 50px 0px 50px 0px">
            <h5><b>Mata Praktikum Belum Tersedia ... </b></h5>  
            <a href="<?php echo base_url(); ?>admin/matprak_pilihtingkat">kembali ... </a>
          </div>          
        </div>
      <?php endif; ?>
    </div>
  </div>
  <div class="fixed-action-btn">
    <a class="btn-floating tooltipped btn-large green z-depth-4 " data-position="left" data-delay="10" data-tooltip="Tambah Mata Praktikum" href="<?php echo base_url(); ?>admin/matprak_tambahmatapraktikum">
      <i class="material-icons">add</i>
    </a>
  </div>
</main>