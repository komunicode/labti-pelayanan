<main>
	<div class="row" style="padding: 59px">
		<div class="center">
			<img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
			<h5>Tambah Praktikan Transfer</h5>
		</div>
		<form method="post" action="<?php echo base_url(); ?>admin/TambahPraktikanTransfer" enctype="multipart/form-data">
			<div class="input-field col s4">
	    		<input type="text" name="nama" value="<?php echo $nama; ?>" required readonly>
	    		<label >Nama</label>
	    	</div>	    	
	    	<div class="input-field col s4">
	    		<input type="text" name="npm" value="<?php echo $npm; ?>" required readonly>
	    		<label >NPM</label>
	    	</div>	    	
	    	<div class="input-field col s4">
	    		<input type="text" name="no_hp" required>
	    		<label >No. Handphone</label>
	    	</div>
	    	<div class="input-field col s12">
	    		<textarea id="textarea1" class="materialize-textarea" name="alasan" required></textarea>
	    		<label for="textarea1">Alasan</label>
	    	</div>
	    	<div class="input-field col s6">
	    		<input type="text" name="username" required>
	    		<label >Username</label>
	    	</div>
	    	<div class="input-field col s6">
	    		<input type="password" name="password" required>
	    		<label >Password</label>
	    	</div>
	    	<div class="input-field col s3">
	            <select class='form-control' name="praktikum" required>
					<option value="" disabled selected>--Mata Praktikum--</option>
	              	<?php foreach($mpt as $a): ?>
					<option value="<?php echo $a['id_matprak']; ?>"><?php echo $a['matprak']; ?></option>
					<?php endforeach; ?><!-- akhir perulangan -->
	            </select>
	            <label >Praktikum Transfer</label>
	        </div>
	        <div class="input-field col s3">
	            <select class='form-control' name="kelas_baru" required>
					<option value="" disabled selected>--Pilih Kelas--</option>
	              	<?php foreach($kt as $a): ?>
					<option value="<?php echo $a['kelas']; ?>"><?php echo $a['kelas']; ?></option>
					<?php endforeach; ?><!-- akhir perulangan -->
	            </select>
	            <label >Kelas Baru</label>
	        </div>
	    	<div class="input-field col s3">
	    		<input type="text" name="pertemuan" required>
	    		<label >Pertemuan</label>
	    	</div>
	    	<div class="input-field col s3">
	    		<input type="text" name="npm_baru" required>
	    		<label >NPM Baru</label>
	    	</div>
	    	<div class="center col s12">
              <input class="waves-effect waves-light btn" type="submit" name="submit" value="TAMBAH PRAKTIKAN TRANSFER">
            </div>
    	</form>
    </div>
</main>