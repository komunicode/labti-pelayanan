	<main>
    <div class="row" style="padding: 20px">
      <div class="center">
        <img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
        <h5>Input Laporan Khusus</h5>
      </div>
      <form method="post" action="<?php echo base_url(); ?>admin/InputLaporanKhusus" enctype="multipart/form-data">
        <?php foreach($praktikankhusus as $a): ?>
          <div class="input-field col s12 m12">
            <input type="text" name="id_user" value="<?php echo $a['id_user']; ?>" readonly> <!-- id user -->
            <label>ID Praktikan</label>
          </div>
          <div class="input-field col s12 m3">
            <input type="text" value="<?php echo $a['nama']; ?>" readonly>
            <label>Nama</label>
          </div>
          <div class="input-field col s12 m3">
            <input type="text" value="<?php echo $a['npm']; ?>" readonly>
            <label>NPM</label>
          </div>
          <div class="input-field col s12 m3">
            <input type="text" name="kelas" value="<?php echo $a['kelas']; ?>" readonly> <!-- kelas -->
            <label>Kelas</label>
          </div>
          <div class="input-field col s12 m3">
            <input type="text" value="<?php echo $a['angkatan']; ?>" readonly>
            <label>Angkatan</label>
          </div>
        <?php endforeach; ?>
        <div class="input-field col s12 m12">
          <input type="text" name="no_hp" required> <!-- no hp -->
          <label>No. Handphone</label>
        </div>
        <div class="input-field col s12 m6">
          <select class='form-control' name="id_matprak" required> <!-- id matprak -->
            <option value="" disabled selected>--Mata Praktikum--</option>
              <?php foreach($mpt as $b): ?>
                <option value="<?php echo $b['id_matprak']; ?>"><?php echo $b['matprak']; ?></option>
              <?php endforeach; ?><!-- akhir perulangan -->
            </select>
          <label >Praktikum Transfer</label>
        </div>
        <div class="input-field col s12 m6">
          <select class='form-control' name="pertemuan" required> <!-- pertemuan -->
            <option value="" disabled selected>--Pilih Pertemuan--</option>
              <?php for ($i=1; $i<9 ; $i++):?>
                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
              <?php endfor; ?><!-- akhir perulangan -->
          </select>
          <label >Pertemuan</label>
        </div>
        <div class="input-field col s12 m12">
          <textarea id="textarea1" class="materialize-textarea" name="catatan" required></textarea> <!-- Catatan -->
          <label>Catatan</label>
        </div>

        <div class="file-field input-field col s12 m4">
          <p>
            <b>Masukkan Surat Keterangan :</b>
          </p>
          <div class="btn">
            <span>File</span>
            <input type="file" name="surket1"><!-- surket -->
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text" placeholder="Upload disini (.jpg | max 5mb)" required name="surket">
          </div>
        </div>

        <div class="file-field input-field col s12 m4">
          <p>
            <b>Masukkan KTP :</b>
          </p>
          <div class="btn">
            <span>File</span>
            <input type="file" name="ktp1">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text" placeholder="Upload disini (.jpg | max 5mb)" required name="ktp">
          </div>
        </div>

        <div class="file-field input-field col s12 m4">
          <p>
            <b>Masukkan Surat Lainnya (Opsional) :</b>
          </p>
          <div class="btn">
            <span>File</span>
            <input type="file" name="surketlain1">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text" placeholder="Upload disini (.jpg | max 5mb)" name="surketlain">
          </div>
        </div>

        <div class="center col s12">
          <input class="waves-effect waves-light btn" type="submit" name="submit" value="INPUT LAPORAN KHUSUS">
        </div>
      </form>
    </div>
  </main>