<main>
  <div class="row z-depth-5" style="padding: 10px; margin: 20px">
    <div class="center">
      <img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
      <h4>Pilih Tingkat</h4>
    </div>
    <ul class="collapsible popout" data-collapsible="accordion">
      <li>
        <div class="collapsible-header">
          <h5><img style="width: 50px" src="<?php echo base_url(); ?>assets/img/labti.png"><b>Tingkat 1</b></h5>
        </div>
        <div class="collapsible-body">
          <div class="row">
            <div class="col s6" style="padding: 5px">
              <span>Semester 1 - 2</span>
            </div>
            <div class="col s6" style="text-align: right; padding: 10px">
              <a class="btn-floating green click-to-toggle z-depth-4" href="<?php echo base_url(); ?>admin/matprak_pilihmatprak/1">
                <i class="material-icons">arrow_forward</i>
              </a>
            </div>
          </div>
        </div>
      </li>

      <li>
        <div class="collapsible-header">
          <h5><img style="width: 50px" src="<?php echo base_url(); ?>assets/img/labti.png"><b>Tingkat 2</b></h5>
        </div>
        <div class="collapsible-body">
          <div class="row">
            <div class="col s6" style="padding: 5px">
              <span>Semester 3 - 4</span>
            </div>
            <div class="col s6" style="text-align: right; padding: 10px">
              <a class="btn-floating green click-to-toggle z-depth-4" href="<?php echo base_url(); ?>admin/matprak_pilihmatprak/2">
                <i class="material-icons">arrow_forward</i>
              </a>
            </div>
          </div>
        </div>
      </li>

      <li>
        <div class="collapsible-header">
          <h5><img style="width: 50px" src="<?php echo base_url(); ?>assets/img/labti.png"><b>Tingkat 3</b></h5>
        </div>
        <div class="collapsible-body">
          <div class="row">
            <div class="col s6" style="padding: 5px">
              <span>Semester 5 - 6</span>
            </div>
            <div class="col s6" style="text-align: right; padding: 10px">
              <a class="btn-floating green click-to-toggle z-depth-4" href="<?php echo base_url(); ?>admin/matprak_pilihmatprak/3">
                <i class="material-icons">arrow_forward</i>
              </a>
            </div>
          </div>
        </div>
      </li>

      <li>
        <div class="collapsible-header">
          <h5><img style="width: 50px" src="<?php echo base_url(); ?>assets/img/labti.png"><b>Tingkat 4</b></h5>
        </div>
        <div class="collapsible-body">
          <div class="row">
            <div class="col s6" style="padding: 5px">
              <span>Semester 7 - 8</span>
            </div>
            <div class="col s6" style="text-align: right; padding: 10px">
              <a class="btn-floating green click-to-toggle z-depth-4" href="<?php echo base_url(); ?>admin/matprak_pilihmatprak/4">
                <i class="material-icons">arrow_forward</i>
              </a>
            </div>
          </div>
        </div>
      </li>
    </ul>
  </div>
</main>