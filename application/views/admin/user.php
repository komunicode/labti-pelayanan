<main>
	<!-- tabel user -->
    <div class="section z-depth-5" style="margin: 50px">
      <div style="padding: 10px">
        <div class="center">
          <img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
          <?php if ($level == '1'): ?>
            <h4 class="header center text-lighten-2">Data Praktikan</h4>
          <?php elseif ($level == '2'): ?>
            <h4 class="header center text-lighten-2">Data Asisten Tetap</h4>
          <?php elseif ($level == '3'): ?>
            <h4 class="header center text-lighten-2">Data PJ</h4>
          <?php endif ?>
        </div>
        <table id="example" class="highlight centered">
          <thead>
            <tr>
              <?php if(($level == 1) OR ($level ==2)): ?>  
                <th>No</th>
                <th>ID User</th>
                <th>Nama</th>
                <th>Kelas</th>
                <th>Action</th>
              <?php elseif($level ==3): ?>  
                <th>No</th>
                <th>ID User</th>
                <th>Nama</th>
                <th>Kelas</th>
                <th>Status Scanner</th>
                <th>Action</th>
              <?php endif; ?>
            </tr>
          </thead>
          <tbody>
            <?php if(($level == 1) OR ($level ==2)): ?>
              <?php $no=1; foreach($record as $a): ?><!-- perulangan disini -->
              <tr class="z-depth-1">
                <td><?php echo $no; ?></td>
                <td><?php echo $a['id_user']; ?></td>
                <td><?php echo $a['nama']; ?></td>
                <td><?php echo $a['kelas']; ?></td>
                <td>
                  <a href="#<?php echo $a['id_user']; ?>" class="btn-floating tooltipped waves-effect waves-light green  z-depth-4" data-position="buttom" data-delay="10" data-tooltip="Lihat User"><i class="material-icons">launch</i></a>
                </td>
              </tr>
              <?php $no++; endforeach; ?><!-- akhir perulangan -->
            <?php elseif($level == 3): ?>
              <?php $no=1; foreach($record as $a): ?><!-- perulangan disini -->
              <tr class="z-depth-1">
                <td><?php echo $no; ?></td>
                <td><?php echo $a['id_user']; ?></td>
                <td><?php echo $a['nama']; ?></td>
                <td><?php echo $a['kelas']; ?></td>
                <?php if($a['akses']==1): ?>
                  <td><a href="<?php echo base_url(); ?>admin/akses_aktif/<?php echo $a['id_user']; ?>"><b class="red-text">Non-Aktif</b></a></td>
                <?php elseif($a['akses']==2): ?>
                  <td><a href="<?php echo base_url(); ?>admin/akses_nonaktif/<?php echo $a['id_user']; ?>"><b class="green-text">Aktif</b></a></td>
                <?php endif; ?>
                <td>
                  <a href="#<?php echo $a['id_user']; ?>" class="btn-floating tooltipped waves-effect waves-light green darken-4 z-depth-4" data-position="buttom" data-delay="10" data-tooltip="Lihat User"><i class="material-icons">launch</i></a>
                </td>
              </tr>
              <?php $no++; endforeach; ?><!-- akhir perulangan -->
            <?php endif; ?>
          </tbody>
        </table>
        <?php if($level==3): ?>
          <div class="center">
            <a href="<?php echo base_url(); ?>admin/akses_nonaktifall" class="red waves-effect waves-light btn">Matikan Semua Akses Scanner</a>  
          </div>
        <?php endif; ?>
      </div>
    </div>

    <!-- modal launch -->
    <?php foreach($record as $b): ?><!-- perulangan disini -->
    <div id="<?php echo $b['id_user']; ?>" class="modal modal-fixed-footer">
      <div class="modal-content">
        <!-- judul -->
        <div class="center">
          <img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
          <?php if ($b['level'] == '1'): ?>
            <h4 class="header center text-lighten-2">Data Praktikan</h4>
          <?php elseif ($b['level'] == '2'): ?>
            <h4 class="header center text-lighten-2">Data Asisten Tetap</h4>
          <?php elseif ($b['level'] == '3'): ?>
            <h4 class="header center text-lighten-2">Data PJ</h4>
          <?php endif ?>
        </div>
        <!-- bagian penampil data -->
        <div class="row">
	        <div class="input-field col s12">
	        	<input readonly type="text" value="<?php echo $b['id_user']; ?>">
	          <label >ID / NPM User</label>
	        </div>
          <div class="input-field col s6">
				    <input readonly type="text" value="<?php echo $b['username']; ?>">
		    	  <label >Username</label>
	    	  </div>
          <div class="input-field col s6">
            <input readonly type="text" value="<?php echo $b['password']; ?>">
            <label >Password</label>
          </div>
	    	  <div class="input-field col s3">
            <input readonly type="text" value="<?php echo $b['nama']; ?>">
            <label >Nama</label>
	    	  </div>
          <div class="input-field col s3">
            <input readonly type="text" value="<?php echo $b['npm']; ?>">
            <label >NPM</label>
          </div>
          <div class="input-field col s3">
            <input readonly type="text" value="<?php echo $b['kelas']; ?>">
            <label >Kelas</label>
          </div>
	    	  <div class="input-field col s3">
            <input readonly type="text" value="<?php echo $b['angkatan']; ?>">
            <label >Angkatan</label>
	    	  </div>
	    	  
  	    	<div class="input-field col s12">
            <?php if($b['level']==1): ?>
              <input readonly type="text" value="Praktikan">
            <?php elseif($b['level']==2): ?>
              <input readonly type="text" value="AT">
            <?php elseif($b['level']==3): ?>
              <input readonly type="text" value="PJ">
            <?php endif; ?>
  			    <label >Tipe User</label>
  	    	</div>
        </div>
      </div>
      <!-- action modal launch -->
      <div class="modal-footer">
        <a href="<?php echo base_url(); ?>admin/user_hapususer/<?php echo $b['id_user']; ?>" class="modal-action modal-close waves-effect waves-green btn-flat " onclick="return  confirm('Hapus Data Mahasiswa ?')">Hapus Data</a>
        <a href="<?php echo base_url(); ?>admin/user_ubahuser/<?php echo $b['id_user']; ?>" class="modal-action modal-close waves-effect waves-green btn-flat ">Ubah Data</a>
      </div>
    </div>
    <?php endforeach; ?><!-- akhir perulangan -->	

    <div class="fixed-action-btn">
	    <a class="btn-floating tooltipped btn-large green z-depth-4 " data-position="left" data-delay="10" data-tooltip="Tambah User" href="<?php echo base_url(); ?>admin/user_tambahuser">
	      <i class="large material-icons">person_add</i>
	    </a>
    </div>
</main>