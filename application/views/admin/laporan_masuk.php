<main>
  <!-- tabel user -->
    <div class="section z-depth-5" style="margin: 50px">
      <div style="padding: 10px">
        <div class="center">
          <img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
          <h4 class="header center text-lighten-2">Data Laporan Masuk</h4>
        </div>
        <table id="example" class="highlight centered">
          <thead>
            <tr>
              <th>No</th>
              <th>NPM</th>
              <th>Nama</th>
              <th>Kelas</th>
              <th>Tanggal Urus</th>
              <th>Mata Praktikum</th>
              <th>Pertemuan</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($record as $a): ?><!-- perulangan disini -->
              <tr class="z-depth-1">
                <td><?php echo $no; ?></td>
                <td><?php echo $a['npm']; ?></td>
                <td><?php echo $a['nama']; ?></td>
                <td><?php echo $a['kelas']; ?></td>
                <td><?php echo $a['tgl_lapor']; ?></td>
                <td><?php echo $a['matprak']; ?></td>
                <td><?php echo $a['pertemuan']; ?></td>
                <?php if(($a['tipe_laporan']==1) AND ($a['status_laporan']==1)): ?>
                  <td><b>Menunggu Praktikan Mengupload Tugas ...</b></td>
                  <td>
                    <a href="" class="btn-floating tooltipped waves-effect waves-light green darken-4 z-depth-4" data-position="buttom" data-delay="10" data-tooltip="Lihat Laporan"><i class="material-icons">launch</i></a>                  
                  </td>
                <?php elseif(($a['tipe_laporan']==1) AND ($a['status_laporan']==2)): ?>
                  <td><b class="blue-text">Tugas Sudah Terupload !!</b></td>
                  <td>
                    <a href="#<?php echo $a['id_laporan']; ?>" class="btn-floating tooltipped waves-effect waves-light green darken-4 z-depth-4" data-position="buttom" data-delay="10" data-tooltip="Lihat Laporan"><i class="material-icons">launch</i></a>                  
                  </td>
                <?php endif; ?>
              </tr>
            <?php $no++; endforeach; ?><!-- akhir perulangan -->
          </tbody>
        </table>
      </div>
    </div>

    <!-- modal launch -->
    <?php foreach($record as $b): ?><!-- perulangan disini -->
        <div id="<?php echo $b['id_laporan']; ?>" class="modal modal-fixed-footer">
          <div class="modal-content">

            <!-- judul -->
            <div class="center">
              <img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
              <h4>Data Laporan Masuk</h4>
            </div>

            <!-- bagian penampil data -->
            <div class="row">
              <div class="input-field col s6">
                <input readonly type="text" value="<?php echo $b['id_laporan']; ?>">
                <label >ID Laporan</label>
              </div>
              <div class="input-field col s6">
                <input readonly type="text" value="<?php echo $b['tgl_lapor']; ?>">
                <label >Tanggal Urus</label>
              </div>
              <div class="input-field col s6">
                <input readonly type="text" value="<?php echo $b['nama']; ?>">
                <label >Nama</label>
              </div>
              <div class="input-field col s6">
                <input readonly type="text" value="<?php echo $b['npm']; ?>">
                <label >NPM</label>
              </div>
              <div class="input-field col s6">
                <input readonly type="text" value="<?php echo $b['kelas']; ?>">
                <label >Kelas</label>
              </div>
              <div class="input-field col s6">
                <input readonly type="text" value="<?php echo $b['no_hp']; ?>">
                <label >No. HP</label>
              </div>
              <div class="input-field col s6">
                <input readonly type="text" value="<?php echo $b['matprak']; ?>">
                <label >Mata Praktikum</label>
              </div>
              <div class="input-field col s6">
                <input readonly type="text" value="<?php echo $b['pertemuan']; ?>">
                <label >Pertemuan</label>
              </div>
              <div class="input-field col s12">
                <input readonly type="text" value="<?php echo $b['alasan']; ?>">
                <label >Alasan</label>
              </div>
              <div class="input-field col s4">
                KTP : <br>
                <a href="<?php echo base_url(); ?>admin/ktp/<?php echo $b['ktp']; ?>" target="_blank"><img data-caption="<?php echo $b['ktp']; ?>" width="100" height="auto" src="<?php echo base_url(); ?>admin/ktp/<?php echo $b['ktp']; ?>"></a>
              </div>
              <div class="input-field col s4">
                Surat Keterangan : <br>
                <a href="<?php echo base_url(); ?>admin/surket/<?php echo $b['surket']; ?>" target="_blank"><img data-caption="<?php echo $b['surket']; ?>" width="100" height="auto" src="<?php echo base_url(); ?>admin/surket/<?php echo $b['surket']; ?>"></a>
              </div>
              <div class="input-field col s4">
                Tugas : <br>
                <a href="<?php echo base_url(); ?>admin/tugas/<?php echo $b['tugas']; ?>" target="_blank"><?php echo $b['tugas']; ?></a>
              </div>
            </div>
          </div>
          <!-- action modal launch -->
          <div class="modal-footer">
            <a href="<?php echo base_url(); ?>admin/konfirmasi_laporanmasuk/<?php echo $b['id_laporan']; ?>" class="modal-action modal-close waves-effect waves-green btn-flat ">Konfirmasi Laporan</a>
          </div>
        </div>
    <?php endforeach; ?><!-- akhir perulangan --> 

    <div class="fixed-action-btn">
      <a class="btn-floating tooltipped btn-large blue z-depth-4 " data-position="left" data-delay="10" data-tooltip="Case Laporan Khusus" href="<?php echo base_url(); ?>admin/case_laporankhusus">
        <i class="large material-icons">star</i>
      </a>
    </div>
</main>