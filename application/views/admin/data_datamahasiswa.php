<main>
	<!-- tabel user -->
    <div class="section z-depth-5" style="margin: 50px">
      <div style="padding: 10px">
        <div class="center">
          <img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
          <h4 class="header center text-lighten-2">Data Mahasiswa</h4>
        </div>
        <table id="example" class="highlight centered">
          <thead>
            <tr>
              <th>No</th>
              <th>NPM</th>
              <th>Nama</th>
              <th>Kelas</th>
              <th>Angkatan</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($record as $a): ?><!-- perulangan disini -->
              <tr class="z-depth-1">
                <td><?php echo $no; ?></td>
                <td><?php echo $a['npm']; ?></td>
                <td><?php echo $a['nama']; ?></td>
                <td><?php echo $a['kelas']; ?></td>
                <td><?php echo $a['angkatan']; ?></td>
                <td>
                  <a href="#<?php echo $a['npm']; ?>" class="btn-floating tooltipped waves-effect waves-light green  z-depth-4" data-position="buttom" data-delay="10" data-tooltip="Lihat User"><i class="material-icons">launch</i></a>
                </td>
              </tr>
            <?php $no++; endforeach; ?><!-- akhir perulangan -->
          </tbody>
        </table>
      </div>
    </div>

    <!-- modal launch -->
    <?php foreach($record as $b): ?><!-- perulangan disini -->
    <div id="<?php echo $b['npm']; ?>" class="modal modal-fixed-footer">
      <div class="modal-content">
        <!-- judul -->
        <div class="center">
          <img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
          <h4>Data Mahasiswa</h4>
        </div>
        <!-- bagian penampil data -->
        <div class="row">
          <div class="input-field col s12">
            <input readonly type="text" value="<?php echo $b['npm']; ?>">
            <label >NPM</label>
          </div>
	    	  <div class="input-field col s4">
            <input readonly type="text" value="<?php echo $b['nama']; ?>">
            <label >Nama</label>
	    	  </div>
          <div class="input-field col s4">
            <input readonly type="text" value="<?php echo $b['kelas']; ?>">
            <label >Kelas</label>
          </div>
	    	  <div class="input-field col s4">
            <input readonly type="text" value="<?php echo $b['angkatan']; ?>">
            <label >Angkatan</label>
	    	  </div>
        </div>
      </div>
      <!-- action modal launch -->
      <div class="modal-footer">
        <a href="<?php echo base_url(); ?>admin/HapusDataMahasiswa/<?php echo $b['npm']; ?>" class="modal-action modal-close waves-effect waves-green btn-flat " onclick="return  confirm('Hapus Data Mahasiswa ?')">Delete</a>
        <a href="<?php echo base_url(); ?>admin/data_ubahdatamahasiswa/<?php echo $b['npm']; ?>" class="modal-action modal-close waves-effect waves-green btn-flat ">Edit</a>
      </div>
    </div>
    <?php endforeach; ?><!-- akhir perulangan -->	

    <div class="fixed-action-btn">
	    <a class="btn-floating tooltipped btn-large green z-depth-4 " data-position="left" data-delay="10" data-tooltip="Tambah Data Mahasiswa" href="<?php echo base_url(); ?>admin/data_tambahdatamahasiswa">
	      <i class="large material-icons">person_add</i>
	    </a>
    </div>
</main>