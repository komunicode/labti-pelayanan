<!DOCTYPE html>
<html>
<head>
	<title>Labti-Pelayanan</title>
	<link href='<?php echo base_url(); ?>assets/img/labti.png' rel='shortcut icon'>

	<!--Import Google Icon Font-->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/material-icons/material-icons.css" />

	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/materialize.min.css" />
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/dataTables.materialize.css" />
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/u_custom.css" />

	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<!-- header -->	
	<div class="navbar-fixed">
		<nav class="lighten-1 z-depth-3" style="background-color: #0f3057;">
		    <div class="nav-wrapper">
		    	<a href="<?php echo base_url(); ?>admin" class="brand-logo" style="padding: 10px"><img style="width: 60px" src="<?php echo base_url(); ?>assets/img/labti.png"></a>
	      		<a href="#!" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
	      		<ul class="right hide-on-med-and-down">
	      			<?php foreach($user as $a): ?>
	        		<li><a href="<?php echo base_url(); ?>admin/logout"><b><?php echo $a['nama']; ?></b><i class="material-icons left tooltipped" data-position="left" data-delay="10" data-tooltip="Logout">power_settings_new</i></a></li>
	        		<?php endforeach; ?>
	      		</ul>	      	
	    	</div>
		</nav>
	</div>
	<ul class="side-nav fixed" id="mobile-demo" style="width: 250px">
		<div class="center white-text lighten-1 z-depth-3" style="background-color: #0f3057;">
			<img style="width: 100px;margin: 10px" src="<?php echo base_url(); ?>assets/img/labti.png">
			<?php foreach($user as $a): ?>
			<h6><b><?php echo $a['nama']; ?></b></h6>
			<h6><?php echo $a['id_user']; ?></h6>
			<?php endforeach; ?>
			<br>
	    </div>
		<li><a href="<?php echo base_url(); ?>admin" class="collapsible-header waves-effect waves-teal"><i class="material-icons left">home</i>Dashboard</a></li>
		<li>
			<ul class="collapsible collapesible-accordion">
				<li>
					<a class="collapsible-header waves-effect waves-teal"><i class="material-icons left">person</i>User</a>
					<div class="collapsible-body">
		            	<ul>
		            		<li><a href="<?php echo base_url(); ?>admin/user/1" class="waves-effect waves-teal"><i class="material-icons left">person</i><?php $a = count($praktikan) ?><?php echo "$a"; ?> - Praktikan</a></li>
		                  	<li><a href="<?php echo base_url(); ?>admin/user/2" class="waves-effect waves-teal"><i class="material-icons left">person_pin</i><?php $a = count($at) ?><?php echo "$a"; ?> - Asisten Tetap</a></li>
		                  	<li><a href="<?php echo base_url(); ?>admin/user/3" class="waves-effect waves-teal"><i class="material-icons left">supervisor_account</i><?php $a = count($pj) ?><?php echo "$a"; ?> - PJ</a></li>
		                </ul>
		            </div>
	            </li>
			</ul>
		</li>
		<li>
			<ul class="collapsible collapesible-accordion">
				<li>
					<a class="collapsible-header waves-effect waves-teal"><i class="material-icons left">format_list_bulleted</i>Pelayanan Praktikan</a>
					<div class="collapsible-body">
		            	<ul>
		            		<li><a href="<?php echo base_url(); ?>admin/laporan_masuk" class="waves-effect waves-teal"><i class="material-icons left">move_to_inbox</i><?php $a = count($lap_masuk) ?><?php echo "$a"; ?> - L. Masuk</a></li>
		                  	<li><a href="<?php echo base_url(); ?>admin/laporan_terkonfirmasi" class="waves-effect waves-teal"><i class="material-icons left">assignment_turned_in</i><?php $a = count($lap_terkonfirmasi) ?><?php echo "$a"; ?> - L. Terkonfirmasi</a></li>
		                  	<li><a href="<?php echo base_url(); ?>admin/laporan_tidakterkonfirmasi" class="waves-effect waves-teal"><i class="material-icons left">assignment_late</i><?php $a = count($lap_ditolak) ?><?php echo "$a"; ?> - L. Ditolak</a></li>
							<li><a href="<?php echo base_url(); ?>admin/laporan_kartuhilang" class="waves-effect waves-teal"><i class="material-icons left">phonelink_erase</i><?php $a = count($lap_kartuhilang) ?><?php echo "$a"; ?> - L. Kartu Hilang</a></li>
							<li><a href="<?php echo base_url(); ?>admin/transfer_praktikantransfer" class="waves-effect waves-teal"><i class="material-icons left">swap_horiz</i><?php $a = count($lap_praktrans) ?><?php echo "$a"; ?> - L. P.Transfer</a></li>
		                </ul>
		            </div>
	            </li>
			</ul>
		</li>
		<li><a href="<?php echo base_url(); ?>admin/matprak_pilihtingkat" class="collapsible-header waves-effect waves-teal"><i class="material-icons left">import_contacts</i>Mata Praktikum</a></li>
		<li><a href="<?php echo base_url(); ?>admin/data_datamahasiswa" class="collapsible-header waves-effect waves-teal"><i class="material-icons left">people</i>Data Mahasiswa</a></li>
		<li><a href="<?php echo base_url(); ?>admin/tugas_praktikan" class="collapsible-header waves-effect waves-teal"><i class="material-icons left">library_books</i>Tugas</a></li>
	    <li><a href="<?php echo base_url(); ?>admin/about" class="collapsible-header waves-effect waves-teal"><i class="material-icons left">info</i>About</a></li>
	    <li><a href="<?php echo base_url(); ?>admin/logout" class="collapsible-header waves-effect waves-teal"><i class="material-icons left">power_settings_new</i>Logout</a></li>
	</ul>
