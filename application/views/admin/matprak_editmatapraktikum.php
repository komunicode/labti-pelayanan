<main>
	<div class="row" style="padding: 59px">
		<?php foreach($record as $a): ?><!-- perulangan disini -->
		<div class="center">
			<img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
			<h5>Edit Mata Praktikum <?php echo $a['matprak']; ?></h5>
		</div>
		<form method="post" action="<?php echo base_url(); ?>admin/matprak_updatematapraktikum">
			<div class="input-field col s12">
		    	<input type="text" value="<?php echo $a['id_matprak']; ?>" readonly name="id_matprak">
		    	<label >ID Mata Praktikum</label>
		    </div>
		    <div class="input-field col s6">
		    	<input type="text" value="<?php echo $a['matprak']; ?>" name="matprak" readonly>
		    	<label >Mata Praktikum</label>
		    </div>
		    <div class="input-field col s6">
		    	<input type="text" value="<?php echo $a['tingkat']; ?>" name="tingkat">
		    	<label >Tingkat</label>
		    </div>
		    <div class="input-field col s12">
		    	<input type="text" value="<?php echo $a['jum_pertemuan']; ?>" name="jum_pertemuan">
		    	<label >Jumlah Pertemuan</label>
		    </div>
		    <div class="input-field col s12">
			    <textarea id="textarea1" name="deskripsi" class="materialize-textarea"><?php echo $a['deskripsi']; ?></textarea>
				<label for="textarea1">Deskripsi Praktikum</label>
		    </div>
		    <div class="center col s12">
	        	<input class="waves-effect waves-light btn" type="submit" name="submit" value="UPDATE PRAKTIKUM">
			</div>
			<?php endforeach; ?><!-- akhir perulangan -->
		</form>
    </div>
</main>