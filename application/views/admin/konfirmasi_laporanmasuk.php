<main>
	<div class="row" style="padding: 59px">
		<div class="center">
			<img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
			<h5>Konfirmasi Laporan</h5>
		</div>
		<form method="post" action="<?php echo base_url(); ?>admin/KonfirmasiLaporanMasuk" enctype="multipart/form-data">
			<?php foreach($record as $a): ?><!-- perulangan disini -->
	    	<div class="input-field col s6">
	    		<input type="text" name="id_laporan" required value="<?php echo $a['id_laporan']; ?>" readonly>
	    		<input type="text" name="id_matprak" required value="<?php echo $a['id_matprak']; ?>" readonly hidden>
				<input type="text" name="no_hp" required value="<?php echo $a['no_hp']; ?>" readonly hidden>
				<input type="text" name="ktp" required value="<?php echo $a['ktp']; ?>" readonly hidden>
				<input type="text" name="surket" required value="<?php echo $a['surket']; ?>" readonly hidden>
				<input type="text" name="id_tugas" required value="<?php echo $a['id_tugas']; ?>" readonly hidden>
				<input type="text" name="tugas" required value="<?php echo $a['tugas']; ?>" readonly hidden>
				<input type="text" name="tipe_laporan" required value="<?php echo $a['tipe_laporan']; ?>" readonly hidden>
	    		<label >ID Laporan</label>
	    	</div>
	    	<div class="input-field col s6">
	    		<input type="text" name="tgl_lapor" required value="<?php echo $a['tgl_lapor']; ?>" readonly>
	    		<label >Tanggal Urus</label>
	    	</div>
	    	<div class="input-field col s12">
	    		<input type="text" required value="<?php echo $a['nama']; ?>" readonly>
	    		<label >Nama</label>
	    	</div>
	    	<div class="input-field col s6">
	    		<input type="text" name="id_user" required value="<?php echo $a['id_user']; ?>" readonly hidden>
	    		<input type="text" name="npm" required value="<?php echo $a['npm']; ?>" readonly>
	    		<label >NPM</label>
	    	</div>
	    	<div class="input-field col s6">
	    		<input type="text" required value="<?php echo $a['kelas']; ?>" readonly>
	    		<label >Kelas</label>
	    	</div>
	    	<div class="input-field col s6">
	    		<input type="text" required value="<?php echo $a['matprak']; ?>" readonly>
	    		<label >Mata Praktikum</label>
	    	</div>
	    	<div class="input-field col s6">
	    		<input type="text" name="pertemuan" required value="<?php echo $a['pertemuan']; ?>" readonly>
	    		<label >Pertemuan</label>
	    	</div>
	    	<div class="input-field col s12">
	    		<input type="text" name="alasan" required value="<?php echo $a['alasan']; ?>" readonly>
	    		<label >Alasan</label>
	    	</div>
	    	<div class="input-field col s4">
            	KTP : <br>
            	<a href="<?php echo base_url(); ?>admin/ktp/<?php echo $a['ktp']; ?>" target="_blank"><img data-caption="<?php echo $a['ktp']; ?>" width="100" height="auto" src="<?php echo base_url(); ?>admin/ktp/<?php echo $a['ktp']; ?>"></a>
          	</div>
          	<div class="input-field col s4">
          		Surat Keterangan : <br>
            	<a href="<?php echo base_url(); ?>admin/surket/<?php echo $a['surket']; ?>" target="_blank"><img data-caption="<?php echo $a['surket']; ?>" width="100" height="auto" src="<?php echo base_url(); ?>admin/surket/<?php echo $a['surket']; ?>"></a>
          	</div>
          	<div class="input-field col s4">
            	Tugas : <br>
            	<a href="<?php echo base_url(); ?>admin/tugas/<?php echo $a['tugas']; ?>" target="_blank"><?php echo $a['tugas']; ?></a>
          	</div>
	    	<?php endforeach; ?><!-- akhir perulangan -->	
	        <div class="input-field col s12">
	    		<input type="text" required name="catatan">
	    		<label >Catatan</label>
	    	</div>
	    	<div class="center col s12">
              <input class="waves-effect waves-light btn" type="submit" name="submit" value="TERIMA LAPORAN" onclick="return  confirm('Terima Laporan ?')">
              <input class="red waves-effect waves-light btn" type="submit" name="submit" value="TOLAK LAPORAN" onclick="return  confirm('Tolak Laporan ?')">
            </div>
    	</form>
    </div>
</main>