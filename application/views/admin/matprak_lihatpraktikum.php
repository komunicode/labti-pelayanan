<main>
	<div class="row" style="padding: 59px">
		<?php if($record!=null): ?>
			<?php foreach($record as $a): ?><!-- perulangan disini -->
				<div class="center">
					<img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
					<h5>Mata Praktikum <?php echo $a['matprak']; ?></h5>
				</div>
				<div class="input-field col s12">
			    	<input type="text" value="<?php echo $a['id_matprak']; ?>" readonly>
			    	<label >ID Mata Praktikum</label>
			    </div>
			    <div class="input-field col s6">
			    	<input type="text" value="<?php echo $a['matprak']; ?>" readonly>
			    	<label >Mata Praktikum</label>
			    </div>
			    <div class="input-field col s6">
			    	<input type="text" value="<?php echo $a['tingkat']; ?>" readonly>
			    	<label >Tingkat</label>
			    </div>
			    <div class="input-field col s12">
			    	<input type="text" value="<?php echo $a['jum_pertemuan']; ?>" readonly>
			    	<label >Jumlah Pertemuan</label>
			    </div>
			    <div class="input-field col s12">
				    <textarea id="textarea1" class="materialize-textarea" readonly><?php echo $a['deskripsi']; ?></textarea>
					<label for="textarea1">Deskripsi Praktikum</label>
			    </div>
			    <div class="center col s12">
		        	<a href="<?php echo base_url(); ?>admin/matprak_editmatapraktikum/<?php echo $a['id_matprak']; ?>"><input class="waves-effect waves-light btn" type="submit" name="submit" value="EDIT PRAKTIKUM"></a>
		        	<a href="<?php echo base_url(); ?>admin/matprak_deletematapraktikum/<?php echo $a['id_matprak']; ?>"><input class="waves-effect red waves-light btn" type="submit" name="submit" value="HAPUS PRAKTIKUM" onclick="return  confirm('Hapus Mata Praktikum <?php echo $a['matprak']; ?>  ?')"></a>
				</div>
			<?php endforeach; ?><!-- akhir perulangan -->
		<?php else: ?>
			<div class="center">
	          <div class="z-depth-2" style="margin: 50px; padding: 50px 0px 50px 0px">
	            <h5><b>Mata Praktikum Belum Tersedia ... </b></h5>  
	            <a href="<?php echo base_url(); ?>admin/matprak_pilihtingkat">kembali ... </a>
	          </div>          
	        </div>
		<?php endif; ?>
    </div>
</main>