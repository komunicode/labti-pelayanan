<main>
	<div class="row" style="padding: 59px">
		<div class="center">
			<img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
			<h5>Tambah Mata Praktikum</h5>
		</div>
		<form method="post" action="<?php echo base_url(); ?>admin/matprak_tambahpraktikum">
			<div class="input-field col s12">
		    	<input type="text" name="id_matprak" required>
		    	<label >ID Mata Praktikum</label>
		    </div>
		    <div class="input-field col s6">
		    	<input type="text" name="matprak" required>
		    	<label >Mata Praktikum</label>
		    </div>
		    <div class="input-field col s6">
		    	<select class='form-control' name="tingkat" required>
	              <option value="" disabled selected>--PILIH TINGKAT--</option>
	              <option value="1">Tingkat 1 (Semester 1 - 2)</option>
	              <option value="2">Tingkat 2 (Semester 3 - 4)</option>
	              <option value="3">Tingkat 3 (Semester 5 - 6)</option>
	              <option value="4">Tingkat 4 (Semester 7 - 8)</option>
	            </select>
	            <label >Untuk Tingkat</label>
		    </div>
		    <div class="input-field col s12">
		    	<select class='form-control' name="jum_pertemuan" required>
	              <option value="" disabled selected>--Jumlah Pertemuan--</option>
	              <option value="1">1 Pertemuan</option>
	              <option value="2">2 Pertemuan</option>
	              <option value="3">3 Pertemuan</option>
	              <option value="4">4 Pertemuan</option>
	              <option value="5">5 Pertemuan</option>
	              <option value="6">6 Pertemuan</option>
	              <option value="7">7 Pertemuan</option>
	              <option value="8">8 Pertemuan</option>
	            </select>
	            <label >Jumlah Pertemuan</label>
		    </div>
		    <div class="input-field col s12">
			    <textarea id="textarea1" name="deskripsi" class="materialize-textarea" required></textarea>
				<label for="textarea1">Deskripsi Praktikum</label>
		    </div>
		    <div class="center col s12">
	        	<input class="waves-effect waves-light btn" type="submit" name="submit" value="TAMBAH PRAKTIKUM">
			</div>
		</form>
    </div>
</main>