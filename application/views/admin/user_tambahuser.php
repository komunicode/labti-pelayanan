<main>
	<div class="row" style="padding: 59px">
		<div class="center">
			<img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
			<h5>Tambah User</h5>
		</div>
		<form method="post" action="<?php echo base_url(); ?>admin/UserTambahUser">
	    	<div class="input-field col s12">
	    		<input type="text" name="npm" required>
	    		<label >NPM</label>
	    	</div>
	    	<div class="input-field col s6">
	    		<input type="text" name="username" required>
	    		<label >Username</label>
	    	</div>
	    	<div class="input-field col s6">
	    		<input type="text" name="password" required>
	    		<label >Password</label>
	    	</div>
	    	<div class="input-field col s12">
	    		<select name="level">
			      <option value="" disabled selected>Pilih Tipe User</option>
			      <option value="1">Praktikan</option>
			      <option value="2">Asisten Tetap</option>
			      <option value="3">PJ</option>
			    </select>
			    <label >Tipe User</label>
	    	</div>
	    	<div class="center col s12">
              <input class="waves-effect waves-light btn" type="submit" name="submit" value="TAMBAH USER">
            </div>
    	</form>
    </div>
</main>