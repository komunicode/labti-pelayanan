<main>
	<div class="row" style="padding: 59px">
		<div class="center">
			<img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
			<h5>PILIH TIPE LAPORAN</h5>
		</div>
		<form method="post" action="<?php echo base_url(); ?>admin/pilih_laporan" enctype="multipart/form-data">
			<div class="input-field col s12">
	    		<select name="tipelaporan">
			      <option value="" disabled selected>Pilih Tipe Laporan</option>
			      <option value="harian">Harian</option>
			      <option value="bulanan">Bulanan</option>
			      <option value="kelas">Kelas</option>
			    </select>
			    <label >Tipe Laporan</label>
	    	</div>
	    	<div class="center col s12">
              <input class="waves-effect waves-light btn" type="submit" name="submit" value="PILIH TIPE LAPORAN">
            </div>
		</form>
    </div>
</main>