<main class="container">
  <div class="row" style="padding: 20px">
		<div class="center">
      <img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
      <h5>Cek Data Mahasiswa</h5>
    </div>
    <form method="post" action="<?php echo base_url(); ?>admin/TransferCekDataMahasiswa">
      <div class="input-field col s12 m12">
        <input type="text" name="npm" required>
        <label>Masukkan NPM Mahasiswa</label>
      </div>
      <div class="center col s12">
        <input class="waves-effect waves-light btn" type="submit" name="submit" value="Cek Data Mahasiswa">
      </div>
    </form>    
  </div>
  <div class="center" style="margin: 20px">
    <p>Data mahasiswa tidak ditemukan ? <a href="<?php echo base_url(); ?>admin/transfer_tambahpraktransferbaru">Tambah Data Mahasiswa Baru</a> </p>
  </div>
</main>