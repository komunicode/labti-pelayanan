<!DOCTYPE html>
<html>
<head>
	<title>Labti-Pelayanan</title>
	<link href='<?php echo base_url(); ?>assets/img/labti.png' rel='shortcut icon'>

	<!--Import Google Icon Font-->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/material-icons/material-icons.css" />

	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/materialize.min.css" />
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/dataTables.materialize.css" />
	

	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<!-- header -->	
	<div class="navbar-fixed">
		<nav class="lighten-1 z-depth-3" style="background-color: #0f3057;">
		    <div class="nav-wrapper">
		    	<a href="<?php echo base_url(); ?>admin" class="brand-logo" style="padding: 10px"><img style="width: 60px" src="<?php echo base_url(); ?>assets/img/labti.png"></a>
	      		<a href="#!" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
	      		<ul class="right hide-on-med-and-down">
	      			<?php foreach($user as $a): ?>
	        		<li><a href="<?php echo base_url(); ?>admin/logout"><b><?php echo $a['nama']; ?></b><i class="material-icons left tooltipped" data-position="left" data-delay="10" data-tooltip="Logout">power_settings_new</i></a></li>
	        		<?php endforeach; ?>
	      		</ul>	      	
	    	</div>
		</nav>
	</div>
	<ul class="side-nav" id="mobile-demo" style="width: 250px">
		<div class="center white-text lighten-1 z-depth-3" style="background-color: #0f3057;">
			<img style="width: 100px;margin: 10px" src="<?php echo base_url(); ?>assets/img/labti.png">
			<?php foreach($user as $a): ?>
			<h6><b><?php echo $a['nama']; ?></b></h6>
			<h6><?php echo $a['id_user']; ?></h6>
			<?php endforeach; ?>
			<br>
	    </div>
	    <li><a href="<?php echo base_url(); ?>admin/about" class="collapsible-header waves-effect waves-teal"><i class="material-icons left">info</i>About</a></li>
	    <li><a href="<?php echo base_url(); ?>admin/logout" class="collapsible-header waves-effect waves-teal"><i class="material-icons left">power_settings_new</i>Logout</a></li>
	</ul>
