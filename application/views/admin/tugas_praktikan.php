<main>
  <!-- tabel user -->
    <div class="section z-depth-5" style="margin: 50px">
      <div style="padding: 10px">
      <div class="center">
        <img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
        <h4 class="header center text-lighten-2">Data Tugas Praktikan</h4>
      </div>
      <table id="example" class="highlight centered">
        <thead>
          <tr>
            <th>No</th>
            <th>Mata Praktikum</th>
            <th>Kelas</th>
            <th>Pertemuan</th>
            <th>Soal Tugas</th>
          </tr>
        </thead>
        <tbody>
          <?php $no=1; foreach($record as $a): ?><!-- perulangan disini -->
            <tr class="z-depth-1">
              <td><?php echo $no; ?></td>
              <td><?php echo $a['matprak']; ?></td>
              <td><?php echo $a['kelas']; ?></td>
              <td><?php echo $a['pertemuan']; ?></td>
              <td><?php echo $a['soal']; ?></td>
            </tr>
          <?php $no++; endforeach; ?><!-- akhir perulangan -->
        </tbody>
      </table>
      </div>
    </div>
</main>