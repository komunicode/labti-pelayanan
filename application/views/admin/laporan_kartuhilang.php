<main>
  <!-- tabel user -->
    <div class="section z-depth-5" style="margin: 50px">
      <div style="padding: 10px">
      <div class="center">
        <img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
        <h4 class="header center text-lighten-2">Data Laporan Kartu Hilang</h4>
      </div>
      <table id="example" class="highlight centered">
        <thead>
          <tr>
            <th>No</th>
            <th>NPM</th>
            <th>Nama</th>
            <th>Kelas</th>
            <th>Tanggal Lapor</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $no=1; foreach($record as $a): ?><!-- perulangan disini -->
            <?php if($a['status_laporan']==1): ?>
              <tr class="purple lighten-1 z-depth-4">
                <td><?php echo $no; ?></td>
                <td><?php echo $a['npm']; ?></td>
                <td><?php echo $a['nama']; ?></td>
                <td><?php echo $a['kelas']; ?></td>
                <td><?php echo $a['tgl_lapor']; ?></td>
                <td><b><?php echo $a['catatan']; ?></b></td>
                <td class="center">
                  <a href="#<?php echo $a['id_laporan']; ?>" class="btn-floating tooltipped waves-effect waves-light green darken-4 z-depth-4" data-position="buttom" data-delay="10" data-tooltip="Lihat Laporan"><i class="material-icons">launch</i></a>                  
                </td>
              </tr>
            <?php else: ?>
              <tr class="z-depth-1">
                <td><?php echo $no; ?></td>
                <td><?php echo $a['npm']; ?></td>
                <td><?php echo $a['nama']; ?></td>
                <td><?php echo $a['kelas']; ?></td>
                <td><?php echo $a['tgl_lapor']; ?></td>
                <td><b class="green-text"><?php echo $a['catatan']; ?></b></td>
                <td class="center">
                  <a href="#<?php echo $a['id_laporan']; ?>" class="btn-floating tooltipped waves-effect waves-light green darken-4 z-depth-4" data-position="buttom" data-delay="10" data-tooltip="Lihat Laporan"><i class="material-icons">launch</i></a>                  
                </td>
              </tr>              
            <?php endif ?>
          <?php $no++; endforeach; ?><!-- akhir perulangan -->
        </tbody>
      </table>
      </div>
    </div>

    <!-- modal launch -->
    <?php foreach($record as $b): ?><!-- perulangan disini -->
      <?php if($b['status_laporan']==1): ?>
        <div id="<?php echo $b['id_laporan']; ?>" class="modal modal-fixed-footer">
          <div class="modal-content">

            <!-- judul -->
            <div class="center">
              <img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
              <h4>Data Laporan Kartu Hilang</h4>
            </div>

            <!-- bagian penampil data -->
            <div class="row">
              <div class="input-field col s6">
                <input readonly type="text" value="<?php echo $b['id_laporan']; ?>">
                <label >ID Laporan</label>
              </div>
              <div class="input-field col s6">
                <input readonly type="text" value="<?php echo $b['tgl_lapor']; ?>">
                <label >Tanggal Lapor</label>
              </div>
              <div class="input-field col s6">
                <input readonly type="text" value="<?php echo $b['nama']; ?>">
                <label >Nama</label>
              </div>
              <div class="input-field col s6">
                <input readonly type="text" value="<?php echo $b['npm']; ?>">
                <label >NPM</label>
              </div>
              <div class="input-field col s6">
                <input readonly type="text" value="<?php echo $b['kelas']; ?>">
                <label >Kelas</label>
              </div>
              <div class="input-field col s6">
                <input readonly type="text" value="<?php echo $b['no_hp']; ?>">
                <label >No. HP</label>
              </div>
            </div>

          </div>

          <!-- action modal launch -->
          <div class="modal-footer">
            <a href="<?php echo base_url(); ?>admin/konfirmasi_laporankartuhilang/<?php echo $b['id_laporan']; ?>" class="modal-action modal-close waves-effect waves-green btn-flat ">Konfirmasi Laporan</a>
          </div>
        </div>
      <?php else : ?>
        <div id="<?php echo $b['id_laporan']; ?>" class="modal">
          <div class="modal-content">

            <!-- judul -->
            <div class="center">
              <img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
              <h4>Data Laporan Kartu Hilang</h4>
            </div>

            <!-- bagian penampil data -->
            <div class="row">
              <div class="input-field col s6">
                <input readonly type="text" value="<?php echo $b['id_laporan']; ?>">
                <label >ID Laporan</label>
              </div>
              <div class="input-field col s6">
                <input readonly type="text" value="<?php echo $b['tgl_lapor']; ?>">
                <label >Tanggal Lapor</label>
              </div>
              <div class="input-field col s6">
                <input readonly type="text" value="<?php echo $b['admin_verifikasi']; ?>">
                <label >Admin Verifikasi</label>
              </div>
              <div class="input-field col s6">
                <input readonly type="text" value="<?php echo $b['tgl_terkonfirmasi']; ?>">
                <label >Tanggal Terkonfirmasi</label>
              </div>
              <div class="input-field col s3">
                <input readonly type="text" value="<?php echo $b['nama']; ?>">
                <label >Nama</label>
              </div>
              <div class="input-field col s3">
                <input readonly type="text" value="<?php echo $b['npm']; ?>">
                <label >NPM</label>
              </div>
              <div class="input-field col s3">
                <input readonly type="text" value="<?php echo $b['kelas']; ?>">
                <label >Kelas</label>
              </div>
              <div class="input-field col s3">
                <input readonly type="text" value="<?php echo $b['no_hp']; ?>">
                <label >No. HP</label>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
    <?php endforeach; ?><!-- akhir perulangan --> 
</main>