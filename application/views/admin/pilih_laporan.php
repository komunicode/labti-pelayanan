<main>
	<div class="row" style="padding: 59px">
		<div class="center">
			<img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
			<?php if ($pilih == 'harian'): ?>
            	<h4 class="header center text-lighten-2">PILIH HARI</h4>
            <?php elseif ($pilih == 'bulanan'): ?>
            	<h4 class="header center text-lighten-2">PILIH BULAN</h4>
          	<?php else: ?>
	            <h4 class="header center text-lighten-2">PILIH KELAS</h4>
          	<?php endif ?>
		</div>
		<?php if ($pilih == 'harian'): ?>
			<form method="post" action="<?php echo base_url(); ?>admin/laporanharian" enctype="multipart/form-data" target="_blank">
				<div class="input-field col s12">
		    		<input name="hari" type="text" class="datepicker">
				    <label >Pilih Hari</label>
		    	</div>
		    	<div class="center col s12">
	              <input class="waves-effect waves-light btn" type="submit" name="submit" value="CETAK LAPORAN">
	            </div>
			</form>


		<?php elseif ($pilih == 'bulanan'): ?>
			<form method="post" action="<?php echo base_url(); ?>admin/laporanbulanan" enctype="multipart/form-data" target="_blank">
				<div class="input-field col s6">
		    		<select name="bulan">
				      	<option value="" disabled selected>Pilih Bulan</option>
				      	<option value="01">Januari</option>
				      	<option value="02">Februari</option>
				      	<option value="03">Maret</option>
				      	<option value="04">April</option>
				      	<option value="05">Mei</option>
				      	<option value="06">Juni</option>
				      	<option value="07">Juli</option>
				      	<option value="08">Agustus</option>
				      	<option value="09">September</option>
				      	<option value="10">Oktober</option>
				      	<option value="11">November</option>
				      	<option value="12">Desember</option>
				    </select>
				    <label >Pilih Bulan</label>
		    	</div>
		    	<div class="input-field col s6">
		    		<select name="tahun">
				      	<option value="" disabled selected>Pilih Tahun</option>
				      	<?php for ($i=0; $i < 100 ; $i++) :?>
				      		<?php if($i<=9): ?>
				      			<option value="200<?php echo $i; ?>">200<?php echo $i; ?></option>
				      		<?php else: ?>
				      			<option value="20<?php echo $i; ?>">20<?php echo $i; ?></option>
				      		<?php endif ?>
				      	<?php endfor ?>
				    </select>
				    <label >Pilih Tahun</label>
		    	</div>
		    	<div class="center col s12">
	              <input class="waves-effect waves-light btn" type="submit" name="submit" value="CETAK LAPORAN">
	            </div>
			</form>


		<?php else: ?>
			<form method="post" action="<?php echo base_url(); ?>admin/laporankelas" enctype="multipart/form-data" target="_blank">
		    	<div class="input-field col s12">
		    		<select name="kelas">
				      	<option value="" disabled selected>Pilih Kelas</option>
				      	<?php foreach($kelas as $a): ?><!-- perulangan disini -->
				      	<option value="<?php echo $a['kelas']; ?>"><?php echo $a['kelas']; ?></option>
				      	<?php endforeach; ?><!-- akhir perulangan -->
				    </select>
				    <label >Pilih Kelas</label>
		    	</div>
		    	<div class="center col s12">
	              <input class="waves-effect waves-light btn" type="submit" name="submit" value="CETAK LAPORAN">
	            </div>
			</form>
		<?php endif ?>
		
    </div>
</main>