<main>
	<div class="row" style="padding: 59px">
		<div class="center">
			<img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
			<h5>Tambah Data Mahasiswa</h5>
		</div>
		<form method="post" action="<?php echo base_url(); ?>admin/TambahDataMahasiswa" enctype="multipart/form-data">
	    	<div class="input-field col s12">
	    		<input type="text" name="npm" required>
	    		<label >NPM</label>
	    	</div>
	    	<div class="input-field col s12">
	    		<input type="text" name="nama" required>
	    		<label >Nama</label>
	    	</div>
	    	<div class="input-field col s6">
	    		<input type="text" name="angkatan" required>
	    		<label >Angkatan</label>
	    	</div>
	    	<div class="input-field col s6">
	    		<input type="text" name="kelas" required>
	    		<label >Kelas</label>
	    	</div>
	    	<div class="center col s12">
              <input class="waves-effect waves-light btn" type="submit" name="submit" value="TAMBAH DATA MAHASISWA" onclick="return  confirm('Data Mahasiswa Sudah Benar ?')">
            </div>
    	</form>
    </div>
</main>