<!DOCTYPE html>
<html>
<head>
	<title>Labti-Pelayanan</title>
	<link href='<?php echo base_url(); ?>assets/img/labti.png' rel='shortcut icon'>

	<!--Import Google Icon Font-->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/material-icons/material-icons.css" />

	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/materialize.min.css" />

	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<!-- header -->	
	<!-- <div class="navbar-fixed">
		<nav class="z-depth-3" style="background-color: #0f3057;">
		    <div class="nav-wrapper" style="padding: 10px">
		    	<a href="#!" class="brand-logo center"><img style="width: 60px" src="<?php echo base_url(); ?>assets/img/labti.png"></a>
	    	</div>
		</nav>
	</div> -->

  	<main>
		<!-- tabel user -->
	    <div class="section z-depth-3" style="margin: 10px">
	    	<div style="padding: 10px">
	    		<div class="center">
			        <img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
			        <h4 class="header center text-lighten-2"><b>Laporan Pelayanan Laboratorium Teknik Informatika</b></h4>
			    </div>
			    <div class="center">
			      	<br>
			      	<h5><b>Penanggung Jawab Laporan</b></h5>
			      	<?php foreach($user as $a): ?>
					<h5><?php echo $a['nama']; ?></h5>
					<h6><?php echo $a['id_user']; ?></h6>
					<?php endforeach; ?>
					<?php
		 				$tgl_skrg = date("d-M-Y");
					    switch(date("l"))
					    {
					        case 'Monday':$nmh="Senin";break; 
					        case 'Tuesday':$nmh="Selasa";break; 
					        case 'Wednesday':$nmh="Rabu";break; 
					        case 'Thursday':$nmh="Kamis";break; 
					        case 'Friday':$nmh="Jum'at";break; 
					        case 'Saturday':$nmh="Sabtu";break; 
					        case 'Sunday':$nmh="minggu";break; 
					    }
					    echo $nmh.","."$tgl_skrg";
					    echo"<br>";
		 			?>
			      	<br>
		      	</div>
		      	<div>
		      		<h5><b>Laporan Ketidakhadiran</b></h5>
		      		<table class="striped centered">
		      			<thead>
				          	<tr>
					          	<th>No.</th>
					            <th>Nama</th>
					            <th>Kelas</th>
					            <th>No. HP</th>
					            <th>Mata Praktikum</th>
					            <th>Pertemuan</th>
					            <th>Alasan</th>
					            <th>Tanggal Lapor</th>
					            <th>Tanggal Terkonfirmasi</th>
								<th>AT Verifikasi</th>	            
								<th>AT Blanko</th>	            
								<th>PJ Scanner</th>	            
					            <th>Status Laporan</th>
				          	</tr>
				        </thead>	
				        <tbody>
				        	<?php $no=1; foreach($record1 as $a): ?>
					            <tr style="font-size: 13px">
					              <td ><?php echo $no; ?></td>
					              <td ><?php echo $a['nama']; ?></td>
					              <td ><?php echo $a['kelas']; ?></td>
					              <td ><?php echo $a['no_hp']; ?></td>
					              <td ><?php echo $a['matprak']; ?></td>
					              <td ><?php echo $a['pertemuan']; ?></td>
					              <td ><?php echo $a['alasan']; ?></td>
					              <td ><?php echo $a['tgl_lapor']; ?></td>
					              <td ><?php echo $a['tgl_terkonfirmasi']; ?></td>
					              <td ><?php echo $a['admin_verifikasi']; ?></td>
					              <td ><?php echo $a['admin_blanko']; ?></td>
					              <td ><?php echo $a['pj']; ?></td>
					              <?php if($a['status_laporan']==1): ?>
					              	<td >Laporan Terkirim, Menunggu Upload Tugas</td>
					              <?php elseif($a['status_laporan']==2): ?>
					              	<td >Laporan Sedang Diproses..</td>
					              <?php elseif($a['status_laporan']==3): ?>
					              	<td >Laporan Sudah Terkonfirmasi, QRCode Aktif</td>
					              <?php elseif($a['status_laporan']==4): ?>
					              	<td >QRCode Terscanner, Menunggu Pembayaran Blanko</td>
					              <?php elseif($a['status_laporan']==5): ?>
					              	<td >Pembayaran Blanko Diterima, Menunggu Penilaian Tugas</td>
					              <?php elseif($a['status_laporan']==6): ?>
					              	<td ><?php echo $a['catatan']; ?></td>
					              <?php elseif($a['status_laporan']==7): ?>
					              	<td >Laporan Ditolak</td>
					              <?php elseif($a['status_laporan']==8): ?>
					              	<td >Laporan Ditolak</td>
					              <?php endif; ?>
					            </tr>
					        <?php $no++; endforeach; ?>
				        </tbody>
		      		</table>
		      	</div>
		      	<div>
		      		<h5><b>Laporan Kartu Hilang</b></h5>
		      		<table class="striped">
		      			<thead>
				          	<tr>
					          	<th class="center">No.</th>
					            <th class="center">Nama</th>
					            <th class="center">Kelas</th>
					            <th class="center">No. HP</th>
					            <th class="center">Tanggal Lapor</th>
					            <th class="center">Tanggal Terkonfirmasi</th>
								<th class="center">AT Verifikasi</th>	            
					            <th class="center">Status Laporan</th>
				          	</tr>
				        </thead>	
				        <tbody>
				        	<?php $no=1; foreach($record2 as $a): ?>
					            <tr style="font-size: 16px">
					              <td class="center"><?php echo $no; ?></td>
					              <td ><?php echo $a['nama']; ?></td>
					              <td ><?php echo $a['kelas']; ?></td>
					              <td ><?php echo $a['no_hp']; ?></td>
					              <td ><?php echo $a['tgl_lapor']; ?></td>
					              <td ><?php echo $a['tgl_terkonfirmasi']; ?></td>
					              <td ><?php echo $a['admin_verifikasi']; ?></td>
					              <td ><?php echo $a['catatan']; ?></td>
					            </tr>
					        <?php $no++; endforeach; ?>
				        </tbody>
		      		</table>
		      	</div>
		      	<div>
		      		<h5><b>Laporan Praktikan Transfer</b></h5>
		      		<table>
		      			<thead>
				          	<tr>
					          	<th class="center">No.</th>
					            <th class="center">Nama</th>
					            <th class="center">Kelas</th>
					            <th class="center">No. HP</th>
					            <th class="center">NPM Baru</th>
					            <th class="center">Kelas Baru</th>
					            <th class="center">Alasan</th>
					            <th class="center">Tanggal Lapor</th>
								<th class="center">AT Verifikasi</th>	            
				          	</tr>
				        </thead>	
				        <tbody>
				        	<?php $no=1; foreach($record3 as $a): ?>
					            <tr style="font-size: 16px">
					              <td class="center" ><?php echo $no; ?></td>
					              <td ><?php echo $a['nama']; ?></td>
					              <td class="center"><?php echo $a['kelas']; ?></td>
					              <td class="center"><?php echo $a['no_hp']; ?></td>
					              <td class="center"><?php echo $a['npm_baru']; ?></td>
					              <td class="center"><?php echo $a['kelas_baru']; ?></td>
					              <td ><?php echo $a['alasan']; ?></td>
					              <td class="center"><?php echo $a['tgl_lapor']; ?></td>
					              <td class="center"><?php echo $a['admin_verifikasi']; ?></td>
					            </tr>
					        <?php $no++; endforeach; ?>
				        </tbody>
		      		</table>
		      	</div>
	      	</div>
	    </div>
	    <!-- <script>
            window.print();
        </script> -->
	</main>

	<!--Import jQuery before materialize.js-->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/materialize.min.js"></script>
</body>
</html>