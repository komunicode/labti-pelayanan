<main>
  <!-- tabel user -->
    <div class="section z-depth-5" style="margin: 50px">
      <div style="padding: 10px">
      <div class="center">
        <img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
        <h4 class="header center text-lighten-2">Data Laporan Terkonfirmasi</h4>
      </div>
      <table id="example" class="highlight centered striped">
        <thead>
          <tr>
            <th>No</th>
            <th>NPM</th>
            <th>Nama</th>
            <th>Kelas</th>
            <th>Tanggal Lapor</th>
            <th>Tanggal Terkonfirmasi</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $no=1; foreach($record as $a): ?><!-- perulangan disini -->
            <tr class="z-depth-1">
              <td><?php echo $no; ?></td>
              <td><p class="truncate"><?php echo $a['npm']; ?></p></td>
              <td><p class="truncate"><?php echo $a['nama']; ?></p></td>
              <td><?php echo $a['kelas']; ?></td>
              <td><p class="truncate"><?php echo $a['tgl_lapor']; ?></p></td>
              <td><p class="truncate"><?php echo $a['tgl_terkonfirmasi']; ?></p></td>
              <?php if($a['status_laporan']==3): ?>
                <td ><b>Laporan Aktif, QRCode Aktif.</b></td>
              <?php elseif($a['status_laporan']==4): ?>
                <td><b class="amber-text">QRCode Terscanner, Menunggu Pembayaran Blanko</b></td>
              <?php elseif($a['status_laporan']==5): ?>
                <td><b class="indigo-text"><?php echo $a['catatan']; ?></b></td>
              <?php elseif($a['status_laporan']==6): ?>
                <td><b class="green-text"><?php echo $a['catatan']; ?></b></td>
              <?php endif ?>
              <td >
                <a href="#<?php echo $a['id_laporan']; ?>" class="btn-floating tooltipped waves-effect waves-light green darken-4 z-depth-4" data-position="buttom" data-delay="10" data-tooltip="Lihat Laporan"><i class="material-icons">launch</i></a>                  
              </td>
            </tr>
          <?php $no++; endforeach; ?><!-- akhir perulangan -->
        </tbody>
      </table>
      </div>
    </div>

    <!-- modal launch -->
    <?php foreach($record as $b): ?><!-- perulangan disini -->
      <div id="<?php echo $b['id_laporan']; ?>" class="modal modal-fixed-footer">
        <div class="modal-content">
          <!-- judul -->
          <div class="center">
            <img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
            <h4>Data Laporan Terkonfirmasi</h4>
          </div>
          <!-- bagian penampil data -->
          <div class="row">
            <div class="input-field col s6">
              <input readonly type="text" value="<?php echo $b['id_laporan']; ?>">
              <label >ID Laporan</label>
            </div>
            <div class="input-field col s6">
              <input readonly type="text" value="<?php echo $b['tgl_lapor']; ?>">
              <label >Tanggal Urus</label>
            </div>
            <div class="input-field col s3">
              <input readonly type="text" value="<?php echo $b['nama']; ?>">
              <label >Nama</label>
            </div>
            <div class="input-field col s3">
              <input readonly type="text" value="<?php echo $b['npm']; ?>">
              <label >NPM</label>
            </div>
            <div class="input-field col s3">
              <input readonly type="text" value="<?php echo $b['kelas']; ?>">
              <label >Kelas</label>
            </div>
            <div class="input-field col s3">
              <input readonly type="text" value="<?php echo $b['no_hp']; ?>">
              <label >No. HP</label>
            </div>
            <div class="input-field col s6">
              <input readonly type="text" value="<?php echo $b['matprak']; ?>">
              <label >Mata Praktikum</label>
            </div>
            <div class="input-field col s6">
              <input readonly type="text" value="<?php echo $b['pertemuan']; ?>">
              <label >Pertemuan</label>
            </div>
            <div class="input-field col s12">
              <input readonly type="text" value="<?php echo $b['alasan']; ?>">
              <label >Alasan</label>
            </div>
            <div class="input-field col s4">
              KTP : <br>
              <a href="<?php echo base_url(); ?>admin/ktp/<?php echo $b['ktp']; ?>" target="_blank"><img data-caption="<?php echo $b['ktp']; ?>" width="100" height="auto" src="<?php echo base_url(); ?>admin/ktp/<?php echo $b['ktp']; ?>"></a>
            </div>
            <div class="input-field col s4">
              Surat Keterangan : <br>
              <a href="<?php echo base_url(); ?>admin/surket/<?php echo $b['surket']; ?>" target="_blank"><img data-caption="<?php echo $b['surket']; ?>" width="100" height="auto" src="<?php echo base_url(); ?>admin/surket/<?php echo $b['surket']; ?>"></a>
            </div>
            <div class="input-field col s4">
              Tugas : <br>
              <a href="<?php echo base_url(); ?>admin/tugas/<?php echo $b['tugas']; ?>" target="_blank"><?php echo $b['tugas']; ?></a>
            </div>
          </div>
        </div>
        <!-- action modal launch -->        
        <?php if($b['status_laporan']==4): ?>
          <div class="modal-footer">
            <a href="<?php echo base_url(); ?>admin/KonfirmasiPembayaranBlanko/<?php echo $b['id_laporan']; ?>" class="modal-action modal-close waves-effect waves-green btn-flat ">Konfirmasi Pembayaran Blanko</a>
          </div>
        <?php else: ?>
        <?php endif; ?>
      </div>
    <?php endforeach; ?><!-- akhir perulangan -->
</main>