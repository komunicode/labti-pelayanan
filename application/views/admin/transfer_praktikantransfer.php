<main>
	<!-- tabel user -->
    <div class="section z-depth-5" style="margin: 50px">
      <div style="padding: 10px">
        <div class="center">
          <img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
            <h4 class="header center text-lighten-2">Data Praktikan Transfer</h4>
        </div>
        <table id="example" class="highlight centered">
          <thead>
            <tr>
              <th>No</th>
              <th>NPM User</th>
              <th>Nama</th>
              <th>Kelas</th>
              <th>Tanggal Lapor</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($record as $a): ?><!-- perulangan disini -->
            <tr class="z-depth-1">
              <td><?php echo $no; ?></td>
              <td><?php echo $a['npm']; ?></td>
              <td><?php echo $a['nama']; ?></td>
              <td><?php echo $a['kelas']; ?></td>
              <td><?php echo $a['tgl_lapor']; ?></td>
              <td class="center">
                <a href="#<?php echo $a['id_laporan']; ?>" class="btn-floating waves-effect waves-light green darken-4 z-depth-4"><i class="material-icons">launch</i></a>
                <a href="<?php echo base_url(); ?>admin/DeletePraktikanTransfer/<?php echo $a['id_laporan']; ?>" class="red darken-4 btn-floating waves-effect waves-light z-depth-4" onclick="return  confirm('Hapus Praktikan Transfer ?')"><i class="material-icons">delete</i></a>
              </td>
            </tr>
            <?php $no++; endforeach; ?><!-- akhir perulangan -->
          </tbody>
        </table>
      </div>
    </div>

    <!-- modal launch -->
    <?php foreach($record as $b): ?><!-- perulangan disini -->
    <div id="<?php echo $b['id_laporan']; ?>" class="modal">
      	<div class="modal-content">
	        <!-- judul -->
	        <div class="center">
	          	<img style="width: 100px" src="<?php echo base_url(); ?>assets/img/labti.png">
	          	<h4>Data Praktikan Transfer</h4>
	        </div>
	        <!-- bagian penampil data -->
	        <div class="row">
	        	<div class="input-field col s6">
		        	<input readonly type="text" value="<?php echo $b['id_laporan']; ?>">
		          	<label >ID Laporan</label>
		        </div>
		        <div class="input-field col s6">
		        	<input readonly type="text" value="<?php echo $b['tgl_lapor']; ?>">
		          	<label >Tanggal Lapor</label>
		        </div>
		        <div class="input-field col s3">
		        	<input readonly type="text" value="<?php echo $b['nama']; ?>">
		          	<label >Nama</label>
		        </div>
		        <div class="input-field col s3">
		        	<input readonly type="text" value="<?php echo $b['npm']; ?>">
		          	<label >NPM</label>
		        </div>
		        <div class="input-field col s3">
		        	<input readonly type="text" value="<?php echo $b['kelas']; ?>">
		          	<label >Kelas</label>
		        </div>
		        <div class="input-field col s3">
		        	<input readonly type="text" value="<?php echo $b['no_hp']; ?>">
		          	<label >No. Handphone</label>
		        </div>
		        <div class="input-field col s6">
		        	<input readonly type="text" value="<?php echo $b['npm_baru']; ?>">
		          	<label >NPM Baru</label>
		        </div>
		        <div class="input-field col s6">
		        	<input readonly type="text" value="<?php echo $b['kelas_baru']; ?>">
		          	<label >Kelas Baru</label>
		        </div>
		        <div class="input-field col s12">
	          		<textarea readonly id="textarea1" class="materialize-textarea"><?php echo $b['alasan']; ?></textarea>
	          		<label for="textarea1">Alasan Pindah</label>
	        	</div>
	        	<div class="input-field col s12">
		        	<input readonly type="text" value="<?php echo $b['admin_verifikasi']; ?>">
		          	<label >Admin</label>
		        </div>
		        <div class="input-field col s12">
	          		<textarea readonly id="textarea1" class="materialize-textarea"><?php echo $b['catatan']; ?></textarea>
	          		<label for="textarea1">Catatan</label>
	        	</div>
	        </div>
      	</div>
    </div>
    <?php endforeach; ?><!-- akhir perulangan -->	

    <div class="fixed-action-btn">
	    <a class="btn-floating tooltipped btn-large green z-depth-4 " data-position="left" data-delay="10" data-tooltip="Tambah Praktikan Transfer" href="<?php echo base_url(); ?>admin/transfer_cekdatamahasiswa">
	      <i class="large material-icons">person_add</i>
	    </a>
    </div>
</main>