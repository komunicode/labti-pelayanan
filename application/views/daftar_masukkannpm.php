<!-- parallax -->
<div class="section z-depth-5" style="background: url(<?php echo base_url(); ?>assets/img/5.jpg); background-repeat: no-repeat; background-position: center;  background-size: cover;">
  <!-- Text -->
  <div class="row">
    <div class="col s12 m6">
    </div>
    <div class="col s12 m6" style="margin-top: 2%">
      <div class="container row">
        <div class="center">
          <img style="width: 150px" src="<?php echo base_url(); ?>assets/img/labti.png">
          <h5><b>Pelayanan Laboratorium Teknik Informatika</b></h5>
          <hr>
        </div>
        <div class="col s12" style="background-color: rgba(15,48,87,0.2); padding: 15px">
          <form method="post" action="<?php echo base_url(); ?>welcome/daftar_buatakun">
            <div class="input-field col s12 m12">
              <input type="text" class="validate" name="npm" required>
              <label Class="black-text" for="icon_prefix">Masukkan NPM Anda</label>
            </div>
            <div class="center">
              <input type="submit" class="blue waves-effect waves-light btn" value="BUAT AKUN"> 
            </div>
          </form>
          <div class="center" style="padding: 20px">
            <b>Sudah Punya Akun ? Klik </b> <a href="<?php echo base_url(); ?>welcome">disini</a>
          </div>
        </div>
      </div>
    </div>
    <div class="col s12">
      <br><br><br><br><br><br><br><br><br><br>
    </div>
  </div>
</div>

<div class="parallax-container z-depth-3">
  <div class="parallax"><img src="<?php echo base_url(); ?>assets/img/6.jpg"></div>
  <div class="center" style="margin-top: 10% ">
    <img style="width: 20%" src="<?php echo base_url(); ?>assets/img/labti.png">
    <h5><b>Pelayanan Laboratorium Teknik Informatika</b></h5>
  </div>
</div>