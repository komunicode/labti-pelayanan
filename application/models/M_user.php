<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_user extends CI_Model {
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }
    function getUser($iduser){
        $this->db->where('id_user',$iduser);
        $user = $this->db->get('user');
        return $user->result_array();
    }

    /*PROSES RIWAYAT*/
    function getLaporanAktif($id_user)
    {
        $this->db->where('id_user',$id_user);
        $this->db->join('matprak', 'matprak.id_matprak = laporan.id_matprak');
        $this->db->join('tugas', 'tugas.id_tugas = laporan.id_tugas');
        $this->db->order_by('tgl_lapor', 'desc');
        $laporan = $this->db->get('laporan');
        return $laporan->result_array();
    }
    function getLaporanDitolak($id_user)
    {
        $this->db->where('id_user',$id_user);
        $this->db->join('matprak', 'matprak.id_matprak = laporan_ditolak.id_matprak');
        $this->db->order_by('tgl_lapor', 'desc');
        $laporan = $this->db->get('laporan_ditolak');
        return $laporan->result_array();
    }
    function getLaporan($idlaporan)
    {
        $this->db->where('id_laporan',$idlaporan);
        $this->db->join('user', 'user.id_user = laporan.id_user');
        $this->db->join('matprak', 'matprak.id_matprak = laporan.id_matprak');
        $this->db->join('tugas', 'tugas.id_tugas = laporan.id_tugas');
        $laporan = $this->db->get('laporan');
        return $laporan->result_array();
    }
    function getCetakLaporan($idlaporan,$iduser)
    {
        $this->db->where('laporan.id_user',$iduser);
        $this->db->where('laporan.id_laporan',$idlaporan);
        $this->db->join('user', 'user.id_user = laporan.id_user');
        $this->db->join('matprak', 'matprak.id_matprak = laporan.id_matprak');
        $laporan = $this->db->get('laporan');
        return $laporan->result_array();
    }
    function CekDeleteLaporan($idlaporan){
        $this->db->where('id_laporan',$idlaporan);
        $laporan = $this->db->get('laporan');
        return $laporan->result_array();
    }
    function DeleteLaporan($idlaporan)
    {
        $this->db->where('id_laporan',$idlaporan);
        $this->db->delete('laporan');
        return TRUE;
    }
    /*PROSES RIWAYAT*/

    /*INPUT PELAYANAN*/
    function getPraktikum($tingkat){
        $this->db->where('tingkat',$tingkat);   
        $matprak = $this->db->get('matprak');
        return $matprak->result_array();
    }
    function getCekPertemuanMatprak($id_matprak){
        $this->db->select('jum_pertemuan');
        $this->db->where('id_matprak',$id_matprak);
        $pertemuan = $this->db->get('matprak');
        return $pertemuan->result_array();
    }
    function getCekLapor($id_matprak,$iduser,$pertemuan){
        $this->db->where('id_matprak',$id_matprak);
        $this->db->where('id_user',$iduser);
        $this->db->where('pertemuan',$pertemuan);
        $cek = $this->db->get('laporan');
        return $cek->result_array();
    }
    function getCekBanyakLapor($id_matprak,$iduser){
        $this->db->where('id_matprak',$id_matprak);
        $this->db->where('id_user',$iduser);
        $this->db->order_by('banyak_lapor', 'desc');
        $cek = $this->db->get('laporan');
        return $cek->result_array();
    }
    function getCekTugas($id_tugas){
        $this->db->where('id_tugas',$id_tugas);
        $cek = $this->db->get('tugas');
        return $cek->result_array();
    }
    function InputLaporanPraktikan($input){
        $this->db->insert('laporan',$input);
        return TRUE;
    }
    function InputTugasPraktikan($input){
        $this->db->where('id_laporan',$input['id_laporan']);
        $this->db->set('tugas',$input['tugas']);
        $this->db->set('status_laporan',$input['status_laporan']);
        $this->db->set('Catatan',$input['catatan']);
        $this->db->update('laporan');
        return TRUE;
    }
    /*INPUT PELAYANAN*/

    
}   