<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_admin extends CI_Model {
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /*COUNT NAVBAR*/
    function getPraktikan(){
        $this->db->where('level','1');
        $praktikan = $this->db->get('user');
        return $praktikan->result_array();
    }
    function getAT(){
        $this->db->where('level','2');
        $at = $this->db->get('user');
        return $at->result_array();
    }
    function getPJ(){
        $this->db->where('level','3');
        $pj = $this->db->get('user');
        return $pj->result_array();
    }
    /*COUNT NAVBAR*/


    function getAdmin($iduser)
    {
        $this->db->where('id_user',$iduser);
        $user = $this->db->get('user');
        return $user->result_array();
    }

    /*CRUD USER*/
    function getUser($level)
    {
        $this->db->where('level',$level);
        $user = $this->db->get('user');
        return $user->result_array();
    }
    function CekDataUser($npm)
    {
        $this->db->where('npm',$npm);
        $user = $this->db->get('data_mahasiswa');
        return $user->result_array();
    }
    function CekIdUser($iduser)
    {
        $this->db->where('id_user',$iduser);
        $user = $this->db->get('user');
        return $user->result_array();
    }
    function CekUsername($username)
    {
        $this->db->where('username',$username);
        $user = $this->db->get('user');
        return $user->result_array();
    }
    function UserTambahUser($input)
    {
        $this->db->insert('user',$input);
        return TRUE;
    }
    function HapusUser($iduser)
    {
        $this->db->where('id_user',$iduser);
        $this->db->delete('user');
        return TRUE;
    }
    function UpdateUser($input)
    {
        $this->db->where('id_user',$input['id_user']);
        $this->db->Update('user',$input);
        return TRUE;
    }
    function akses_aktif($iduser)
    {
        $this->db->where('id_user',$iduser);
        $this->db->set('akses','2');
        $this->db->Update('user');
        return TRUE;
    }
    function akses_nonaktif($iduser)
    {
        $this->db->where('id_user',$iduser);
        $this->db->set('akses','1');
        $this->db->Update('user');
        return TRUE;
    }
    function akses_nonaktifall()
    {
        $this->db->where('level','3');
        $this->db->set('akses','1');
        $this->db->Update('user');
        return TRUE;
    }
    /*CRUD USER*/

    /*CRUD LAPORAN*/
    function getLaporanMasuk()
    {
        $this->db->where('tipe_laporan','1');
        $this->db->where("(status_laporan='1' OR status_laporan='2')", NULL, FALSE);
        $this->db->join('user', 'user.id_user = laporan.id_user');
        $this->db->join('matprak', 'matprak.id_matprak = laporan.id_matprak');
        $this->db->order_by('tgl_lapor', 'desc');
        $laporan = $this->db->get('laporan');
        return $laporan->result_array();
    }
    function getKonfirmasiLaporanMasuk($idlaporan)
    {
        $this->db->where('id_laporan',$idlaporan);
        $this->db->join('user', 'user.id_user = laporan.id_user');
        $this->db->join('matprak', 'matprak.id_matprak = laporan.id_matprak');
        $laporan = $this->db->get('laporan');
        return $laporan->result_array();
    }
    function TerimaLaporanMasuk($input)
    {
        $this->db->where('id_laporan',$input['id_laporan']);
        $this->db->set('tgl_terkonfirmasi',$input['tgl_terkonfirmasi']);
        $this->db->set('admin_verifikasi',$input['admin_verifikasi']);
        $this->db->set('status_laporan',$input['status_laporan']);
        $this->db->set('catatan',$input['catatan']);
        $this->db->update('laporan');
        return TRUE;
    }
    function TolakLaporanMasuk($input)
    {
        $this->db->insert('laporan_ditolak',$input);
        $this->db->where('id_laporan',$input['id_laporan']);
        $this->db->delete('laporan');
        return TRUE;
    }
    function getLaporanTerkonfirmasi()
    {
        $this->db->where('tipe_laporan','1');
        $this->db->where("(status_laporan='3' OR status_laporan='4' OR status_laporan='5' OR status_laporan='6')", NULL, FALSE);
        $this->db->join('user', 'user.id_user = laporan.id_user');
        $this->db->join('matprak', 'matprak.id_matprak = laporan.id_matprak');
        $this->db->order_by('tgl_lapor', 'desc');
        $laporan = $this->db->get('laporan');
        return $laporan->result_array();
    }
    function getLaporanTidakTerkonfirmasi()
    {
        $this->db->where('tipe_laporan','1');
        $this->db->where("(status_laporan='7' OR status_laporan='8')", NULL, FALSE);
        $this->db->join('user', 'user.id_user = laporan_ditolak.id_user');
        $this->db->join('matprak', 'matprak.id_matprak = laporan_ditolak.id_matprak');
        $this->db->order_by('tgl_lapor', 'desc');
        $laporan = $this->db->get('laporan_ditolak');
        return $laporan->result_array();
    }
    function KonfirmasiPembayaranBlanko($input){
        $this->db->where('id_laporan',$input['id_laporan']);
        $this->db->set('admin_blanko',$input['admin_blanko']);
        $this->db->set('status_laporan',$input['status_laporan']);
        $this->db->set('catatan',$input['catatan']);
        $this->db->update('laporan');
        return TRUE;
    }
    /*CRUD LAPORAN*/

    /*CRUD LAPORAN KHUSUS*/
    function CekAksesUser($iduser)
    {
        $this->db->where('id_user',$iduser);
        $this->db->where('akses','1');
        $a = $this->db->get('user');
        return $a->result_array();
    }

    function getCaseKhususPraktikan($npm)
    {
        $this->db->where('npm',$npm);
        $a = $this->db->get('user');
        return $a->result_array();
    }
    function getPraktikanKhusus($npm)
    {
        $this->db->where('npm',$npm);
        $this->db->where('level','1');
        $a = $this->db->get('user');
        return $a->result_array();
    }
    function getCekTugas($id_tugas){
        $this->db->where('id_tugas',$id_tugas);
        $cek = $this->db->get('tugas');
        return $cek->result_array();
    }
    function getCekKesamaanPertemuan($id_user,$id_matprak,$pertemuan){
        $this->db->where('id_user',$id_user);
        $this->db->where('id_matprak',$id_matprak);
        $this->db->where('pertemuan',$pertemuan);
        $a = $this->db->get('laporan');
        return $a->result_array();
    }
    function InputLaporanPraktikanKhusus($input)
    {
        $this->db->insert('laporan',$input);
        return TRUE;
    }
    /*CRUD LAPORAN KHUSUS*/

    /*CRUD LAPORAN KARTU HILANG*/
    function getLaporanKartuHilang()
    {
        $this->db->where('tipe_laporan','2');
        $this->db->join('user', 'user.id_user = laporan.id_user');
        $this->db->join('matprak', 'matprak.id_matprak = laporan.id_matprak');
        $this->db->order_by('tgl_lapor', 'desc');
        $laporan = $this->db->get('laporan');
        return $laporan->result_array();
    }
    function getKonfirmasiLaporanKartuHilang($idlaporan)
    {
        $this->db->where('id_laporan',$idlaporan);
        $this->db->join('user', 'user.id_user = laporan.id_user');
        $this->db->join('matprak', 'matprak.id_matprak = laporan.id_matprak');
        $laporan = $this->db->get('laporan');
        return $laporan->result_array();
    }
    function TerimaLaporanKartuHilang($input)
    {
        $this->db->where('id_laporan',$input['id_laporan']);
        $this->db->set('tgl_terkonfirmasi',$input['tgl_terkonfirmasi']);
        $this->db->set('admin_verifikasi',$input['admin_verifikasi']);
        $this->db->set('status_laporan',$input['status_laporan']);
        $this->db->set('catatan',$input['catatan']);
        $this->db->update('laporan');
        return TRUE;
    }
    /*CRUD LAPORAN KARTU HILANG*/

    /*CRUD LAPORAN PRAKTIKAN TRANSFER*/
    function getLaporanPrakTrans()
    {
        $this->db->where('tipe_laporan','3');
        $this->db->join('user', 'user.id_user = laporan.id_user');
        $this->db->join('matprak', 'matprak.id_matprak = laporan.id_matprak');
        $this->db->order_by('tgl_lapor', 'desc');
        $laporan = $this->db->get('laporan');
        return $laporan->result_array();
    }
    function TransferCekDataMahasiswa($npm)
    {
        $this->db->where('npm',$npm);
        $user = $this->db->get('data_mahasiswa');
        return $user->result_array();
    }
    function getMataPraktikumTransfer()
    {
        $this->db->where("(tingkat='1' OR tingkat='2' OR tingkat='3' OR tingkat='4')", NULL, FALSE);
        $this->db->order_by('tingkat', 'asc');
        $a = $this->db->get('matprak');
        return $a->result_array();
    }
    function getKelasTransfer()
    {   
        $this->db->distinct();
        $this->db->select('kelas');
        $this->db->order_by('kelas', 'asc');
        $a = $this->db->get('data_mahasiswa');
        return $a->result_array();
    }
    function CekNpmBaru($npm){
        $this->db->where('npm',$npm);
        $user = $this->db->get('data_mahasiswa');
        return $user->result_array();
    }
    function CekKesamaanUsername($user){
        $this->db->where('username',$user);
        $user = $this->db->get('user');
        return $user->result_array();
    }
    function InputUserBaru1($input)
    {
        $this->db->insert('data_mahasiswa',$input);
        return TRUE;
    }  
    function InputUserBaru2($input)
    {
        $this->db->insert('user',$input);
        return TRUE;
    }
    function InputLaporanPrakTrans($input)
    {
        $this->db->insert('laporan',$input);
        return TRUE;
    }
    function AmbilPraktikanTransfer($idlaporan){
        $this->db->where('id_laporan',$idlaporan);
        $laporan = $this->db->get('laporan');
        return $laporan->result_array();   
    }
    function DeleteLaporanPrakTransfer1($idlaporan)
    {  
        $this->db->where('id_laporan',$idlaporan);
        $this->db->delete('laporan');
        return TRUE;
    }
    function DeleteLaporanPrakTransfer2($iduser)
    {  
        $this->db->where('id_user',$iduser);
        $this->db->delete('user');
        return TRUE;
    }
    function DeleteLaporanPrakTransfer3($npm)
    {  
        $this->db->where('npm',$npm);
        $this->db->delete('data_mahasiswa');
        return TRUE;
    }
    
    /*CRUD LAPORAN PRAKTIKAN TRANSFER*/

    /*CRUD MATPRAK*/
    function getMataPraktikum()
    {
        $this->db->where("(tingkat='1' OR tingkat='2' OR tingkat='3' OR tingkat='4')", NULL, FALSE);
        $matprak = $this->db->get('matprak');
        return $matprak->result_array();
    }
    function getMatPrak($tingkat)
    {
        $this->db->where('tingkat',$tingkat);
        $matprak = $this->db->get('matprak');
        return $matprak->result_array();
    }
    function getLihatMataPraktikum($idmatprak)
    {
        $this->db->where('id_matprak',$idmatprak);
        $matprak = $this->db->get('matprak');
        return $matprak->result_array();
    }
    function UpdateMataPraktikum($input)
    {
        $this->db->where('id_matprak',$input['id_matprak']);
        $this->db->Update('matprak',$input);
        return TRUE;
    }
    function getTingkatMatprak($idmatprak)
    {
        $this->db->select('tingkat');
        $this->db->where('id_matprak',$idmatprak);
        $matprak = $this->db->get('matprak');
        return $matprak->result_array();
    }
    function DeleteMataPraktikum($idmatprak)
    {
        $this->db->where('id_matprak',$idmatprak);
        $this->db->delete('matprak');
        return TRUE;
    }
    function TambahMataPraktikum($input)
    {
        $this->db->insert('matprak',$input);
        return TRUE;
    }    
    /*CRUD MATPRAK*/

    /*CRUD TUGAS PRAKTIKAN*/
    function getTugasPraktikan()
    {
        $this->db->where("(pertemuan='1' OR pertemuan='2' OR pertemuan='3' OR pertemuan='4' OR pertemuan='5' OR pertemuan='6' OR pertemuan='7')", NULL, FALSE);
        $this->db->join('matprak', 'matprak.id_matprak = tugas.id_matprak');
        $this->db->order_by('kelas', 'asc');
        $tugas = $this->db->get('tugas');
        return $tugas->result_array();
    }
    /*CRUD TUGAS PRAKTIKAN*/

    /*CRUD DATA MAHASISWA*/
    function getDataMahasiswa()
    {
        $this->db->order_by('kelas', 'asc');
        $mhs = $this->db->get('data_mahasiswa');
        return $mhs->result_array();
    }
    function CekKesamaanNPM($npm)
    {
        $this->db->where('npm',$npm);
        $mhs = $this->db->get('data_mahasiswa');
        return $mhs->result_array();
    }
    function TambahDataMahasiswa($input)
    {
        $this->db->insert('data_mahasiswa',$input);
        return TRUE;
    }
    function UbahDataMahasiswa($input)
    {
        $this->db->where('npm',$input['npm']);
        $this->db->update('data_mahasiswa',$input);
        return TRUE;
    }  
    function HapusDataMahasiswa($npm)
    {
        $this->db->where('npm',$npm);
        $this->db->delete('data_mahasiswa');
        return TRUE;
    }

    /*CRUD DATA MAHASISWA*/

    /*CETAK LAPORAN PELAYANAN*/
    function getCetakLaporanKetidakhadiran()
    {
        $this->db->where('tipe_laporan','1');
        $this->db->join('user', 'user.id_user = laporan.id_user');
        $this->db->join('matprak', 'matprak.id_matprak = laporan.id_matprak');
        $this->db->order_by('laporan.id_matprak', 'asc');
        $this->db->order_by('tgl_lapor', 'asc');
        $laporan = $this->db->get('laporan');
        return $laporan->result_array();
    }
    function getCetakLaporanKartuhilang()
    {
        $this->db->where('tipe_laporan','2');
        $this->db->join('user', 'user.id_user = laporan.id_user');
        $this->db->join('matprak', 'matprak.id_matprak = laporan.id_matprak');
        $this->db->order_by('tgl_lapor', 'asc');
        $laporan = $this->db->get('laporan');
        return $laporan->result_array();
    }
    function getCetakLaporanPraktikantransfer()
    {
        $this->db->where('tipe_laporan','3');
        $this->db->join('user', 'user.id_user = laporan.id_user');
        $this->db->join('matprak', 'matprak.id_matprak = laporan.id_matprak');
        $this->db->order_by('tgl_lapor', 'asc');
        $laporan = $this->db->get('laporan');
        return $laporan->result_array();
    }

    /*CETAK LAPORAN PELAYANAN*/
}   