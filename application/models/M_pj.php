<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_pj extends CI_Model {
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function getPj($iduser)
    {
        $this->db->where('id_user',$iduser);
        $user = $this->db->get('user');
        return $user->result_array();
    }    

    /*CRUD NILAI TUGAS*/
    function getDataTugas($namapj)
    {
        $this->db->where('pj',$namapj);
        $this->db->where("(status_laporan='4' OR status_laporan='5' OR status_laporan='6')", NULL, FALSE);
        $this->db->join('user', 'user.id_user = laporan.id_user');
        $this->db->join('matprak', 'matprak.id_matprak = laporan.id_matprak');
        $this->db->join('tugas', 'tugas.id_tugas = laporan.id_tugas');
        $tugas = $this->db->get('laporan');
        return $tugas->result_array();
    }
    function getPenilaianDataTugas($idlaporan)
    {
        $this->db->where('id_laporan',$idlaporan);
        $this->db->where('status_laporan','5');
        $this->db->join('user', 'user.id_user = laporan.id_user');
        $this->db->join('matprak', 'matprak.id_matprak = laporan.id_matprak');
        $this->db->join('tugas', 'tugas.id_tugas = laporan.id_tugas');
        $tugas = $this->db->get('laporan');
        return $tugas->result_array();
    }
    function NilaiTugasPraktikan($input){
        $this->db->where('id_laporan',$input['id_laporan']);
        $this->db->set('nilai',$input['nilai']);
        $this->db->set('status_laporan',$input['status']);
        $this->db->set('catatan',$input['catatan']);
        $this->db->update('laporan');
        return TRUE;
    }
    /*CRUD NILAI TUGAS*/
}   