<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class m_login extends CI_Model {

	function getUserBaru($npm){
		$this->db->where('npm',$npm);
		$user = $this->db->get('data_mahasiswa');
        return $user->result_array();
	}
	function getCekIdUser($iduser){
		$this->db->where('id_user',$iduser);
		$user = $this->db->get('user');
        return $user->result_array();
	}
	function getCekUsername($username){
		$this->db->where('username',$username);
		$user = $this->db->get('user');
        return $user->result_array();
	}
	function InputUserBaru($input){
		$this->db->insert('user',$input);
        return TRUE;
	}

    //untuk login admin dan menyimpan sesi untuk admin
    function cek_login($username, $password) {
        $this->db->where('username',$username);
        $this->db->where('password',$password);
        $a = $this->db->get('user');
        return $a;
    }
}