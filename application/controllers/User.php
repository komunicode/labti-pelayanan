<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_user');
		if($this->session->userdata('logged_in')){
			if($this->session->userdata('role')==2){
				redirect('admin');
			}
			elseif($this->session->userdata('role')==3){
				redirect('pj');
			}
		}
		else{
			redirect('welcome');
		}
	}
	public function index()
	{
		$iduser 					= $this->session->userdata('iduser');
		$data['user'] 				= $this->m_user->getUser($iduser);

		$data['lap_aktif']			= $this->m_user->getLaporanAktif($iduser);
		$data['lap_ditolak']		= $this->m_user->getLaporanDitolak($iduser);

		$this->load->view('user/header',$data);
		$this->load->view('user/index');
		$this->load->view('user/footer');
	}

	/*INPUT PELAYANAN*/
	public function laporan_pilihkategori()
	{
		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_user->getUser($iduser);

		$this->load->view('user/header',$data);
		$this->load->view('user/laporan_pilihkategori');
		$this->load->view('user/footer');
	}
	public function laporan_pilihpraktikum($idkategori)
	{
		if($idkategori!=null){
			if($idkategori==1){
				$alasan		= 'Sakit, Rawat Inap';
			}
			elseif($idkategori==2){
				$alasan		= 'Sakit, Non-Rawat Inap';
			}
			elseif($idkategori==3){
				$alasan		= 'Terlambat, Keterlambatan Kereta';
			}
			elseif($idkategori==4){
				$alasan		= 'Terlambat, Ban Bocor';
			}
			elseif($idkategori==5){
				$alasan		= 'Terlambat, Lainnya';
			}
			elseif($idkategori==6){
				$alasan		= 'Izin, Acara Keluarga';
			}
			elseif($idkategori==7){
				$alasan		= 'Izin, Acara Kampus';
			}
			elseif($idkategori==8){
				$alasan		= 'Izin, Lainnya';
			}
			$data['alasan']	= $alasan;

			$iduser 		= $this->session->userdata('iduser');
			$data['user'] 	= $this->m_user->getUser($iduser);
			$a 				= $data['user'][0];
			$tingkat		= substr($a['kelas'],0,1);
			$data['mp']		= $this->m_user->getPraktikum($tingkat);

			$this->load->view('user/header',$data);
			$this->load->view('user/laporan_pilihpraktikum');
			$this->load->view('user/footer');	
		}
		else{
			redirect('user/laporan_pilihkategori');
		}
		
	}
	public function laporan_inputlaporan()
	{
		$data['alasan']		= $this->input->post('alasan');
		$id_matprak			= $this->input->post('id_matprak');
		$data['id_matprak']	= $id_matprak;
		$pertemuan 			= $this->input->post('pertemuan');
		$data['pertemuan']	= $pertemuan;
		$data['alasan1']	= $this->input->post('alasan1');

		$iduser 			= $this->session->userdata('iduser');
		$data['user'] 		= $this->m_user->getUser($iduser);

		if($id_matprak!=null){
			/*validasi matprak dan pertemuan*/
			$data['vmp'] 		= $this->m_user->getCekPertemuanMatprak($id_matprak);
			$hasil				= $data['vmp'][0];
			$vmp				= $hasil['jum_pertemuan'];
			if($pertemuan<=$vmp){
				/*cek laporan*/
				$data['cek'] 		= $this->m_user->getCekLapor($id_matprak,$iduser,$pertemuan);
				if($data['cek']!=null){
					echo 
						"<script>
		            		alert('Input Laporan Gagal !! Anda Sudah Pernah Melapor Pada Praktikum dan Pertemuan Ini !!');
		            		history.go(-1);
		            		window.location.reload();
		            	</script>";
				}
				else{
					/*cek batas lapor*/
					$data['cek1'] 	= $this->m_user->getCekBanyakLapor($id_matprak,$iduser);
					if($data['cek1']!=null){
						$bl 			= $data['cek1'][0];
						$banyaklapor 	= $bl['banyak_lapor'];
						if(($vmp>4) AND ($banyaklapor>=2)){
							echo 
							"<script>
			            		alert('Input Laporan Gagal !! Anda Sudah Melebihi 2x Batas Lapor pada Praktikum Ini !!');
			            		window.location.href='index';
			            	</script>";
						}
						elseif(($vmp<=4) AND ($banyaklapor>=1)){
							echo 
							"<script>
			            		alert('Input Laporan Gagal !! Anda Sudah Melebihi 1x Batas Lapor pada Praktikum Ini !!');
			            		window.location.href='index';
			            	</script>";
						}
						else{
			            	/*kasih id tugas*/
			            	$a 					= $data['user'][0];
							$kelas 				= $a['kelas'];
							$data['id_tugas'] 	= $kelas."-".$id_matprak.".".$pertemuan;

							$this->load->view('user/header',$data);
							$this->load->view('user/laporan_inputlaporan');
							$this->load->view('user/footer');
						}
					}
					else{
						/*kasih id tugas*/
			            $a 					= $data['user'][0];
						$kelas 				= $a['kelas'];
						$data['id_tugas'] 	= $kelas."-".$id_matprak.".".$pertemuan;

						$this->load->view('user/header',$data);
						$this->load->view('user/laporan_inputlaporan');
						$this->load->view('user/footer');
					}
											
				}
			}
			else{
				echo 
					"<script>
	            		alert('Input Laporan Gagal !! Pertemuan yang Anda Masukkan Salah !!');
	            		history.go(-1);
	            		window.location.reload();
	            	</script>";
			}
		}
		else{
			redirect('user/laporan_pilihpraktikum');
		}
	}
	public function KonfirmasiInputLaporan(){
		$id_matprak	= $this->input->post('id_matprak');
		$id_user	= $this->input->post('id_user');
		$pertemuan	= $this->input->post('pertemuan');
		$alasan		= $this->input->post('alasan');
		$no_hp		= $this->input->post('no_hp');
		$ktp		= $this->input->post('ktp');
		$surket		= $this->input->post('surket');
		$id_tugas	= $this->input->post('id_tugas');
		$tipelaporan= '1';

		date_default_timezone_set("Asia/Bangkok");
		$date 		= date("m-d-Y_H:i:s");
		$a 			= date("mdYHis");

		$id_laporan = $tipelaporan.'-'.$a.'-'.$id_user.'-'.$id_matprak.'-'.$pertemuan;

		/*cek ketersediaan tugas*/
		$cek['cektugas'] = $this->m_user->getCekTugas($id_tugas);
		if($cek['cektugas']!=null){
			if(($ktp==null) AND ($surket==null)){
				$data['cbl'] = $this->m_user->getCekBanyakLapor($id_matprak,$id_user);
		        if($data['cbl']==null){
		        	$input = array(
						'id_laporan'		=>$id_laporan,
						'id_matprak'		=>$id_matprak,
						'id_user'			=>$id_user,
						'npm_baru'			=>'',
						'kelas_baru'		=>'',
						'pertemuan'			=>$pertemuan,
						'tgl_lapor'			=>$date,
						'tgl_terkonfirmasi'	=>'',
						'no_hp'				=>$no_hp,
						'alasan'			=>$alasan,
						'ktp'				=>'',
						'surket'			=>'',
						'id_tugas'			=>$id_tugas,
						'tugas'				=>'',
						'pj'				=>'',
						'admin_verifikasi'	=>'',
						'admin_blanko'		=>'',
						'tipe_laporan'		=>$tipelaporan,
						'status_laporan'	=>'1',
						'banyak_lapor'		=>'1',
						'catatan'			=>'Laporan Terkirim !! Silahkan Upload Tugas !!',
						'nilai'				=>'',
					);	
					$this->m_user->InputLaporanPraktikan($input);
					echo 
						"<script>
							alert('Laporan Terkirim !! Silahkan Lihat Instruksi Selanjutnya !!');
							window.location.href='index';
						</script>";	
		        }
		        else{
		        	$a 		= $data['cbl'][0];
					$b 		= $a['banyak_lapor'];
					$c 		= $b + 1;
					$input 	= array(
						'id_laporan'		=>$id_laporan,
						'id_matprak'		=>$id_matprak,
						'id_user'			=>$id_user,
						'npm_baru'			=>'',
						'kelas_baru'		=>'',
						'pertemuan'			=>$pertemuan,
						'tgl_lapor'			=>$date,
						'tgl_terkonfirmasi'	=>'',
						'no_hp'				=>$no_hp,
						'alasan'			=>$alasan,
						'ktp'				=>'',
						'surket'			=>'',
						'id_tugas'			=>$id_tugas,
						'tugas'				=>'',
						'pj'				=>'',
						'admin_verifikasi'	=>'',
						'admin_blanko'		=>'',
						'tipe_laporan'		=>$tipelaporan,
						'status_laporan'	=>'1',
						'banyak_lapor'		=>$c,
						'catatan'			=>'Laporan Terkirim !! Silahkan Upload Tugas !!',
						'nilai'				=>'',
					);	
					$this->m_user->InputLaporanPraktikan($input);
					echo 
						"<script>
							alert('Laporan Terkirim !! Silahkan Lihat Instruksi Selanjutnya !!');
							window.location.href='index';
						</script>";
		        }

			}
			elseif(($ktp==null) AND ($surket!=null)){
				$filesurket = 'surket-'.$id_laporan.'.jpg';

				$this->load->library('upload');
				$config['upload_path']  = 'pelayanan/surket';
		        $config['allowed_types']= 'jpg';
		        $config['max_size']     = '5000';
				$config['file_name'] 	=  $filesurket;
				$this->upload->initialize($config);

				if ( ! $this->upload->do_upload('surket1')){
		            echo 
			            "<script>
			            	alert('File Gagal Terupload !! Periksa kembali file Surat Keterangan Anda !!');
			            	window.location.href='laporan_pilihkategori';
			            </script>";
		        }
		        else{
		        	$data['cbl'] = $this->m_user->getCekBanyakLapor($id_matprak,$id_user);
		        	if($data['cbl']==null){
		        		$input = array(
							'id_laporan'		=>$id_laporan,
							'id_matprak'		=>$id_matprak,
							'id_user'			=>$id_user,
							'npm_baru'			=>'',
							'kelas_baru'		=>'',
							'pertemuan'			=>$pertemuan,
							'tgl_lapor'			=>$date,
							'tgl_terkonfirmasi'	=>'',
							'no_hp'				=>$no_hp,
							'alasan'			=>$alasan,
							'ktp'				=>'',
							'surket'			=>$filesurket,
							'id_tugas'			=>$id_tugas,
							'tugas'				=>'',
							'pj'				=>'',
							'admin_verifikasi'	=>'',
							'admin_blanko'		=>'',
							'tipe_laporan'		=>$tipelaporan,
							'status_laporan'	=>'1',
							'banyak_lapor'		=>'1',
							'catatan'			=>'Laporan Terkirim !! Silahkan Upload Tugas !!',
							'nilai'				=>'',
						);	
						$this->m_user->InputLaporanPraktikan($input);
						echo 
							"<script>
								alert('Laporan Terkirim !! Silahkan Lihat Instruksi Selanjutnya !!');
								window.location.href='index';
							</script>";	
		        	}
		        	else{
		        		$a 		= $data['cbl'][0];
						$b 		= $a['banyak_lapor'];
						$c 		= $b + 1;
						$input 	= array(
							'id_laporan'		=>$id_laporan,
							'id_matprak'		=>$id_matprak,
							'id_user'			=>$id_user,
							'npm_baru'			=>'',
							'kelas_baru'		=>'',
							'pertemuan'			=>$pertemuan,
							'tgl_lapor'			=>$date,
							'tgl_terkonfirmasi'	=>'',
							'no_hp'				=>$no_hp,
							'alasan'			=>$alasan,
							'ktp'				=>'',
							'surket'			=>$filesurket,
							'id_tugas'			=>$id_tugas,
							'tugas'				=>'',
							'pj'				=>'',
							'admin_verifikasi'	=>'',
							'admin_blanko'		=>'',
							'tipe_laporan'		=>$tipelaporan,
							'status_laporan'	=>'1',
							'banyak_lapor'		=>$c,
							'catatan'			=>'Laporan Terkirim !! Silahkan Upload Tugas !!',
							'nilai'				=>'',
						);	
						$this->m_user->InputLaporanPraktikan($input);
						echo 
							"<script>
								alert('Laporan Terkirim !! Silahkan Lihat Instruksi Selanjutnya !!');
								window.location.href='index';
							</script>";
		        	}
		        }

			}
			else{
				$filektp 	= 'ktp-'.$id_laporan.'.jpg';
				$filesurket = 'surket-'.$id_laporan.'.jpg';

				$this->load->library('upload');
				$config['upload_path']  = 'pelayanan/ktp';
		        $config['allowed_types']= 'jpg';
		        $config['max_size']     = '5000';
				$config['file_name'] 	=  $filektp;
				$this->upload->initialize($config);

				if ( ! $this->upload->do_upload('ktp1')){
		            echo 
			            "<script>
			            	alert('File Gagal Terupload !! Periksa kembali file KTP Anda !!');
			            	window.location.href='laporan_pilihkategori';
			            </script>";
		        }
		        else{
		            /*UPLOAD KTP*/

		            unset($config);
		            $config['upload_path']  = 'pelayanan/surket';
			        $config['allowed_types']= 'jpg';
			        $config['max_size']     = '5000';
					$config['file_name'] 	=  $filesurket;
					$this->upload->initialize($config);

					if ( ! $this->upload->do_upload('surket1')){
						unlink('pelayanan/ktp/'.$filektp);
			            echo 
			            	"<script>
			            		alert('File Gagal Terupload !! Periksa kembali file Surat Keterangan Anda !!');
			            		window.location.href='laporan_pilihkategori';
			            	</script>";
		        	}
		        	else{
		        		/*UPLOAD Surket*/
		        		$data['cbl'] = $this->m_user->getCekBanyakLapor($id_matprak,$id_user);
		        		if($data['cbl']==null){
		        			$input = array(
								'id_laporan'		=>$id_laporan,
								'id_matprak'		=>$id_matprak,
								'id_user'			=>$id_user,
								'npm_baru'			=>'',
								'kelas_baru'		=>'',
								'pertemuan'			=>$pertemuan,
								'tgl_lapor'			=>$date,
								'tgl_terkonfirmasi'	=>'',
								'no_hp'				=>$no_hp,
								'alasan'			=>$alasan,
								'ktp'				=>$filektp,
								'surket'			=>$filesurket,
								'id_tugas'			=>$id_tugas,
								'tugas'				=>'',
								'pj'				=>'',
								'admin_verifikasi'	=>'',
								'admin_blanko'		=>'',
								'tipe_laporan'		=>$tipelaporan,
								'status_laporan'	=>'1',
								'banyak_lapor'		=>'1',
								'catatan'			=>'Laporan Terkirim !! Silahkan Upload Tugas !!',
								'nilai'				=>'',
							);	
							$this->m_user->InputLaporanPraktikan($input);
							echo 
								"<script>
									alert('Laporan Terkirim !! Silahkan Lihat Instruksi Selanjutnya !!');
									window.location.href='index';
								</script>";	
		        		}
		        		else{
		        			$a 		= $data['cbl'][0];
							$b 		= $a['banyak_lapor'];
							$c 		= $b + 1;
							$input 	= array(
								'id_laporan'		=>$id_laporan,
								'id_matprak'		=>$id_matprak,
								'id_user'			=>$id_user,
								'npm_baru'			=>'',
								'kelas_baru'		=>'',
								'pertemuan'			=>$pertemuan,
								'tgl_lapor'			=>$date,
								'tgl_terkonfirmasi'	=>'',
								'no_hp'				=>$no_hp,
								'alasan'			=>$alasan,
								'ktp'				=>$filektp,
								'surket'			=>$filesurket,
								'id_tugas'			=>$id_tugas,
								'tugas'				=>'',
								'pj'				=>'',
								'admin_verifikasi'	=>'',
								'admin_blanko'		=>'',
								'tipe_laporan'		=>$tipelaporan,
								'status_laporan'	=>'1',
								'banyak_lapor'		=>$c,
								'catatan'			=>'Laporan Terkirim !! Silahkan Upload Tugas !!',
								'nilai'				=>'',
							);	
							$this->m_user->InputLaporanPraktikan($input);
							echo 
								"<script>
									alert('Laporan Terkirim !! Silahkan Lihat Instruksi Selanjutnya !!');
									window.location.href='index';
								</script>";
		        		}
		        	}
				}
			}
		}
		else{
			echo 
				"<script>
					alert('Laporan Gagal Terkirim !! Tugas dari PJ Belum Tersedia !!');
					window.location.href='index';
				</script>";
		}
	}
	/*INPUT PELAYANAN*/

	/*PROSES RIWAYAT*/	
	public function laporan_inputtugas($idlaporan)
	{
		if($idlaporan!=null){
			$iduser 		= $this->session->userdata('iduser');
			$data['user'] 	= $this->m_user->getUser($iduser);

			$data['record'] = $this->m_user->getLaporan($idlaporan);
			$this->load->view('user/header',$data);
			$this->load->view('user/laporan_inputtugas');
			$this->load->view('user/footer');
		}
		else{
			redirect('user');
		}
		
	}
	public function InputTugasPraktikan(){
		$id_laporan	= $this->input->post('id_laporan');
		$tugas		= $this->input->post('tugas');

		$filetugas 	= 'tugas-'.$id_laporan.'.pdf';

		$this->load->library('upload');
		$config['upload_path']  = 'pelayanan/tugas';
		$config['allowed_types']= 'pdf';
		$config['max_size']     = '10000';
		$config['file_name'] 	=  $filetugas;
		$this->upload->initialize($config);
		
		if ( ! $this->upload->do_upload('tugas1')){
			echo 
				"<script>
		            alert('File Tugas Gagal Terupload !! Periksa kembali file Tugas Anda !!');
		            history.go(-1);
	            	window.location.reload();
				</script>";
		}
		else{
			$input 	= array(
				'id_laporan'		=>$id_laporan,
				'tugas'				=>$filetugas,
				'status_laporan'	=>'2',
				'catatan'			=>'Tugas Terkirim !! Menunggu Konfirmasi Pelayanan !!',
			);	
			$this->m_user->InputTugasPraktikan($input);
			echo 
				"<script>
					alert('Tugas Terkirim !! Laporan Akan di Proses !! Silahkan Tunggu Konfirmasi Selanjutnya !!');
					window.location.href='index';
				</script>";
		}
	}
	public function laporan_cetaklaporan($idlaporan)
	{
		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_user->getUser($iduser);
		$data['record'] = $this->m_user->getCetakLaporan($idlaporan,$iduser);
		$this->load->view('user/laporan_cetaklaporan',$data);
	}
	public function delete_laporan($idlaporan)
	{
		$data['record'] = $this->m_user->CekDeleteLaporan($idlaporan);
		$a 				= $data['record'][0];
		$tipe 			= $a['tipe_laporan'];
		if($tipe==1){
			$ktp 		= $a['ktp'];
			$surket 	= $a['surket'];
			$surketlain = $a['surketlain'];
			if($surketlain==null){
				if(($ktp==null) AND ($surket!=null)){
					$filesurket = 'surket-'.$idlaporan.'.jpg';
					unlink('pelayanan/surket/'.$filesurket);
				}
				elseif(($ktp!=null) AND ($surket!=null)){
					$filektp 	= 'ktp-'.$idlaporan.'.jpg';
					$filesurket = 'surket-'.$idlaporan.'.jpg';

					unlink('pelayanan/ktp/'.$filektp);
					unlink('pelayanan/surket/'.$filesurket);

				}
			}
			else{
				if(($ktp==null) AND ($surket!=null)){
					$filesurket = 'surket-'.$idlaporan.'.jpg';
					unlink('pelayanan/surket/'.$filesurket);
				}
				elseif(($ktp!=null) AND ($surket!=null)){
					$filektp 		= 'ktp-'.$idlaporan.'.jpg';
					$filesurket 	= 'surket-'.$idlaporan.'.jpg';
					$filesurketlain = 'surketlain-'.$idlaporan.'.jpg';

					unlink('pelayanan/ktp/'.$filektp);
					unlink('pelayanan/surket/'.$filesurket);
					unlink('pelayanan/surketlain/'.$filesurketlain);

				}
			}
			
			$this->m_user->DeleteLaporan($idlaporan);
			echo 
				"<script>
					alert('Laporan Ketidakhadiran Terhapus!');
					window.location.href='../index';
				</script>";  
		}
		else{
			$this->m_user->DeleteLaporan($idlaporan);
			echo 
				"<script>
					alert('Laporan Kartu Hilang Terhapus!');
					window.location.href='../index';
				</script>";
		}
	}
	/*PROSES RIWAYAT*/

	/*INPUT KARTU HILANG*/
	public function input_kartuhilang(){
		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_user->getUser($iduser);

		$this->load->view('user/header',$data);
		$this->load->view('user/input_kartuhilang');
		$this->load->view('user/footer');
	}
	public function inputlaporan_kartuhilang()
	{	
		date_default_timezone_set("Asia/Bangkok");
		$id_user	= $this->input->post('iduser');
		$date 		= date("m-d-Y_H:i:s");
		$no_hp 		= $this->input->post('no_hp');
		$a 			= date("mdYHis");
		$tipelaporan= '2';
		$id_laporan 	= $tipelaporan.'-'.$a.'-'.$id_user;
		$input 	= array(
			'id_laporan'		=>$id_laporan,
			'id_matprak'		=>'KH',
			'id_user'			=>$id_user,
			'npm_baru'			=>'',
			'kelas_baru'		=>'',
			'pertemuan'			=>'',
			'tgl_lapor'			=>$date,
			'tgl_terkonfirmasi'	=>'',
			'no_hp'				=>$no_hp,
			'alasan'			=>'',
			'ktp'				=>'',
			'surket'			=>'',
			'id_tugas'			=>'KH',
			'tugas'				=>'',
			'pj'				=>'',
			'admin_verifikasi'	=>'',
			'admin_blanko'		=>'',
			'tipe_laporan'		=>$tipelaporan,
			'status_laporan'	=>'1',
			'banyak_lapor'		=>'',
			'catatan'			=>'Laporan Kartu Hilang Terkirim !! Menunggu Konfirmasi !!',
			'nilai'				=>'',
		);
		$this->m_user->InputLaporanPraktikan($input);
		echo 
		"<script>
			alert('Laporan Terkirim !! Silahkan Menuju ke Ruangan Pelayanan di Gedung E535 dengan membawa Surat Pernyataan Kehilangan Kartu untuk Mengkonfirmasi Laporan !!');
			window.location.href='index';
		</script>";
	}
	/*INPUT KARTU HILANG*/

	public function about()
	{
		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_user->getUser($iduser);

		$this->load->view('user/header',$data);
		$this->load->view('user/about');
		$this->load->view('user/footer');
	}
	public function contact()
	{
		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_user->getUser($iduser);
		
		$this->load->view('user/header',$data);
		$this->load->view('user/contact');
		$this->load->view('user/footer');
	}
	public function logout(){
      	$this->session->sess_destroy();
      	echo 
			"<script>
				alert('Terima Kasih !! Selamat Datang Kembali !!');
				window.location.href='../welcome';
			</script>";
  	}
}

?>