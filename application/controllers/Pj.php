<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pj extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_pj');
		if($this->session->userdata('logged_in')){
			if($this->session->userdata('role')==1){
				redirect('user');
			}
			elseif($this->session->userdata('role')==2){
				redirect('admin');
			}
		}
		else{
			redirect('welcome');
		}
	}

	/*Dashboard*/
	public function index()
	{
		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_pj->getPj($iduser);

		$namapj 		= $this->session->userdata('namapj');
		$data['tugas'] 	= $this->m_pj->getDataTugas($namapj);

		$this->load->view('pj/header',$data);
		$this->load->view('pj/index');
		$this->load->view('pj/footer');
	}
	/*Dashboard*/

	/*CRUD NILAI TUGAS*/
	public function tugas_datatugas(){
		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_pj->getPj($iduser);

		$namapj 		= $this->session->userdata('namapj');
		$data['tugas'] 	= $this->m_pj->getDataTugas($namapj);

		$data['record'] = $this->m_pj->getDataTugas($namapj);

		$this->load->view('pj/header',$data);
		$this->load->view('pj/tugas_datatugas');
		$this->load->view('pj/footer');	
	}
	public function tugas($idtugas)
	{
		header("Content-type: application/pdf");
  		readfile("pelayanan/tugas/$idtugas");
  		exit(0);
	}
	public function tugas_nilaitugas($idlaporan){
		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_pj->getPj($iduser);
		$data['record'] = $this->m_pj->getPenilaianDataTugas($idlaporan);

		$namapj 		= $this->session->userdata('namapj');
		$data['tugas'] 	= $this->m_pj->getDataTugas($namapj);

		$this->load->view('pj/header',$data);
		$this->load->view('pj/tugas_nilaitugas');
		$this->load->view('pj/footer');	
	}
	public function NilaiTugasPraktikan(){
		$nilai 		= $this->input->post('nilai');
		if(($nilai<1) OR ($nilai>100)){
			echo 
				"<script>
					alert('Range Nilai yang Anda Berikan Salah !!');
					history.go(-1);
				</script>";

		}
		else{
			$catatan	= 'Tugas Sudah Dinilai, dengan Nilai '.$nilai;
			$input = array(
		        	'id_laporan'=> $this->input->post('id_laporan'),
		        	'nilai'		=> $nilai,
		        	'status'	=> '6',
		        	'catatan'	=> $catatan,
			);
			$this->m_pj->NilaiTugasPraktikan($input);
			echo 
				"<script>
					alert('Tugas Berhasil Dinilai !!');
					window.location.href='tugas_datatugas';
				</script>";
		}
	}
	/*CRUD NILAI TUGAS*/

	public function about()
	{				
		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_pj->getPj($iduser);

		$namapj 		= $this->session->userdata('namapj');
		$data['tugas'] 	= $this->m_pj->getDataTugas($namapj);
		
		$this->load->view('pj/header',$data);
		$this->load->view('pj/about');
		$this->load->view('pj/footer');
	}
	public function logout(){
      	$this->session->sess_destroy();
      	echo 
			"<script>
				alert('Terima Kasih !! Selamat Datang Kembali !!');
				window.location.href='../welcome';
			</script>";
  	}

}

?>