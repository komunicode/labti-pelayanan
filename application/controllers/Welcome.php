<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('m_login');
		if(!$this->session->userdata('logged_in')){
		}
		else{
			redirect('user');
		}
	}


	public function index()
	{
		$this->load->view('header');
		$this->load->view('index');
		$this->load->view('footer');
	}
	public function daftar_masukkannpm()
	{
		$this->load->view('header');
		$this->load->view('daftar_masukkannpm');
		$this->load->view('footer');
	}
	public function daftar_buatakun(){
		$npm			= $this->input->post('npm');
		$data['record']	= $this->m_login->getUserBaru($npm);
		if($data['record']==null){
			echo 
				"<script>
	            	alert('NPM Tidak Ditemukan');
	            	history.go(-1);
	            	window.location.reload();
	            </script>";
		}
		else{
			$this->load->view('header',$data);
			$this->load->view('daftar_buatakun');
			$this->load->view('footer');
		}		
	}
	public function daftar_buatakunbaru(){
		$npm			= $this->input->post('npm');
		$nama			= $this->input->post('nama');
		$kelas			= $this->input->post('kelas');
		$angkatan		= $this->input->post('angkatan');
		$username		= $this->input->post('username');
		$password		= $this->input->post('password');
		$id_user		= 'Prak-'.$npm.'-LabTI';

		/*cek kesamaan id user*/
		$data['iduser']	= $this->m_login->getCekIdUser($id_user);
		if($data['iduser']!=null){
			echo 
				"<script>
	            	alert('Buat Akun Gagal !! Anda Sudah Memiliki Akun !!');
	            	window.location.href='index';
	            </script>";
		}
		else{
			/*cek kesamaan username*/
			$data['us']	= $this->m_login->getCekUsername($username);
			if($data['us']!=null){
				echo 
					"<script>
		            	alert('Buat Akun Gagal !! Username Sudah Tersedia !! Masukkan Username Lainnya !!!');
		            	window.location.href='daftar_masukkannpm';		            	
		            </script>";
			}
			else{
				$input = array(
					'id_user'	=>$id_user,
					'npm'		=>$npm,
					'username'	=>$username,
					'password'	=>$password,
					'nama'		=>$nama,
					'angkatan'	=>$angkatan,
					'kelas'		=>$kelas,
					'level'		=>'1',
				);	
				$this->m_login->InputUserBaru($input);
				echo 
					"<script>
		            	alert('Pembuatan Akun Berhasil !!');
		            	window.location.href='index';
		            </script>";
			}
		}
	}
	public function about()
	{
		$this->load->view('header');
		$this->load->view('about');
		$this->load->view('footer');
	}
	public function contact()
	{
		$this->load->view('header');
		$this->load->view('contact');
		$this->load->view('footer');
	}

	//fungsi untuk pengecekan data
	function cek_login() {
		$username 	= $this->input->post('username', true);
		$password 	= $this->input->post('password', true);
		$a = $this->m_login->cek_login($username, $password)->row();
		if($a!=null){
			$level = $a->level;
			if ($level == "1") {
				$sess_array = array();
				$sess_array = array('iduser' => $a->id_user, 'logged_in' => true, 'role' => 1);
				$this->session->set_userdata($sess_array);
				$status = array('status' => true);
				redirect('user');
			}
			elseif ($level == "2") {
				$sess_array = array();
				$sess_array = array('namaadmin' => $a->username, 'iduser' => $a->id_user, 'logged_in' => true, 'role' => 2);
				$this->session->set_userdata($sess_array);
				$status = array('status' => true);
				redirect('admin');
			}
			elseif ($level == "3") {
				$sess_array = array();
				$sess_array = array('namapj' => $a->username, 'iduser' => $a->id_user, 'logged_in' => true, 'role' => 3);
				$this->session->set_userdata($sess_array);
				$status = array('status' => true);
				redirect('pj');
			}
			else {
				echo 
					"<script>alert('Gagal login: Cek username, password!');
					window.location.href='index';						
					</script>";
			}
		}
		else{
			echo 
				"<script>alert('Gagal login: Cek username, password!');						
				window.location.href='index';
				</script>";	
		}
		
	}
}
