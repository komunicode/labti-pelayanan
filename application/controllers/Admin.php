<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_admin');
		if($this->session->userdata('logged_in')){
			if($this->session->userdata('role')==1){
				redirect('user');
			}
			elseif($this->session->userdata('role')==3){
				redirect('pj');
			}

		}
		else{
			redirect('welcome');
		}
	}

	/*Dashboard*/
	public function index()
	{
		$iduser 					= $this->session->userdata('iduser');
		$data['user'] 				= $this->m_admin->getAdmin($iduser);

		$data['praktikan'] 			= $this->m_admin->getPraktikan();
		$data['at'] 				= $this->m_admin->getAT();
		$data['pj'] 				= $this->m_admin->getPJ();
		$data['mhs'] 				= $this->m_admin->getDataMahasiswa();

		$data['lap_masuk'] 			= $this->m_admin->getLaporanMasuk();
		$data['lap_terkonfirmasi'] 	= $this->m_admin->getLaporanTerkonfirmasi();
		$data['lap_ditolak'] 		= $this->m_admin->getLaporanTidakTerkonfirmasi();
		$data['lap_kartuhilang']	= $this->m_admin->getLaporanKartuHilang();
		$data['lap_praktrans']		= $this->m_admin->getLaporanPrakTrans();

		$data['mp']					= $this->m_admin->getMataPraktikum();
		$data['tugas']				= $this->m_admin->getTugasPraktikan();

		$this->load->view('admin/header1',$data);
		$this->load->view('admin/index');
		$this->load->view('admin/footer');
	}
	/*Dashboard*/

	/*CRUD USER*/
	public function user($level)
	{
		if($level!=null){
			$data['praktikan'] 			= $this->m_admin->getPraktikan();
			$data['at'] 				= $this->m_admin->getAT();
			$data['pj'] 				= $this->m_admin->getPJ();

			$data['lap_masuk'] 			= $this->m_admin->getLaporanMasuk();
			$data['lap_terkonfirmasi'] 	= $this->m_admin->getLaporanTerkonfirmasi();
			$data['lap_ditolak'] 		= $this->m_admin->getLaporanTidakTerkonfirmasi();
			$data['lap_kartuhilang']	= $this->m_admin->getLaporanKartuHilang();
			$data['lap_praktrans']		= $this->m_admin->getLaporanPrakTrans();

			$iduser 			= $this->session->userdata('iduser');
			$data['user'] 		= $this->m_admin->getAdmin($iduser);
			$data['record'] 	= $this->m_admin->getUser($level);
			$data['level'] 		= $level;
			$this->load->view('admin/header',$data);
			$this->load->view('admin/user');
			$this->load->view('admin/footer');
		}
		else{
			redirect('admin/index');
		}
		
	}	
	public function user_tambahuser()
	{
		$data['praktikan'] 	= $this->m_admin->getPraktikan();
		$data['at'] 		= $this->m_admin->getAT();
		$data['pj'] 		= $this->m_admin->getPJ();

		$data['lap_masuk'] 			= $this->m_admin->getLaporanMasuk();
		$data['lap_terkonfirmasi'] 	= $this->m_admin->getLaporanTerkonfirmasi();
		$data['lap_ditolak'] 		= $this->m_admin->getLaporanTidakTerkonfirmasi();
		$data['lap_kartuhilang']	= $this->m_admin->getLaporanKartuHilang();
		$data['lap_praktrans']		= $this->m_admin->getLaporanPrakTrans();

		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_admin->getAdmin($iduser);
		$this->load->view('admin/header',$data);
		$this->load->view('admin/user_tambahuser');
		$this->load->view('admin/footer');
	}
	public function UserTambahUser()
	{
		$npm 		= $this->input->post('npm');
		$username 	= $this->input->post('username');
		$password 	= $this->input->post('password');
		$level 		= $this->input->post('level');

		/*CEK KETERSEDIAAN DATA MAHASISWA*/
		$data['cekdatamhs'] = $this->m_admin->CekDataUser($npm);
		if($data['cekdatamhs']!=null){
			$a 			= $data['cekdatamhs'][0];
			$nama 		= $a['nama'];
			$angkatan 	= $a['angkatan'];
			$kelas 		= $a['kelas'];

			if($level==1){
				$id_user	= 'Prak-'.$npm.'-Labti';
				$level		= '1';
				$akses 		= '';
			}
			elseif($level==2){
				$id_user	= 'ATLabTI-'.$npm;
				$level		= '2';
				$akses 		= '';
			}			
			elseif($level==3){
				$id_user	= 'PJLabTI-'.$npm;
				$level		= '3';
				$akses 		= '1';
			}
			$input = array(
		        	'id_user'	=>$id_user,
		        	'npm'		=>$npm,
		        	'username'	=>$username,
		        	'password'	=>$password,
		        	'nama'		=>$nama,
		        	'angkatan'	=>$angkatan,
		        	'kelas'		=>$kelas,
		        	'level'		=>$level,
		        	'akses'		=>$akses,
			);	

			/*cek id user*/
			$data['cekiduser'] = $this->m_admin->CekIdUser($id_user);
			if($data['cekiduser']==null){
				$data['CekUsername'] = $this->m_admin->CekUsername($username);
				if($data['CekUsername']==null){
					$this->m_admin->UserTambahUser($input);
					if($level==1){
						echo 
						"<script>
							alert('Praktikan Berhasil Ditambahkan !!');
							window.location.href='../admin/user/1';
						</script>";
					}
					elseif($level==2){
						echo 
						"<script>
							alert('Asisten Tetap Berhasil Ditambahkan !!');
							window.location.href='../admin/user/2';
						</script>";
					}			
					elseif($level==3){
						echo 
						"<script>
							alert('PJ Berhasil Ditambahkan !!');
							window.location.href='../admin/user/3';
						</script>";
					}
				}
				else{
					echo 
					"<script>
						alert('Username Sudah Tersedia !! Masukkan Username Lainnya !!');
						history.go(-1);
					</script>";
				}
			}
			else{
				echo 
				"<script>
					alert('Data User Sudah Tersedia !!');
					history.go(-1);
				</script>";
			}
		}
		else{
			echo 
			"<script>
				alert('Data Mahasiswa Tidak Tersedia !!');
				history.go(-1);
			</script>";
		}
	}
	public function user_hapususer($iduser)
	{
		if($iduser!=null){
			$get = substr($iduser,0,2);
			$this->m_admin->HapusUser($iduser);
			if($get=='Pr'){
				echo 
					"<script>
						alert('Data Praktikan Berhasil Dihapus !!');
						window.location.href='../user/1';
					</script>";
			}
			elseif($get=='AT'){
				echo 
					"<script>
						alert('Data Asisten Tetap Berhasil Dihapus !!');
						window.location.href='../user/2';
					</script>";	
			}
			elseif($get=='PJ'){
				echo 
					"<script>
						alert('Data PJ Berhasil Dihapus !!');
						window.location.href='../user/3';
					</script>";
			}
		}
		else{
			redirect('admin/welcome');
		}
	}
	public function user_ubahuser($id_user)
	{
		if($id_user!=null){
			$data['praktikan'] 	= $this->m_admin->getPraktikan();
			$data['at'] 		= $this->m_admin->getAT();
			$data['pj'] 		= $this->m_admin->getPJ();

			$data['lap_masuk'] 			= $this->m_admin->getLaporanMasuk();
			$data['lap_terkonfirmasi'] 	= $this->m_admin->getLaporanTerkonfirmasi();
			$data['lap_ditolak'] 		= $this->m_admin->getLaporanTidakTerkonfirmasi();
			$data['lap_kartuhilang']	= $this->m_admin->getLaporanKartuHilang();
			$data['lap_praktrans']		= $this->m_admin->getLaporanPrakTrans();

			$iduser 		= $this->session->userdata('iduser');
			$data['user'] 	= $this->m_admin->getAdmin($iduser);
			$data['record'] = $this->m_admin->CekIdUser($id_user);
			$this->load->view('admin/header',$data);
			$this->load->view('admin/user_ubahuser');
			$this->load->view('admin/footer');
		}
		else{
			redirect('admin/index');
		}
		
	}	
	public function UserUpdateUser()
	{
		$iduser = $this->input->post('id_user');
		$input = array(
	        	'id_user'	=>$iduser,
	        	'password'	=>$this->input->post('password'),
	        	'nama'		=>$this->input->post('nama'),
	        	'kelas'		=>$this->input->post('kelas'),
	        	'angkatan'	=>$this->input->post('angkatan'),
		);	
		$this->m_admin->UpdateUser($input);
        $get = substr($iduser,0,2);
		if($get=='Pr'){
			echo 
				"<script>
					alert('Data Praktikan Berhasil Diubah !!');
					window.location.href='../admin/user/1';
				</script>";
		}
		elseif($get=='AT'){
			echo 
				"<script>
					alert('Data Asisten Tetap Berhasil Diubah !!');
					window.location.href='../admin/user/2';
				</script>";	
		}
		elseif($get=='PJ'){
			echo 
				"<script>
					alert('Data PJ Berhasil Diubah !!');
					window.location.href='../admin/user/3';
				</script>";
		}
	}
	public function akses_aktif($iduser)
	{
		$this->m_admin->akses_aktif($iduser);
        echo 
			"<script>
				alert('Akses Scanner Aktif !!');
				window.location.href='../user/3';
			</script>";
	}
	public function akses_nonaktif($iduser)
	{
		$this->m_admin->akses_nonaktif($iduser);
        echo 
			"<script>
				alert('Akses Scanner Non-Aktif !!');
				window.location.href='../user/3';
			</script>";
	}
	public function akses_nonaktifall()
	{
		$this->m_admin->akses_nonaktifall();
        echo 
			"<script>
				alert('Semua Akses Scanner Non-Aktif !!');
				window.location.href='../admin/user/3';
			</script>";
	}
	/*CRUD USER*/

	/*CRUD LAPORAN*/
	public function laporan_masuk()
	{
		$data['praktikan'] 	= $this->m_admin->getPraktikan();
		$data['at'] 		= $this->m_admin->getAT();
		$data['pj'] 		= $this->m_admin->getPJ();

		$data['lap_masuk'] 			= $this->m_admin->getLaporanMasuk();
		$data['lap_terkonfirmasi'] 	= $this->m_admin->getLaporanTerkonfirmasi();
		$data['lap_ditolak'] 		= $this->m_admin->getLaporanTidakTerkonfirmasi();
		$data['lap_kartuhilang']	= $this->m_admin->getLaporanKartuHilang();
		$data['lap_praktrans']		= $this->m_admin->getLaporanPrakTrans();

		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_admin->getAdmin($iduser);
		$data['record'] = $this->m_admin->getLaporanMasuk();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/laporan_masuk');
		$this->load->view('admin/footer');
	}
	public function ktp($idktp)
	{
		header("Content-type: image/jpeg");
  		readfile("pelayanan/ktp/$idktp");
  		exit(0);
	}
	public function surket($idsurket)
	{
		header("Content-type: image/jpeg");
  		readfile("pelayanan/surket/$idsurket");
  		exit(0);
	}
	public function tugas($idtugas)
	{
		header("Content-type: application/pdf");
  		readfile("pelayanan/tugas/$idtugas");
  		exit(0);
	}
	public function konfirmasi_laporanmasuk($idlaporan)
	{
		$data['praktikan'] 	= $this->m_admin->getPraktikan();
		$data['at'] 		= $this->m_admin->getAT();
		$data['pj'] 		= $this->m_admin->getPJ();

		$data['lap_masuk'] 			= $this->m_admin->getLaporanMasuk();
		$data['lap_terkonfirmasi'] 	= $this->m_admin->getLaporanTerkonfirmasi();
		$data['lap_ditolak'] 		= $this->m_admin->getLaporanTidakTerkonfirmasi();
		$data['lap_kartuhilang']	= $this->m_admin->getLaporanKartuHilang();
		$data['lap_praktrans']		= $this->m_admin->getLaporanPrakTrans();

		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_admin->getAdmin($iduser);
		$data['record'] = $this->m_admin->getKonfirmasiLaporanMasuk($idlaporan);
		$this->load->view('admin/header',$data);
		$this->load->view('admin/konfirmasi_laporanmasuk');
		$this->load->view('admin/footer');
	}
	public function KonfirmasiLaporanMasuk()
	{
		date_default_timezone_set("Asia/Bangkok");
		$date 		= date("m-d-Y_H:i:s");
		$namaadmin 	= $this->session->userdata('namaadmin');
		$idlaporan	= $this->input->post('id_laporan');
		$catatan	= $this->input->post('catatan');
		$ceksubmit 	= $this->input->post('submit');
		if( $ceksubmit == 'TERIMA LAPORAN' ){
			$input 	= array(
				'id_laporan'		=>$idlaporan,
				'tgl_terkonfirmasi'	=>$date,
				'admin_verifikasi'	=>$namaadmin,
				'status_laporan'	=>'3',
				'catatan'			=>$catatan,
			);	
			$this->m_admin->TerimaLaporanMasuk($input);
			echo 
				"<script>
					alert('Laporan Terkonfirmasi !!');
					window.location.href='laporan_masuk';
				</script>";    		
		}
		else{
			$input 	= array(
				'id_laporan'		=>$idlaporan,
				'id_matprak'		=>$this->input->post('id_matprak'),
				'id_user'			=>$this->input->post('id_user'),
				'pertemuan'			=>$this->input->post('pertemuan'),
				'tgl_lapor'			=>$this->input->post('tgl_lapor'),
				'tgl_terkonfirmasi'	=>$date,
				'no_hp'				=>$this->input->post('no_hp'),
				'alasan'			=>$this->input->post('alasan'),
				'ktp'				=>$this->input->post('ktp'),
				'surket'			=>$this->input->post('surket'),
				'id_tugas'			=>$this->input->post('id_tugas'),
				'tugas'				=>$this->input->post('tugas'),
				'pj'				=>'',
				'admin_verifikasi'	=>$namaadmin,
				'tipe_laporan'		=>$this->input->post('tipe_laporan'),
				'status_laporan'	=>'7',
				'catatan'			=>$catatan,
			);	
			$this->m_admin->TolakLaporanMasuk($input);
			echo 
				"<script>
					alert('Laporan Tidak Terkonfirmasi !!');
					window.location.href='laporan_masuk';
				</script>";
		}
	}
	public function laporan_terkonfirmasi()
	{
		$data['praktikan'] 	= $this->m_admin->getPraktikan();
		$data['at'] 		= $this->m_admin->getAT();
		$data['pj'] 		= $this->m_admin->getPJ();

		$data['lap_masuk'] 			= $this->m_admin->getLaporanMasuk();
		$data['lap_terkonfirmasi'] 	= $this->m_admin->getLaporanTerkonfirmasi();
		$data['lap_ditolak'] 		= $this->m_admin->getLaporanTidakTerkonfirmasi();
		$data['lap_kartuhilang']	= $this->m_admin->getLaporanKartuHilang();
		$data['lap_praktrans']		= $this->m_admin->getLaporanPrakTrans();

		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_admin->getAdmin($iduser);
		$data['record'] = $this->m_admin->getLaporanTerkonfirmasi();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/laporan_terkonfirmasi');
		$this->load->view('admin/footer');			
	}
	public function laporan_tidakterkonfirmasi()
	{
		$data['praktikan'] 	= $this->m_admin->getPraktikan();
		$data['at'] 		= $this->m_admin->getAT();
		$data['pj'] 		= $this->m_admin->getPJ();

		$data['lap_masuk'] 			= $this->m_admin->getLaporanMasuk();
		$data['lap_terkonfirmasi'] 	= $this->m_admin->getLaporanTerkonfirmasi();
		$data['lap_ditolak'] 		= $this->m_admin->getLaporanTidakTerkonfirmasi();
		$data['lap_kartuhilang']	= $this->m_admin->getLaporanKartuHilang();
		$data['lap_praktrans']		= $this->m_admin->getLaporanPrakTrans();

		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_admin->getAdmin($iduser);
		$data['record'] = $this->m_admin->getLaporanTidakTerkonfirmasi();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/laporan_tidakterkonfirmasi');
		$this->load->view('admin/footer');
	}
	public function KonfirmasiPembayaranBlanko($idlaporan){	
		$namaadmin 	= $this->session->userdata('namaadmin');
		$input 	= array(
			'id_laporan'		=>$idlaporan,
			'admin_blanko'		=>$namaadmin,
			'status_laporan'	=>'5',
			'catatan'			=>'Pembayaran Blanko Diterima, Menunggu Penilaian PJ',
		);	
		$this->m_admin->KonfirmasiPembayaranBlanko($input);
		echo 
			"<script>
				alert('Pembayaran Blanko Diterima !!');
				history.go(-1);
			</script>";    		
	}
	/*CRUD LAPORAN*/

	/*CRUD LAPORAN KHUSUS*/
	public function case_laporankhusus()
	{
		$data['praktikan'] 	= $this->m_admin->getPraktikan();
		$data['at'] 		= $this->m_admin->getAT();
		$data['pj'] 		= $this->m_admin->getPJ();

		$data['lap_masuk'] 			= $this->m_admin->getLaporanMasuk();
		$data['lap_terkonfirmasi'] 	= $this->m_admin->getLaporanTerkonfirmasi();
		$data['lap_ditolak'] 		= $this->m_admin->getLaporanTidakTerkonfirmasi();
		$data['lap_kartuhilang']	= $this->m_admin->getLaporanKartuHilang();
		$data['lap_praktrans']		= $this->m_admin->getLaporanPrakTrans();


		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_admin->getAdmin($iduser);
		
		/*cek akses user*/
		$data['cek'] 		= $this->m_admin->CekAksesUser($iduser);
		if($data['cek']!=null){
			$this->load->view('admin/header',$data);
			$this->load->view('admin/case_laporankhusus');
			$this->load->view('admin/footer');
		}
		else{
			echo 
				"<script>
					alert('Anda Tidak Memiliki Akses ke Halaman Ini !!');
					history.go(-1);
				</script>";  
		}
	}
	public function case_inputlaporankhusus(){
		$npm = $this->input->post('npm');
		/*ambil data praktikan*/
		$data['record'] 	= $this->m_admin->getCaseKhususPraktikan($npm);
		if($data['record']!=null){

			$data['praktikan'] 	= $this->m_admin->getPraktikan();
			$data['at'] 		= $this->m_admin->getAT();
			$data['pj'] 		= $this->m_admin->getPJ();

			$data['lap_masuk'] 			= $this->m_admin->getLaporanMasuk();
			$data['lap_terkonfirmasi'] 	= $this->m_admin->getLaporanTerkonfirmasi();
			$data['lap_ditolak'] 		= $this->m_admin->getLaporanTidakTerkonfirmasi();
			$data['lap_kartuhilang']	= $this->m_admin->getLaporanKartuHilang();
			$data['lap_praktrans']		= $this->m_admin->getLaporanPrakTrans();

			$iduser 					= $this->session->userdata('iduser');
			$data['user'] 				= $this->m_admin->getAdmin($iduser);
			$data['praktikankhusus'] 	= $this->m_admin->getPraktikanKhusus($npm);	
			$data['mpt'] 				= $this->m_admin->getMataPraktikumTransfer();		
			
			$this->load->view('admin/header',$data);
			$this->load->view('admin/case_inputlaporankhusus');
			$this->load->view('admin/footer');
		}
		else{
			echo 
			"<script>
				alert('Data Praktikan Tidak Ditemukan !!');
				history.go(-1);
			</script>";  
		}	
	}
	public function InputLaporanKhusus(){
		$id_matprak	= $this->input->post('id_matprak');
		$id_user	= $this->input->post('id_user');
		$pertemuan	= $this->input->post('pertemuan');
		$alasan		= 'CASE KHUSUS';
		$no_hp		= $this->input->post('no_hp');
		$kelas		= $this->input->post('kelas');
		$ktp		= $this->input->post('ktp');
		$surket		= $this->input->post('surket');
		$surketlain	= $this->input->post('surketlain');
		$id_tugas	= $kelas."-".$id_matprak.".".$pertemuan;
		$catatan	= $this->input->post('catatan');
		$tipelaporan= '1';

		date_default_timezone_set("Asia/Bangkok");
		$date 		= date("m-d-Y_H:i:s");
		$a 			= date("mdYHis");

		$id_laporan = $tipelaporan.'-'.$a.'-'.$id_user.'-'.$id_matprak.'-'.$pertemuan;

		/*cek ketersediaan tugas*/
		$cek['cektugas'] = $this->m_admin->getCekTugas($id_tugas);
		if($cek['cektugas']!=null){
			/*cek kesamaan pertemuan*/
			$cek['cek1'] = $this->m_admin->getCekKesamaanPertemuan($id_user,$id_matprak,$pertemuan);
			if($cek['cek1']==null){
				if($surketlain!=null){
					/*kalau surket lain tidak kosong*/
					$filektp 		= 'ktp-'.$id_laporan.'.jpg';
					$filesurket 	= 'surket-'.$id_laporan.'.jpg';
					$filesurketlain = 'surketlain-'.$id_laporan.'.jpg';

					$this->load->library('upload');
					$config['upload_path']  = 'pelayanan/ktp';
			        $config['allowed_types']= 'jpg';
			        $config['max_size']     = '5000';
					$config['file_name'] 	=  $filektp;
					$this->upload->initialize($config);

					if ( ! $this->upload->do_upload('ktp1')){
			            echo 
				            "<script>
				            	alert('File Gagal Terupload !! Periksa kembali file KTP Anda !!');
				            	window.location.href='case_laporankhusus';
				            </script>";
			        }
			        else{
			        	/*upload ktp*/
			        	unset($config);
			            $config['upload_path']  = 'pelayanan/surket';
				        $config['allowed_types']= 'jpg';
				        $config['max_size']     = '5000';
						$config['file_name'] 	=  $filesurket;
						$this->upload->initialize($config);

						if ( ! $this->upload->do_upload('surket1')){
							unlink('pelayanan/ktp/'.$filektp);
				            echo 
				            	"<script>
				            		alert('File Gagal Terupload !! Periksa kembali file Surat Keterangan Anda !!');
				            		window.location.href='case_laporankhusus';
				            	</script>";
			        	}
			        	else{
			        		/*upload surket*/
			        		unset($config);
				            $config['upload_path']  = 'pelayanan/surketlain';
					        $config['allowed_types']= 'jpg';
					        $config['max_size']     = '5000';
							$config['file_name'] 	=  $filesurketlain;
							$this->upload->initialize($config);
							if ( ! $this->upload->do_upload('surketlain1')){
								unlink('pelayanan/ktp/'.$filektp);
								unlink('pelayanan/Surket/'.$filesurket);
					            echo 
					            	"<script>
					            		alert('File Gagal Terupload !! Periksa kembali file Surat Keterangan Lain Anda !!');
					            		window.location.href='case_laporankhusus';
					            	</script>";
				        	}
				        	else{
				        		/*upload surketlain*/
				        		$input = array(
									'id_laporan'		=>$id_laporan,
									'id_matprak'		=>$id_matprak,
									'id_user'			=>$id_user,
									'npm_baru'			=>'',
									'kelas_baru'		=>'',
									'pertemuan'			=>$pertemuan,
									'tgl_lapor'			=>$date,
									'tgl_terkonfirmasi'	=>'',
									'no_hp'				=>$no_hp,
									'alasan'			=>$alasan,
									'ktp'				=>$filektp,
									'surket'			=>$filesurket,
									'surketlain'		=>$filesurketlain,
									'id_tugas'			=>$id_tugas,
									'tugas'				=>'',
									'pj'				=>'',
									'admin_verifikasi'	=>'',
									'admin_blanko'		=>'',
									'tipe_laporan'		=>$tipelaporan,
									'status_laporan'	=>'1',
									'banyak_lapor'		=>'Case Khusus',
									'catatan'			=>'Laporan Terkirim !! Silahkan Upload Tugas !!',
									'nilai'				=>'',
								);	
								$this->m_admin->InputLaporanPraktikanKhusus($input);
								echo 
									"<script>
										alert('Laporan Khusus Terkirim !!');
										window.location.href='../admin/laporan_masuk';
									</script>";	
				        	}

			        	}
			        }
				}
				else{
					/*kalau surket lain kosong*/
					$filektp 		= 'ktp-'.$id_laporan.'.jpg';
					$filesurket 	= 'surket-'.$id_laporan.'.jpg';

					$this->load->library('upload');
					$config['upload_path']  = 'pelayanan/ktp';
			        $config['allowed_types']= 'jpg';
			        $config['max_size']     = '5000';
					$config['file_name'] 	=  $filektp;
					$this->upload->initialize($config);

					if ( ! $this->upload->do_upload('ktp1')){
			            echo 
				            "<script>
				            	alert('File Gagal Terupload !! Periksa kembali file KTP Anda !!');
				            	window.location.href='case_laporankhusus';
				            </script>";
			        }
			        else{
			        	/*upload ktp*/
			        	unset($config);
			            $config['upload_path']  = 'pelayanan/surket';
				        $config['allowed_types']= 'jpg';
				        $config['max_size']     = '5000';
						$config['file_name'] 	=  $filesurket;
						$this->upload->initialize($config);

						if ( ! $this->upload->do_upload('surket1')){
							unlink('pelayanan/ktp/'.$filektp);
				            echo 
				            	"<script>
				            		alert('File Gagal Terupload !! Periksa kembali file Surat Keterangan Anda !!');
				            		window.location.href='case_laporankhusus';
				            	</script>";
			        	}
			        	else{
			        		/*upload surket*/
				        	$input = array(
								'id_laporan'		=>$id_laporan,
								'id_matprak'		=>$id_matprak,
								'id_user'			=>$id_user,
								'npm_baru'			=>'',
								'kelas_baru'		=>'',
								'pertemuan'			=>$pertemuan,
								'tgl_lapor'			=>$date,
								'tgl_terkonfirmasi'	=>'',
								'no_hp'				=>$no_hp,
								'alasan'			=>$alasan,
								'ktp'				=>$filektp,
								'surket'			=>$filesurket,
								'id_tugas'			=>$id_tugas,
								'tugas'				=>'',
								'pj'				=>'',
								'admin_verifikasi'	=>'',
								'admin_blanko'		=>'',
								'tipe_laporan'		=>$tipelaporan,
								'status_laporan'	=>'1',
								'banyak_lapor'		=>'Case Khusus',
								'catatan'			=>'Laporan Terkirim !! Silahkan Upload Tugas !!',
								'nilai'				=>'',
							);	
							$this->m_admin->InputLaporanPraktikanKhusus($input);
							echo 
								"<script>
									alert('Laporan Khusus Terkirim !!');
									window.location.href='../admin/laporan_masuk';
								</script>";	
				        }
			        }
				}
			}
			else{
				echo 
					"<script>
						alert('Laporan Gagal Terkirim !! Anda Sudah Pernah Melapor pada Pertemuan Ini !!');
						window.location.href='case_laporankhusus';
					</script>";
			}			
		}
		else{
			echo 
				"<script>
					alert('Laporan Gagal Terkirim !! Tugas dari PJ Belum Tersedia !!');
					window.location.href='case_laporankhusus';
				</script>";
		}
	}
	/*CRUD LAPORAN KHUSUS*/







	/*CRUD LAPORAN KARTU HILANG*/
	public function laporan_kartuhilang()
	{
		$data['praktikan'] 	= $this->m_admin->getPraktikan();
		$data['at'] 		= $this->m_admin->getAT();
		$data['pj'] 		= $this->m_admin->getPJ();

		$data['lap_masuk'] 			= $this->m_admin->getLaporanMasuk();
		$data['lap_terkonfirmasi'] 	= $this->m_admin->getLaporanTerkonfirmasi();
		$data['lap_ditolak'] 		= $this->m_admin->getLaporanTidakTerkonfirmasi();
		$data['lap_kartuhilang']	= $this->m_admin->getLaporanKartuHilang();
		$data['lap_praktrans']		= $this->m_admin->getLaporanPrakTrans();

		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_admin->getAdmin($iduser);
		$data['record'] = $this->m_admin->getLaporanKartuHilang();		
		$this->load->view('admin/header',$data);
		$this->load->view('admin/laporan_kartuhilang');
		$this->load->view('admin/footer');	
	}
	public function konfirmasi_laporankartuhilang($idlaporan)
	{
		$data['praktikan'] 	= $this->m_admin->getPraktikan();
		$data['at'] 		= $this->m_admin->getAT();
		$data['pj'] 		= $this->m_admin->getPJ();

		$data['lap_masuk'] 			= $this->m_admin->getLaporanMasuk();
		$data['lap_terkonfirmasi'] 	= $this->m_admin->getLaporanTerkonfirmasi();
		$data['lap_ditolak'] 		= $this->m_admin->getLaporanTidakTerkonfirmasi();
		$data['lap_kartuhilang']	= $this->m_admin->getLaporanKartuHilang();
		$data['lap_praktrans']		= $this->m_admin->getLaporanPrakTrans();

		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_admin->getAdmin($iduser);
		$data['record'] = $this->m_admin->getKonfirmasiLaporanKartuHilang($idlaporan);
		$this->load->view('admin/header',$data);
		$this->load->view('admin/konfirmasi_laporankartuhilang');
		$this->load->view('admin/footer');
	}
	public function KonfirmasiLaporanKartuHilang()
	{
		date_default_timezone_set("Asia/Bangkok");
		$date 		= date("m-d-Y_H:i:s");
		$namaadmin 	= $this->session->userdata('namaadmin');
		$idlaporan	= $this->input->post('id_laporan');
		$input 	= array(
			'id_laporan'		=>$idlaporan,
			'tgl_terkonfirmasi'	=>$date,
			'admin_verifikasi'	=>$namaadmin,
			'status_laporan'	=>'6',
			'catatan'			=>'Laporan Kartu Hilang Terkonfirmasi',
		);	
		$this->m_admin->TerimaLaporanKartuHilang($input);
		echo 
			"<script>
				alert('Laporan Kartu Hilang Terkonfirmasi !!');
				window.location.href='laporan_kartuhilang';
			</script>";    		
	}
	/*CRUD LAPORAN KARTU HILANG*/

	/*CRUD PRAKTIKAN TRANSFER*/
	public function transfer_praktikantransfer()
	{
		$data['praktikan'] 	= $this->m_admin->getPraktikan();
		$data['at'] 		= $this->m_admin->getAT();
		$data['pj'] 		= $this->m_admin->getPJ();

		$data['lap_masuk'] 			= $this->m_admin->getLaporanMasuk();
		$data['lap_terkonfirmasi'] 	= $this->m_admin->getLaporanTerkonfirmasi();
		$data['lap_ditolak'] 		= $this->m_admin->getLaporanTidakTerkonfirmasi();
		$data['lap_kartuhilang']	= $this->m_admin->getLaporanKartuHilang();
		$data['lap_praktrans']		= $this->m_admin->getLaporanPrakTrans();

		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_admin->getAdmin($iduser);
		$data['record'] = $this->m_admin->getLaporanPrakTrans();		
		$this->load->view('admin/header',$data);
		$this->load->view('admin/transfer_praktikantransfer');
		$this->load->view('admin/footer');	
	}
	public function transfer_cekdatamahasiswa()
	{
		$data['praktikan'] 	= $this->m_admin->getPraktikan();
		$data['at'] 		= $this->m_admin->getAT();
		$data['pj'] 		= $this->m_admin->getPJ();

		$data['lap_masuk'] 			= $this->m_admin->getLaporanMasuk();
		$data['lap_terkonfirmasi'] 	= $this->m_admin->getLaporanTerkonfirmasi();
		$data['lap_ditolak'] 		= $this->m_admin->getLaporanTidakTerkonfirmasi();
		$data['lap_kartuhilang']	= $this->m_admin->getLaporanKartuHilang();
		$data['lap_praktrans']		= $this->m_admin->getLaporanPrakTrans();

		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_admin->getAdmin($iduser);
		$this->load->view('admin/header',$data);
		$this->load->view('admin/transfer_cekdatamahasiswa');
		$this->load->view('admin/footer');	
	}
	public function TransferCekDataMahasiswa()
	{
		$npm 				= $this->input->post('npm');
		$data['mahasiswa'] 	= $this->m_admin->TransferCekDataMahasiswa($npm);
		if($data['mahasiswa']!=null){

			$data['praktikan'] 	= $this->m_admin->getPraktikan();
			$data['at'] 		= $this->m_admin->getAT();
			$data['pj'] 		= $this->m_admin->getPJ();

			$data['lap_masuk'] 			= $this->m_admin->getLaporanMasuk();
			$data['lap_terkonfirmasi'] 	= $this->m_admin->getLaporanTerkonfirmasi();
			$data['lap_ditolak'] 		= $this->m_admin->getLaporanTidakTerkonfirmasi();
			$data['lap_kartuhilang']	= $this->m_admin->getLaporanKartuHilang();
			$data['lap_praktrans']		= $this->m_admin->getLaporanPrakTrans();

			$iduser 		= $this->session->userdata('iduser');
			$data['user'] 	= $this->m_admin->getAdmin($iduser);
			$data['mpt'] 	= $this->m_admin->getMataPraktikumTransfer();
			$data['kt'] 	= $this->m_admin->getKelasTransfer();
			$data['npm']	= $npm;
			$a 				= $data['mahasiswa'][0];
			$data['nama']	= $a['nama'];
			$this->load->view('admin/header',$data);
			$this->load->view('admin/transfer_tambahpraktransfer');
			$this->load->view('admin/footer');	
		}
		else{
			echo 
				"<script>
					alert('Data Mahasiswa Tidak Ditemukan !!');
					history.go(-1);
				</script>"; 
		}
	}	
	public function TambahPraktikanTransfer()
	{
		date_default_timezone_set("Asia/Bangkok");
		$date 		= date("m-d-Y_H:i:s");
		$namaadmin 	= $this->session->userdata('namaadmin');
		$tipelaporan= '3';
		$a 			= date("mdYHis");

		$nama 		= $this->input->post('nama');
		$npm 		= $this->input->post('npm');
		$username 	= $this->input->post('username');
		$password 	= $this->input->post('password');
		$no_hp 		= $this->input->post('no_hp');
		$alasan		= $this->input->post('alasan');
		$praktikum 	= $this->input->post('praktikum');
		$kelas_baru	= $this->input->post('kelas_baru');
		$npm_baru	= $this->input->post('npm_baru');
		$pertemuan	= $this->input->post('pertemuan');
		$catatan	= 	'Mata Praktikum : '.$praktikum.', '.
						'Pertemuan : '.$pertemuan;

		$id_user	= 'Prak-'.$npm_baru.'-LabTI';
		$idlaporan 	= $tipelaporan.'-'.$a.'-'.$id_user;

		

		/*cek kesamaan npm baru 1*/
		$data['npmbaru'] = $this->m_admin->CekNpmBaru($npm_baru);
		if($data['npmbaru']!=null){			
			echo 
				"<script>
					alert('NPM Baru Sudah Tersedia !! Gunakan NPM Lainnya !!');
					window.location.href='transfer_cekdatamahasiswa';
				</script>"; 
		}
		else{
			/*cek kesamaan username*/
			$data['username'] = $this->m_admin->CekKesamaanUsername($username);
			if($data['username']!=null){
				echo 
				"<script>
					alert('Username Sudah Tersedia !! Gunakan Username Lainnya !!');
					window.location.href='transfer_cekdatamahasiswa';
				</script>"; 
			}
			else{
				/*input pada tabel1*/
				$input1 = array(
					'npm'		=>$npm_baru,
					'nama'		=>$nama,
					'kelas'		=>$kelas_baru,
					'angkatan'	=>'TRANS',
				);
				$this->m_admin->InputUserBaru1($input1);
				/*input pada tabel2*/
				
				$input2 = array(
					'id_user'	=>$id_user,
					'npm'		=>$npm_baru,
					'username'	=>$username,
					'password'	=>$password,
					'nama'		=>$nama,
					'angkatan'	=>'TRANS',
					'kelas'		=>$kelas_baru,
					'level'		=>'1',
				);
				$this->m_admin->InputUserBaru2($input2);
				/*input pada tabel laporan*/
				$input3 = array(
					'id_laporan'		=>$idlaporan,
					'id_matprak'		=>'PT',
					'id_user'			=>$id_user,
					'npm_baru'			=>$npm_baru,
					'kelas_baru'		=>$kelas_baru,
					'tgl_lapor'			=>$date,
					'no_hp'				=>$no_hp,
					'alasan'			=>$alasan,
					'id_tugas'			=>'PT',
					'admin_verifikasi'	=>$namaadmin,
					'tipe_laporan'		=>'3',
					'catatan'			=>$catatan,
				);
				$this->m_admin->InputLaporanPrakTrans($input3);
				echo 
				"<script>
					alert('Laporan Praktikan Transfer Berhasil Ditambahkan !!');
					window.location.href='transfer_praktikantransfer';
				</script>"; 
			}
		}		
	}
	public function transfer_tambahpraktransferbaru()
	{
		$data['praktikan'] 	= $this->m_admin->getPraktikan();
		$data['at'] 		= $this->m_admin->getAT();
		$data['pj'] 		= $this->m_admin->getPJ();

		$data['lap_masuk'] 			= $this->m_admin->getLaporanMasuk();
		$data['lap_terkonfirmasi'] 	= $this->m_admin->getLaporanTerkonfirmasi();
		$data['lap_ditolak'] 		= $this->m_admin->getLaporanTidakTerkonfirmasi();
		$data['lap_kartuhilang']	= $this->m_admin->getLaporanKartuHilang();
		$data['lap_praktrans']		= $this->m_admin->getLaporanPrakTrans();

		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_admin->getAdmin($iduser);
		
		$data['mpt'] 	= $this->m_admin->getMataPraktikumTransfer();
		$data['kt'] 	= $this->m_admin->getKelasTransfer();

		$this->load->view('admin/header',$data);
		$this->load->view('admin/transfer_tambahpraktransferbaru');
		$this->load->view('admin/footer');
	}
	public function TambahPraktikanTransferBaru()
	{
		date_default_timezone_set("Asia/Bangkok");
		$date 		= date("m-d-Y_H:i:s");
		$namaadmin 	= $this->session->userdata('namaadmin');
		$tipelaporan= '3';
		$a 			= date("mdYHis");

		$nama 		= $this->input->post('nama');
		$npm 		= $this->input->post('npm');
		$no_hp 		= $this->input->post('no_hp');
		$alasan		= $this->input->post('alasan');
		$username 	= $this->input->post('username');
		$password 	= $this->input->post('password');	
		$praktikum 	= $this->input->post('praktikum');
		$kelas_baru	= $this->input->post('kelas_baru');
		$pertemuan	= $this->input->post('pertemuan');
		$catatan	= 	'Mata Praktikum : '.$praktikum.', '.
						'Pertemuan : '.$pertemuan;

		$id_user	= 'Prak-'.$npm.'-LabTI';
		$idlaporan 	= $tipelaporan.'-'.$a.'-'.$id_user;

		/*cek kesamaan username*/
		$data['username'] = $this->m_admin->CekKesamaanUsername($username);
		if($data['username']!=null){
			echo 
				"<script>
					alert('Username Sudah Tersedia !! Gunakan Username Lainnya !!');
					history.go(-1);
				</script>"; 
		}
		else{
			/*input pada tabel1*/
			$input1 = array(
				'npm'		=>$npm,
				'nama'		=>$nama,
				'kelas'		=>$kelas_baru,
				'angkatan'	=>'TRANS',
			);
			$this->m_admin->InputUserBaru1($input1);
			/*input pada tabel2*/
			$input2 = array(
				'id_user'	=>$id_user,
				'npm'		=>$npm,
				'username'	=>$username,
				'password'	=>$password,
				'nama'		=>$nama,
				'angkatan'	=>'TRANS',
				'kelas'		=>$kelas_baru,
				'level'		=>'1',
			);
			$this->m_admin->InputUserBaru2($input2);
			/*input pada tabel laporan*/
			$input3 = array(
				'id_laporan'		=>$idlaporan,
				'id_matprak'		=>'PT',
				'id_user'			=>$id_user,
				'npm_baru'			=>$npm,
				'kelas_baru'		=>$kelas_baru,
				'tgl_lapor'			=>$date,
				'no_hp'				=>$no_hp,
				'alasan'			=>$alasan,
				'id_tugas'			=>'PT',
				'admin_verifikasi'	=>$namaadmin,
				'tipe_laporan'		=>'3',
				'catatan'			=>$catatan,
			);
			$this->m_admin->InputLaporanPrakTrans($input3);
			echo 
				"<script>
					alert('Laporan Praktikan Transfer Berhasil Ditambahkan !!');
					window.location.href='transfer_praktikantransfer';
				</script>"; 
		}
	}
	public function DeletePraktikanTransfer($idlaporan)
	{
		$data['record'] = $this->m_admin->AmbilPraktikanTransfer($idlaporan);
		$a = $data['record'][0];
		/*hapus dari tabel1*/
		$this->m_admin->DeleteLaporanPrakTransfer1($idlaporan);
		/*hapus dari tabel2*/
		$b = $a['id_user'];
		$this->m_admin->DeleteLaporanPrakTransfer2($b);
		/*hapus dari tabel3*/
		$c = $a['npm_baru'];
		$this->m_admin->DeleteLaporanPrakTransfer3($c);
		echo 
			"<script>
				alert('Laporan Praktikan Transfer Berhasil Dihapus !!');
				window.location.href='../transfer_praktikantransfer';
			</script>"; 
	}
	/*CRUD PRAKTIKAN TRANSFER*/

	/*CRUD MATA PRAKTIKUM*/
	public function matprak_pilihtingkat()
	{
		$data['praktikan'] 	= $this->m_admin->getPraktikan();
		$data['at'] 		= $this->m_admin->getAT();
		$data['pj'] 		= $this->m_admin->getPJ();

		$data['lap_masuk'] 			= $this->m_admin->getLaporanMasuk();
		$data['lap_terkonfirmasi'] 	= $this->m_admin->getLaporanTerkonfirmasi();
		$data['lap_ditolak'] 		= $this->m_admin->getLaporanTidakTerkonfirmasi();
		$data['lap_kartuhilang']	= $this->m_admin->getLaporanKartuHilang();
		$data['lap_praktrans']		= $this->m_admin->getLaporanPrakTrans();

		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_admin->getAdmin($iduser);
		$this->load->view('admin/header',$data);
		$this->load->view('admin/matprak_pilihtingkat');
		$this->load->view('admin/footer');
	}
	public function matprak_pilihmatprak($tingkat)
	{
		if($tingkat>4 OR $tingkat<1 ){
			redirect('admin/matprak_pilihtingkat','refresh');
		}
		else{
			$data['praktikan'] 	= $this->m_admin->getPraktikan();
			$data['at'] 		= $this->m_admin->getAT();
			$data['pj'] 		= $this->m_admin->getPJ();

			$data['lap_masuk'] 			= $this->m_admin->getLaporanMasuk();
			$data['lap_terkonfirmasi'] 	= $this->m_admin->getLaporanTerkonfirmasi();
			$data['lap_ditolak'] 		= $this->m_admin->getLaporanTidakTerkonfirmasi();
			$data['lap_kartuhilang']	= $this->m_admin->getLaporanKartuHilang();
			$data['lap_praktrans']		= $this->m_admin->getLaporanPrakTrans();

			$iduser 		= $this->session->userdata('iduser');
			$data['user'] 	= $this->m_admin->getAdmin($iduser);
			$data['record']	= $this->m_admin->getMatprak($tingkat);
			$this->load->view('admin/header',$data);
			$this->load->view('admin/matprak_pilihmatprak');
			$this->load->view('admin/footer');
		}
	}
	public function matprak_lihatpraktikum($idmatprak)
	{
		$data['praktikan'] 	= $this->m_admin->getPraktikan();
		$data['at'] 		= $this->m_admin->getAT();
		$data['pj'] 		= $this->m_admin->getPJ();

		$data['lap_masuk'] 			= $this->m_admin->getLaporanMasuk();
		$data['lap_terkonfirmasi'] 	= $this->m_admin->getLaporanTerkonfirmasi();
		$data['lap_ditolak'] 		= $this->m_admin->getLaporanTidakTerkonfirmasi();
		$data['lap_kartuhilang']	= $this->m_admin->getLaporanKartuHilang();
		$data['lap_praktrans']		= $this->m_admin->getLaporanPrakTrans();

		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_admin->getAdmin($iduser);
		$data['record'] = $this->m_admin->getLihatMataPraktikum($idmatprak);
		$this->load->view('admin/header',$data);
		$this->load->view('admin/matprak_lihatpraktikum');
		$this->load->view('admin/footer');
	}
	public function matprak_editmatapraktikum($idmatprak)
	{
		$data['praktikan'] 	= $this->m_admin->getPraktikan();
		$data['at'] 		= $this->m_admin->getAT();
		$data['pj'] 		= $this->m_admin->getPJ();

		$data['lap_masuk'] 			= $this->m_admin->getLaporanMasuk();
		$data['lap_terkonfirmasi'] 	= $this->m_admin->getLaporanTerkonfirmasi();
		$data['lap_ditolak'] 		= $this->m_admin->getLaporanTidakTerkonfirmasi();
		$data['lap_kartuhilang']	= $this->m_admin->getLaporanKartuHilang();
		$data['lap_praktrans']		= $this->m_admin->getLaporanPrakTrans();

		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_admin->getAdmin($iduser);
		$data['record'] = $this->m_admin->getLihatMataPraktikum($idmatprak);
		$this->load->view('admin/header',$data);
		$this->load->view('admin/matprak_editmatapraktikum');
		$this->load->view('admin/footer');
	}
	public function matprak_updatematapraktikum()
	{
		$b 		= $this->input->post('tingkat');
		$input 	= array(
	        	'id_matprak'	=>$this->input->post('id_matprak'),
	        	'matprak'		=>$this->input->post('matprak'),
	        	'tingkat'		=>$b,
	        	'jum_pertemuan'	=>$this->input->post('jum_pertemuan'),
	        	'deskripsi'		=>$this->input->post('deskripsi'),
		);	
		$this->m_admin->UpdateMataPraktikum($input);
		if($b==1){
			echo 
			"<script>
				alert('Mata Praktikum Berhasil Dirubah !!');
				window.location.href='matprak_pilihmatprak/1';
			</script>";		
		}
		elseif($b==2){
			echo 
			"<script>
				alert('Mata Praktikum Berhasil Dirubah !!');
				window.location.href='matprak_pilihmatprak/2';
			</script>";
		}
		elseif($b==3){
			echo 
			"<script>
				alert('Mata Praktikum Berhasil Dirubah !!');
				window.location.href='matprak_pilihmatprak/3';
			</script>";
		}
		elseif($b==4){
			echo 
			"<script>
				alert('Mata Praktikum Berhasil Dirubah !!');
				window.location.href='matprak_pilihmatprak/4';
			</script>";
		} 
	}
	public function matprak_deletematapraktikum($idmatprak)
	{
		$data['record'] = $this->m_admin->getTingkatMatprak($idmatprak);
		$a = $data['record'][0];
		$b = $a['tingkat'];
		$this->m_admin->DeleteMataPraktikum($idmatprak);
		if($b==1){
			redirect('admin/matprak_pilihmatprak/1');	
		}
		elseif($b==2){
			redirect('admin/matprak_pilihmatprak/2');	
		}
		elseif($b==3){
			redirect('admin/matprak_pilihmatprak/3');	
		}
		elseif($b==4){
			redirect('admin/matprak_pilihmatprak/4');	
		}        
	}
	public function matprak_tambahmatapraktikum()
	{
		$data['praktikan'] 	= $this->m_admin->getPraktikan();
		$data['at'] 		= $this->m_admin->getAT();
		$data['pj'] 		= $this->m_admin->getPJ();

		$data['lap_masuk'] 			= $this->m_admin->getLaporanMasuk();
		$data['lap_terkonfirmasi'] 	= $this->m_admin->getLaporanTerkonfirmasi();
		$data['lap_ditolak'] 		= $this->m_admin->getLaporanTidakTerkonfirmasi();
		$data['lap_kartuhilang']	= $this->m_admin->getLaporanKartuHilang();
		$data['lap_praktrans']		= $this->m_admin->getLaporanPrakTrans();

		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_admin->getAdmin($iduser);
		$this->load->view('admin/header',$data);
		$this->load->view('admin/matprak_tambahmatapraktikum');
		$this->load->view('admin/footer');
	}
	public function matprak_tambahpraktikum()
	{
		$a 		= $this->input->post('id_matprak');	
		$b 		= $this->input->post('tingkat');
		$input 	= array(
	        	'id_matprak'	=>$a,
	        	'matprak'		=>$this->input->post('matprak'),
	        	'tingkat'		=>$b,
	        	'jum_pertemuan'	=>$this->input->post('jum_pertemuan'),
	        	'deskripsi'		=>$this->input->post('deskripsi'),
		);	
		/*cek kesamaan id matprak*/
		$data['record'] = $this->m_admin->getLihatMataPraktikum($a);
		if($data['record']==null){
			$this->m_admin->TambahMataPraktikum($input);
	        if($b==1){
				echo 
				"<script>
					alert('Mata Praktikum Berhasil Ditambahkan !!');
					window.location.href='matprak_pilihmatprak/1';
				</script>";		
			}
			elseif($b==2){
				echo 
				"<script>
					alert('Mata Praktikum Berhasil Ditambahkan !!');
					window.location.href='matprak_pilihmatprak/2';
				</script>";
			}
			elseif($b==3){
				echo 
				"<script>
					alert('Mata Praktikum Berhasil Ditambahkan !!');
					window.location.href='matprak_pilihmatprak/3';
				</script>";
			}
			elseif($b==4){
				echo 
				"<script>
					alert('Mata Praktikum Berhasil Ditambahkan !!');
					window.location.href='matprak_pilihmatprak/4';
				</script>";
			} 
		}
		else{
			echo 
				"<script>
					alert('ID Mata Praktikum Tersedia !! Masukkan ID Lainnya !!');
					history.go(-1);
				</script>";
		}
		
	}
	/*CRUD MATA PRAKTIKUM*/

	public function tugas_praktikan()
	{
		$data['praktikan'] 	= $this->m_admin->getPraktikan();
		$data['at'] 		= $this->m_admin->getAT();
		$data['pj'] 		= $this->m_admin->getPJ();

		$data['lap_masuk'] 			= $this->m_admin->getLaporanMasuk();
		$data['lap_terkonfirmasi'] 	= $this->m_admin->getLaporanTerkonfirmasi();
		$data['lap_ditolak'] 		= $this->m_admin->getLaporanTidakTerkonfirmasi();
		$data['lap_kartuhilang']	= $this->m_admin->getLaporanKartuHilang();
		$data['lap_praktrans']		= $this->m_admin->getLaporanPrakTrans();
		
		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_admin->getAdmin($iduser);
		$data['record'] = $this->m_admin->getTugasPraktikan();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/tugas_praktikan');
		$this->load->view('admin/footer');
	}

	/*CRUD DATA MAHASISWA*/
	public function data_datamahasiswa()
	{
		$data['praktikan'] 	= $this->m_admin->getPraktikan();
		$data['at'] 		= $this->m_admin->getAT();
		$data['pj'] 		= $this->m_admin->getPJ();

		$data['lap_masuk'] 			= $this->m_admin->getLaporanMasuk();
		$data['lap_terkonfirmasi'] 	= $this->m_admin->getLaporanTerkonfirmasi();
		$data['lap_ditolak'] 		= $this->m_admin->getLaporanTidakTerkonfirmasi();
		$data['lap_kartuhilang']	= $this->m_admin->getLaporanKartuHilang();
		$data['lap_praktrans']		= $this->m_admin->getLaporanPrakTrans();
		
		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_admin->getAdmin($iduser);
		$data['record'] = $this->m_admin->getDataMahasiswa();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/data_datamahasiswa');
		$this->load->view('admin/footer');
	}
	public function data_tambahdatamahasiswa()
	{
		$data['praktikan'] 	= $this->m_admin->getPraktikan();
		$data['at'] 		= $this->m_admin->getAT();
		$data['pj'] 		= $this->m_admin->getPJ();

		$data['lap_masuk'] 			= $this->m_admin->getLaporanMasuk();
		$data['lap_terkonfirmasi'] 	= $this->m_admin->getLaporanTerkonfirmasi();
		$data['lap_ditolak'] 		= $this->m_admin->getLaporanTidakTerkonfirmasi();
		$data['lap_kartuhilang']	= $this->m_admin->getLaporanKartuHilang();
		$data['lap_praktrans']		= $this->m_admin->getLaporanPrakTrans();
		
		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_admin->getAdmin($iduser);
		
		$this->load->view('admin/header',$data);
		$this->load->view('admin/data_tambahdatamahasiswa');
		$this->load->view('admin/footer');
	}
	public function TambahDataMahasiswa()
	{
		$npm 		= $this->input->post('npm');
		$nama 		= $this->input->post('nama');
		$angkatan 	= $this->input->post('angkatan');
		$kelas 		= $this->input->post('kelas');

		/*cek kesamaan npm*/
		$data['record'] 	= $this->m_admin->CekKesamaanNPM($npm);
		if($data['record']==null){
			$input 	= array(
	        	'npm'		=>$npm,
	        	'nama'		=>$nama,
	        	'angkatan'	=>$angkatan,
	        	'kelas'		=>$kelas,
			);	
			$this->m_admin->TambahDataMahasiswa($input);	
			echo 
				"<script>
					alert('Data Mahasiswa Berhasil Ditambahkan !!');
					window.location.href='data_datamahasiswa';
				</script>";
		}
		else{
			echo 
				"<script>
					alert('NPM dan Data Mahasiswa Sudah Tersedia !!');
					history.go(-1);
				</script>";
		}
	}
	public function data_ubahdatamahasiswa($npm)
	{
		if($npm!=null){
			$data['praktikan'] 	= $this->m_admin->getPraktikan();
			$data['at'] 		= $this->m_admin->getAT();
			$data['pj'] 		= $this->m_admin->getPJ();

			$data['lap_masuk'] 			= $this->m_admin->getLaporanMasuk();
			$data['lap_terkonfirmasi'] 	= $this->m_admin->getLaporanTerkonfirmasi();
			$data['lap_ditolak'] 		= $this->m_admin->getLaporanTidakTerkonfirmasi();
			$data['lap_kartuhilang']	= $this->m_admin->getLaporanKartuHilang();
			$data['lap_praktrans']		= $this->m_admin->getLaporanPrakTrans();
			
			$iduser 		= $this->session->userdata('iduser');
			$data['user'] 	= $this->m_admin->getAdmin($iduser);
			$data['record'] = $this->m_admin->CekKesamaanNPM($npm);
			$this->load->view('admin/header',$data);
			$this->load->view('admin/data_ubahdatamahasiswa');
			$this->load->view('admin/footer');
		}
		else{
			redirect('admin/data_datamahasiswa');
		}
	}
	public function UbahDataMahasiswa()
	{
		$npm 		= $this->input->post('npm');
		$nama 		= $this->input->post('nama');
		$angkatan 	= $this->input->post('angkatan');
		$kelas 		= $this->input->post('kelas');

		$input 	= array(
	        'npm'		=>$npm,
	        'nama'		=>$nama,
	        'angkatan'	=>$angkatan,
	        'kelas'		=>$kelas,
		);	
		$this->m_admin->UbahDataMahasiswa($input);	
		echo 
			"<script>
				alert('Data Mahasiswa Berhasil Diubah !!');
				window.location.href='data_datamahasiswa';
			</script>";
	}
	public function HapusDataMahasiswa($npm)
	{
		$this->m_admin->HapusDataMahasiswa($npm);	
		echo 
			"<script>
				alert('Data Mahasiswa Berhasil Dihapus !!');
				window.location.href='../data_datamahasiswa';
			</script>";
	}
	/*CRUD DATA MAHASISWA*/

	/*CETAK LAPORAN PELAYANAN*/
	public function cetak_laporanpelayanan(){
		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_admin->getAdmin($iduser);

		$data['record1']= $this->m_admin->getCetakLaporanKetidakhadiran();
		$data['record2']= $this->m_admin->getCetakLaporanKartuhilang();
		$data['record3']= $this->m_admin->getCetakLaporanPraktikantransfer();

		$this->load->view('admin/cetak_laporanpelayanan',$data);
	}

	/*CETAK LAPORAN PELAYANAN*/

	public function about()
	{
		$data['praktikan'] 	= $this->m_admin->getPraktikan();
		$data['at'] 		= $this->m_admin->getAT();
		$data['pj'] 		= $this->m_admin->getPJ();

		$data['lap_masuk'] 			= $this->m_admin->getLaporanMasuk();
		$data['lap_terkonfirmasi'] 	= $this->m_admin->getLaporanTerkonfirmasi();
		$data['lap_ditolak'] 		= $this->m_admin->getLaporanTidakTerkonfirmasi();
		$data['lap_kartuhilang']	= $this->m_admin->getLaporanKartuHilang();
		$data['lap_praktrans']		= $this->m_admin->getLaporanPrakTrans();
		
		$iduser 		= $this->session->userdata('iduser');
		$data['user'] 	= $this->m_admin->getAdmin($iduser);
		$this->load->view('admin/header',$data);
		$this->load->view('admin/about');
		$this->load->view('admin/footer');
	}
	public function logout(){
      	$this->session->sess_destroy();
      	echo 
			"<script>
				alert('Terima Kasih !! Selamat Datang Kembali !!');
				window.location.href='../welcome';
			</script>";
  	}

}

?>